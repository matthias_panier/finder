object Form1: TForm1
  AlignWithMargins = True
  Left = 0
  Top = 0
  Margins.Left = 8
  Margins.Top = 0
  Margins.Right = 8
  Margins.Bottom = 0
  ActiveControl = VFiles
  Caption = 'Doubletten-Finder'
  ClientHeight = 651
  ClientWidth = 1164
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Splitter1: TSplitter
    Left = 289
    Top = 0
    Width = 6
    Height = 632
    ResizeStyle = rsUpdate
    ExplicitLeft = 306
    ExplicitTop = 29
    ExplicitHeight = 657
  end
  object Splitter2: TSplitter
    Left = 973
    Top = 0
    Width = 6
    Height = 632
    Align = alRight
    ResizeStyle = rsUpdate
    ExplicitLeft = 976
    ExplicitTop = 29
    ExplicitHeight = 657
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 632
    Width = 1164
    Height = 19
    DoubleBuffered = True
    Panels = <
      item
        Alignment = taCenter
        Text = 'Disconnected'
        Width = 100
      end
      item
        Text = 'Datenbankpfad'
        Width = 400
      end
      item
        Text = 'Status Ordner'
        Width = 200
      end
      item
        Text = 'aktueller Pfad'
        Width = 500
      end>
    ParentDoubleBuffered = False
  end
  object Panel1: TPanel
    Left = 979
    Top = 0
    Width = 185
    Height = 632
    Align = alRight
    Alignment = taLeftJustify
    TabOrder = 1
    DesignSize = (
      185
      632)
    object Label1: TLabel
      Left = 24
      Top = 197
      Width = 31
      Height = 13
      Caption = 'Label1'
    end
    object Image1: TImage
      Left = 24
      Top = 280
      Width = 129
      Height = 313
      Anchors = [akLeft, akTop, akRight, akBottom]
      Proportional = True
      Stretch = True
    end
    object Label2: TLabel
      Left = 24
      Top = 216
      Width = 31
      Height = 13
      Caption = 'Label2'
    end
    object Label3: TLabel
      Left = 24
      Top = 235
      Width = 31
      Height = 13
      Caption = 'Label3'
    end
    object Label4: TLabel
      Left = 24
      Top = 254
      Width = 31
      Height = 13
      Caption = 'Label4'
    end
    object Image2: TImage
      Left = 24
      Top = 195
      Width = 89
      Height = 79
      Stretch = True
      Transparent = True
    end
    object Button1: TButton
      Left = 24
      Top = 123
      Width = 129
      Height = 26
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Verzeichnis einlesen'
      TabOrder = 0
      OnClick = Button1Click
    end
    object Button2: TButton
      Left = 24
      Top = 165
      Width = 129
      Height = 26
      Anchors = [akLeft, akTop, akRight]
      Caption = 'Einlesen'
      TabOrder = 1
      OnClick = Button2Click
    end
    object Panel4: TPanel
      AlignWithMargins = True
      Left = 4
      Top = 4
      Width = 177
      Height = 113
      Align = alTop
      TabOrder = 2
      object Label10: TLabel
        Left = 80
        Top = 70
        Width = 37
        Height = 13
        Caption = 'Label10'
      end
      object Label9: TLabel
        Left = 80
        Top = 51
        Width = 31
        Height = 13
        Caption = 'Label9'
      end
      object Label8: TLabel
        Left = 80
        Top = 32
        Width = 31
        Height = 13
        Caption = 'Label8'
      end
      object Label5: TLabel
        Left = 80
        Top = 13
        Width = 31
        Height = 13
        Caption = 'Label5'
      end
      object SpeedButton7: TSpeedButton
        Left = 16
        Top = 88
        Width = 23
        Height = 22
        OnClick = SpeedButton7Click
      end
      object Label11: TLabel
        Left = 16
        Top = 13
        Width = 35
        Height = 13
        Caption = 'Folders'
      end
      object Label12: TLabel
        Left = 16
        Top = 32
        Width = 21
        Height = 13
        Caption = 'Files'
      end
      object Label13: TLabel
        Left = 16
        Top = 51
        Width = 35
        Height = 13
        Caption = 'Hashes'
      end
      object Label14: TLabel
        Left = 16
        Top = 70
        Width = 35
        Height = 13
        Caption = 'FileSize'
      end
    end
  end
  object PVFiles1: TPanel
    Left = 295
    Top = 0
    Width = 678
    Height = 632
    Align = alClient
    TabOrder = 2
    object VFiles: TVirtualStringTree
      AlignWithMargins = True
      Left = 9
      Top = 47
      Width = 660
      Height = 576
      Margins.Left = 8
      Margins.Top = 8
      Margins.Right = 8
      Margins.Bottom = 8
      Align = alClient
      Ctl3D = True
      Header.AutoSizeIndex = 0
      Header.Options = [hoColumnResize, hoDrag, hoShowImages, hoShowSortGlyphs, hoVisible]
      Header.Style = hsFlatButtons
      Images = ImageList1
      ParentCtl3D = False
      PopupMenu = PopupMenuVFiles
      TabOrder = 0
      TreeOptions.SelectionOptions = [toFullRowSelect]
      OnChange = VFilesChange
      OnCompareNodes = VFilesCompareNodes
      OnDblClick = VFilesDblClick
      OnFreeNode = VSTFreeNode
      OnGetText = VFilesGetText
      OnPaintText = VFilesPaintText
      OnGetImageIndex = VSTGetImageIndex
      OnHeaderClick = VFilesHeaderClick
      OnKeyUp = VFilesKeyUp
      Columns = <
        item
          Position = 0
          Text = 'Name'
          Width = 160
        end
        item
          Alignment = taRightJustify
          CaptionAlignment = taCenter
          Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coParentColor, coResizable, coShowDropMark, coVisible, coAllowFocus, coUseCaptionAlignment, coEditable, coStyleColor]
          Position = 1
          Text = 'Gr'#246#223'e'
          Width = 100
        end
        item
          Position = 2
          Text = 'Datum'
          Width = 120
        end
        item
          Position = 3
          Text = 'Pfad'
          Width = 91
        end
        item
          Color = 15269887
          Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coResizable, coShowDropMark, coVisible, coAllowFocus, coEditable, coStyleColor]
          Position = 4
          Text = 'Hashed'
          Width = 56
        end
        item
          Color = 15269887
          Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coResizable, coShowDropMark, coVisible, coAllowFocus, coEditable, coStyleColor]
          Position = 5
          Text = 'Tagged'
          Width = 100
        end
        item
          Alignment = taRightJustify
          Color = 15269887
          Options = [coAllowClick, coDraggable, coEnabled, coParentBidiMode, coResizable, coShowDropMark, coVisible, coAllowFocus, coEditable, coStyleColor]
          Position = 6
          Text = 'DB ID'
        end>
    end
    object Panel3: TPanel
      Left = 1
      Top = 1
      Width = 676
      Height = 38
      Align = alTop
      BevelEdges = [beBottom]
      BevelKind = bkSoft
      BevelOuter = bvNone
      TabOrder = 1
      DesignSize = (
        676
        36)
      object Gauge1: TGauge
        AlignWithMargins = True
        Left = 8
        Top = 3
        Width = 660
        Height = 27
        Margins.Left = 8
        Margins.Right = 8
        Margins.Bottom = 0
        Align = alClient
        BackColor = clBtnFace
        BorderStyle = bsNone
        Color = clWhite
        ForeColor = 8454016
        ParentColor = False
        Progress = 0
        ShowText = False
        ExplicitLeft = 10
      end
      object GaugeFiles: TGauge
        AlignWithMargins = True
        Left = 8
        Top = 30
        Width = 660
        Height = 6
        Margins.Left = 8
        Margins.Top = 0
        Margins.Right = 8
        Margins.Bottom = 0
        Align = alBottom
        BackColor = clGradientInactiveCaption
        BorderStyle = bsNone
        ForeColor = clGradientActiveCaption
        MaxValue = 0
        Progress = 0
        ShowText = False
        ExplicitLeft = 6
        ExplicitTop = 32
      end
      object Label6: TLabel
        Left = 661
        Top = 16
        Width = 6
        Height = 13
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '0'
      end
      object Label7: TLabel
        Left = 661
        Top = 3
        Width = 6
        Height = 13
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '0'
        Transparent = True
      end
    end
  end
  object PVFolder1: TPanel
    Left = 0
    Top = 0
    Width = 289
    Height = 632
    Align = alLeft
    TabOrder = 3
    object VFolder: TVirtualStringTree
      AlignWithMargins = True
      Left = 9
      Top = 47
      Width = 271
      Height = 576
      Margins.Left = 8
      Margins.Top = 8
      Margins.Right = 8
      Margins.Bottom = 8
      Align = alClient
      BevelInner = bvNone
      Ctl3D = True
      Header.AutoSizeIndex = 0
      Header.Options = [hoAutoResize, hoColumnResize, hoDrag, hoShowSortGlyphs, hoVisible]
      Images = ImageList1
      ParentCtl3D = False
      PopupMenu = PopupMenuVFolder
      TabOrder = 0
      OnChange = VFolderChange
      OnFreeNode = VSTFreeNode
      OnGetText = VFolderGetText
      OnPaintText = VFolderPaintText
      OnGetImageIndex = VSTGetImageIndex
      OnInitNode = VFolderInitNode
      Columns = <
        item
          Position = 0
          Text = 'Name'
          Width = 99
        end
        item
          Position = 1
          Text = 'C'
          Width = 42
        end
        item
          Position = 2
          Text = 'D'
          Width = 42
        end
        item
          Position = 3
          Text = 'F'
          Width = 42
        end
        item
          Position = 4
          Text = 'P'
          Width = 42
        end>
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 287
      Height = 38
      Align = alTop
      BevelEdges = [beBottom]
      BevelKind = bkSoft
      BevelOuter = bvNone
      TabOrder = 1
      object GaugeFolders: TGauge
        AlignWithMargins = True
        Left = 8
        Top = 30
        Width = 271
        Height = 6
        Margins.Left = 8
        Margins.Top = 0
        Margins.Right = 8
        Margins.Bottom = 0
        Align = alBottom
        BackColor = clGradientInactiveCaption
        BorderStyle = bsNone
        ForeColor = clGradientActiveCaption
        MaxValue = 0
        Progress = 0
        ShowText = False
        ExplicitLeft = 188
        ExplicitTop = 0
        ExplicitWidth = 100
      end
    end
  end
  object MainMenu1: TMainMenu
    Left = 912
    Top = 384
    object Datei1: TMenuItem
      Caption = 'Datei'
      object Beenden1: TMenuItem
        Caption = 'Beenden'
        OnClick = Beenden1Click
      end
    end
    object Einstellungen1: TMenuItem
      Caption = 'Optionen'
      object Datenbank1: TMenuItem
        Caption = 'Datenbank'
        OnClick = Datenbank1Click
      end
      object Einstellungen2: TMenuItem
        Caption = 'Einstellungen'
        OnClick = Einstellungen2Click
      end
      object Logging1: TMenuItem
        Caption = 'Logging'
        OnClick = Logging1Click
      end
    end
  end
  object ImageList1: TImageList
    ShareImages = True
    Left = 400
    Top = 64
  end
  object ImageList2: TImageList
    ShareImages = True
    Left = 400
    Top = 120
  end
  object PopupMenuVFolder: TPopupMenu
    Left = 80
    Top = 104
    object NeuenOrdnererstellen1: TMenuItem
      Caption = 'Neuen Ordner erstellen'
    end
    object OrdnerEinlesen1: TMenuItem
      Caption = 'Ordner Einlesen ...'
    end
  end
  object PopupMenuVFiles: TPopupMenu
    Left = 496
    Top = 216
  end
end
