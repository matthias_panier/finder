unit classExif;

interface

uses
    u_DM_type_def,
    CCR.Exif,
    system.sysutils,
    DateUtils;

type
    TClassExif = class
    private
        FFilepath: string;
        FIsImage: boolean;

        PropMeta: TMeta;

        procedure setFilePath(const filepath: string);
        function getFileName: string;
    protected
    public
        property filepath: string read FFilepath write setFilePath;
        property filename: string read getFileName;
        property isImage: boolean read FIsImage;

        property Meta: TMeta read PropMeta;

        constructor create;
        destructor destroy; override;

    end;

implementation

constructor TClassExif.create;
begin
    PropMeta := getEmptyTMeta;
    FFilepath := '';
end;

destructor TClassExif.destroy;
begin
    PropMeta := getEmptyTMeta;
    FFilepath := '';
end;

procedure TClassExif.setFilePath(const filepath: string);
var
    ExifData: TExifData;
    datum: int64;
    dimx, dimy: int64;
begin
    PropMeta := getEmptyTMeta;
    FFilepath := filepath;
    FIsImage := false;

    if (UpperCase(ExtractFileExt(FFilepath)) = '.JPG') then
    begin
        try
            try
                ExifData := TExifData.create;
                FIsImage := ExifData.LoadFromGraphic(UnicodeString(FFilepath));

                if (FIsImage) then
                begin
                    datum := DateTimeToUnix(ExifData.DateTime);
                    dimx := ExifData.ExifImageWidth;
                    dimy := ExifData.ExifImageHeight;
                    PropMeta.kind := MyKimage;

                    if (dimx > 2000111000) then
                        dimx := 0;
                    if (dimy > 2000111000) then
                        dimy := 0;
                    if (datum < 0) then
                        datum := 0;

                    PropMeta.dimx := dimx;
                    PropMeta.dimy := dimy;

                    PropMeta.DPIX := 0;
                    PropMeta.DPIY := 0;
                    PropMeta.Description := ExifData.ImageDescription;
                    PropMeta.Camera := ExifData.CameraModel;
                    PropMeta.make := ExifData.CameraMake;
                    PropMeta.Date := datum;
                    PropMeta.Coordinates.longitude := GPSLonToReal(ExifData.GPSLongitude);
                    PropMeta.Coordinates.latitude := GPSLatToReal(ExifData.GPSLatitude);
                    PropMeta.Coordinates.altitude := GPSAltToInt(ExifData.GPSAltitude);

                    PropMeta.display.Description := ExifData.ImageDescription;
                    PropMeta.display.Date := PropMeta.Date;
                    PropMeta.display.Camera := PropMeta.Camera;
                    PropMeta.display.make := PropMeta.make;
                    PropMeta.display.Coordinates := PropMeta.Coordinates;

                end;
            except
                on E: Exception do
                begin
                    // E.Message;
                end;
            end;
        finally
            ExifData.Free;
        end;
    end;
    // https://delphihaven.wordpress.com/ccr-exif/
end;

function TClassExif.getFileName: string;
begin
    result := extractfilename(FFilepath);
end;

end.
