unit classDriveRead;

interface

uses classDriveReadDB, u_DM_type_def, System.SysUtils, Winapi.Windows, classDBSet, VirtualTrees;

type
    TclassDriveRead = class(TclassDriveReadDB)

    private
        drives: TDrives;

        FuseCallback: boolean;
        FaddDriveCallBack: TCBaddDrive;

        FParentNode: PVirtualNode;

        procedure getLocalDrives;
        function PrepareDriveEntry(pfad: string): TDrive;
        function addDrive(const currentDrive: TDrive): integer;

        function isValidDrive(vDrive: PChar): boolean;
        function retrieveVolumeName(const pfad: string): string;
        function retrieveVolumeSerialNumber(const pfad: string): string;

    public
        constructor Create(const dbName: string; const aDataSet: TDataSet; ParentNode: PVirtualNode);
        destructor destroy; override;

        procedure useCallback(const useCallback: boolean);
        procedure setAddDriveCallback(const addDriveCallBack: TCBaddDrive);
        procedure setParentNode(const ParentNode: PVirtualNode);
        procedure readLocalDrives;
        procedure addDBDrives;
    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////
constructor TclassDriveRead.Create(const dbName: string; const aDataSet: TDataSet; ParentNode: PVirtualNode);
begin
    inherited Create(dbName, aDataSet);
    setlength(drives, 0);

    useCallback(false);
    setParentNode(ParentNode);
end;

/// ///////////////////////////
// Setter Functions
/// ///////////////////////////
procedure TclassDriveRead.useCallback(const useCallback: boolean);
begin
    FuseCallback := useCallback;
end;

procedure TclassDriveRead.setAddDriveCallback(const addDriveCallBack: TCBaddDrive);
begin
    FaddDriveCallBack := addDriveCallBack;
end;

procedure TclassDriveRead.setParentNode(const ParentNode: PVirtualNode);
begin
    FParentNode := ParentNode;
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////
destructor TclassDriveRead.destroy;
begin
    inherited;

    setlength(drives, 0);
    setlength(DBDrives, 0);

    // can't be freed because the node of the VST will be invalidated by freeing
    // parentSet.Free;
end;

procedure TclassDriveRead.readLocalDrives;
begin
    if (DataSet.Computer.source.isFromFilesystem) then
    begin
        getLocalDrives;
    end;
end;

/// ///////////////////////////////////////////////////////
/// Getting all Physical Drives of the System
/// No MTP
/// ///////////////////////////////////////////////////////
procedure TclassDriveRead.getLocalDrives;
var
    vDrivesSize: cardinal;
    vDrives: array [0 .. 128] of char;
    vDrive: PChar;
    Drive : TDrive;
begin
    // https://www.delphipraxis.net/332-laufwerke-finden-und-erkennen.html
    vDrivesSize := GetLogicalDriveStrings(sizeof(vDrives), vDrives);

    if vDrivesSize = 0 then
        Exit;

    vDrive := vDrives;

    try
        try
            while vDrive^ <> #0 do
            begin
                if isValidDrive(vDrive) then
                begin
                    Drive := PrepareDriveEntry(StrPas(vDrive));
                    addDrive(Drive);
                end;

                Inc(vDrive, sizeof(vDrive));
            end;
        except
            on E: Exception do
            begin

            end;
        end;
    finally
        vDrives := '';
    end;
end;

/// ///////////////////////////////////////////////////////
/// returns true if the drive has to be read, skipping CDRoms
/// ///////////////////////////////////////////////////////
function TclassDriveRead.isValidDrive(vDrive: PChar): boolean;
begin
    result := true;

    // ignore CDRoms
    if GetDriveType(vDrive) = DRIVE_CDROM then
    begin
        result := false;
    end;
end;

function TclassDriveRead.PrepareDriveEntry(pfad: string): TDrive;
var dID:dfID;
begin
    finalize(result);

    result.Database.Computer_ID := DataSet.Computer.ID;
    result.Database.pfad := pfad;
    result.Database.folder := pfad;

    result.Database.ID := 0;
    result.Database.Folder_ID := 0;
    result.Database.DSize := diskSize(ord(pfad[1]) - 64);
    result.Database.SerialNR := retrieveVolumeSerialNumber(pfad);
    result.Database.Caption := retrieveVolumeName(pfad);

    dID := getDBDriveID(result.Database);
    result.Database.ID := dID.driveID;
    result.Database.Folder_ID := dID.folderID;

    result.imageIndex := getPathIconID(pfad);
    result.DataSet := DataSet;
    result.DataSet.Path := pfad;
    result.DataSet.kind := getKindRecord(false, true, false, false);
    result.DataSet.Drive.ID := result.Database.ID;
    result.DataSet.Drive.source.isFromDB := result.Database.ID > 0;
    result.DataSet.Drive.source.isFromFilesystem := true;

    result.DataSet.folder.ID := result.Database.Folder_ID;
    result.DataSet.folder.source.isFromDB := result.Database.Folder_ID > 0;
    result.DataSet.folder.source.isFromFilesystem := true;
end;

function TclassDriveRead.retrieveVolumeSerialNumber(const pfad: string): string;
var
    Drive: string;
    FileSysName, VolName: array [0 .. 255] of char;
    SerialNum, MaxCLength, FileSysFlag: DWORD;
begin
    result := '';
    Drive := IncludeTrailingPathDelimiter(extractFileDrive(pfad));
    GetVolumeInformation(PChar(Drive), VolName, 255, @SerialNum, MaxCLength, FileSysFlag, FileSysName, 255);
    result := IntToHex(SerialNum, 8);
end;

function TclassDriveRead.retrieveVolumeName(const pfad: string): string;
var
    dummy: DWORD;
    buffer: array [0 .. MAX_PATH] of char;
    oldmode: LongInt;
    DriveLetter: char;
begin
    DriveLetter := pfad[1];

    oldmode := SetErrorMode(SEM_FAILCRITICALERRORS);
    try
        GetVolumeInformation(PChar(DriveLetter + ':\'), buffer, sizeof(buffer), nil, dummy, dummy, nil, 0);
        result := StrPas(buffer);

        if (result = '') then
            result := 'Laufwerk';
    finally
        SetErrorMode(oldmode);
    end;
end;

function TclassDriveRead.addDrive(const currentDrive: TDrive): integer;
var
    l: integer;
begin
    l := length(drives);
    setlength(drives, l + 1);

    drives[l] := currentDrive;

    if (FuseCallback) and (assigned(FaddDriveCallBack)) then
        FaddDriveCallBack(currentDrive, FParentNode);

    result := l;
end;

procedure TclassDriveRead.addDBDrives;
var
    i, j: integer;
    l, m: integer;
    found: boolean;
    currentDrive: TDrive;
begin
    m := high(drives);

    for i := 0 to high(DBDrives) do
    begin
        // check if DB Drive is in drives list
        found := false;

        for j := 0 to m do
        begin
            if (drives[j].Database.ID = DBDrives[i].ID) then
            begin
                found := true;
                break;
            end;
        end;

        // if not in list, then prepare a DB Drive and add it to the list
        if not found then
        begin
            finalize(currentDrive);

            currentDrive.Database := DBDrives[i];
            currentDrive.DataSet := DataSet;
            currentDrive.DataSet.kind := getKindRecord(false, true, false, false);
            currentDrive.DataSet.Drive.ID := DBDrives[i].ID;
            currentDrive.DataSet.Drive.source.isFromDB := true;
            currentDrive.DataSet.Drive.source.isFromFilesystem := false;

            addDrive(currentDrive);
        end;
    end;
end;

end.
