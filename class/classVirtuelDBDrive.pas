unit classVirtuelDBDrive;

interface

uses
    u_DM_type_def,
    system.SysUtils,
    classDBBase;

type
    TClassVirtualDBDrive = class(TClassDBBase)
        // function selectIDOfDrive(const drive: TDrive): integer;
        // function insertDrive(const drive: TDrive): integer;
        //
        // function selectIDofMainFolder(const driveID: integer): integer;
        // function updateMainFolder(const driveID, folderID: integer): boolean;
        // function insertMainFolder(const drive: TDrive; const driveID: integer): integer;
        // function GetParentIDofPath(const driveID: integer; const pfad: string): integer;
    protected
    public
        // function getDriveID(const drive: TDrive): integer;
    end;

implementation

// function TClassVirtualDBDrive.getDriveID(const drive: TDrive): integer;
// begin
// result := selectIDOfDrive(drive);
//
// if (result = 0) then
// begin
// result := insertDrive(drive);
// insertMainFolder(drive, result);
// end;
// end;
//
// function TClassVirtualDBDrive.selectIDOfDrive(const drive: TDrive): integer;
// begin
// result := 0;
//
// q.Close;
//
// q.SQL.Text := 'SELECT ID FROM DRIVES WHERE COMPUTER_ID = :cid AND PATH = :path AND SERIALNR = :sn';
// q.Params[0].AsInteger := drive.Database.Computer_ID;
// q.Params[1].asstring := drive.Database.pfad;
// q.Params[2].asstring := drive.Database.SerialNR;
//
// try
// q.Open;
// if not q.Eof then
// begin
// result := q.Fields[0].AsInteger;
// end;
// finally
// q.SQL.Clear;
// q.Params.Clear;
// q.Close;
// end;
// end;
//
// function TClassVirtualDBDrive.insertDrive(const drive: TDrive): integer;
// var
// id: integer;
// begin
/// /    id := getNextIDOfTable('DRIVES');
/// /
/// /    q.Close;
/// /    q.SQL.Text :=
/// /        'INSERT INTO DRIVES ("ID", "COMPUTER_ID", "DSIZE", "PATH", "SERIALNR", "CAPTION", "CREATE_DATE") VALUES (:id, :cid, :dsize, :path, :sn, :caption, :datum);';
/// /    q.Params[0].AsInteger := id;
/// /    q.Params[1].AsInteger := drive.DBID.computerID;
/// /    q.Params[2].AsLargeInt := drive.size;
/// /    q.Params[3].asstring := drive.pfad;
/// /    q.Params[4].asstring := drive.serialNR;
/// /    q.Params[5].asstring := drive.name;
/// /    q.Params[6].AsFloat := now;
/// /
/// /    try
/// /        q.ExecSQL;
/// /        result := id;
/// /    finally
/// /        q.SQL.Clear;
/// /        q.Params.Clear;
/// /        q.Close;
/// /    end;
// end;
//
// function TClassVirtualDBDrive.selectIDofMainFolder(const driveID: integer): integer;
// begin
// result := 0;
//
// q.Close;
//
// q.SQL.Text := 'SELECT FOLDER_ID FROM DRIVES WHERE ID = :id';
// q.Params[0].AsInteger := driveID;
//
// try
// q.Open;
// if not q.Eof then
// begin
// result := q.Fields[0].AsInteger;
// end;
// finally
// q.SQL.Clear;
// q.Params.Clear;
// q.Close;
// end;
// end;
//
// function TClassVirtualDBDrive.updateMainFolder(const driveID, folderID: integer): boolean;
// begin
// result := false;
//
// q.Close;
// q.SQL.Text := 'UPDATE DRIVES SET FOLDER_ID= :fid WHERE ID = :id';
// q.Params[0].AsInteger := folderID;
// q.Params[1].AsInteger := driveID;
//
// try
// q.ExecSQL;
// result := true;
// finally
// q.SQL.Clear;
// q.Params.Clear;
// q.Close;
// end;
// end;
//
// function TClassVirtualDBDrive.GetParentIDofPath(const driveID: integer; const pfad: string): integer;
// begin
// result := 0;
//
// q.Close;
// q.SQL.Text := 'SELECT PARENT_ID FROM FOLDERS WHERE DRIVES_ID =:did AND PATH =:pfad';
// q.Params[0].AsInteger := driveID;
// q.Params[1].asstring := ExcludeTrailingPathDelimiter(pfad);
//
// try
// q.Open;
//
// if not q.Eof then
// begin
// result := q.Fields[0].AsInteger;
// end;
// finally
// q.SQL.Clear;
// q.Params.Clear;
// q.Close;
// end;
// end;
//
// function TClassVirtualDBDrive.insertMainFolder(const drive: TDrive; const driveID: integer): integer;
// var
// id: integer;
// begin
/// /    result := GetParentIDofPath(driveID,drive.pfad);
/// /
/// /    if result = 0 then
/// /    begin
/// /
/// /        id := getNextIDOfTable('FOLDERS');
/// /
/// /        q.Close;
/// /        q.SQL.Text :=
/// /            'INSERT INTO FOLDERS ("ID", "PARENT_ID", "DRIVES_ID", "PATH", "FOLDER", "CREATE_DATE") VALUES (:id, :parentID, :driveID, :pfad, :name, :datum);';
/// /        q.Params[0].AsInteger := id;
/// /        q.Params[1].AsInteger := id;
/// /        q.Params[2].AsInteger := driveID;
/// /        q.Params[3].asstring := ExcludeTrailingPathDelimiter(drive.pfad);
/// /        q.Params[4].asstring := ExtractFilename(ExcludeTrailingPathDelimiter(drive.pfad));
/// /        q.Params[5].AsFloat := now;
/// /
/// /        try
/// /            q.ExecSQL;
/// /            result := id;
/// /            updateMainFolder(driveID, id);
/// /        finally
/// /            q.SQL.Clear;
/// /            q.Params.Clear;
/// /            q.Close;
/// /        end;
/// /
/// /    end;
/// /
// end;

end.
