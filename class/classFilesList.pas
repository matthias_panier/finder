unit classFilesList;

interface

uses u_DM_type_def, System.Masks, System.SysUtils;

type
    TClassFilesList = class
        function getListAsText: string;
        procedure setListFromText(const s: string);
    public
        active: boolean;
        List: array of TFilesListEntry;

        property Text: string read getListAsText write setListFromText;
        constructor create;
        destructor destroy; override;
        function IsInList(const filename: string; const filesize: int64): boolean;
        function AddFileEntry(const mask: string; const minSize, maxSize: int64): boolean;
        function EditFileEntry(const position: integer; const mask: string; const minSize, maxSize: int64): boolean;
        function RemoveFileEntry(const position: integer): boolean;
    end;

implementation

constructor TClassFilesList.create;
begin
    setLength(List, 0);
    active := false;
end;

destructor TClassFilesList.destroy;
begin
    setLength(List, 0);
    active := false;
end;

function TClassFilesList.getListAsText: string;
var
    i: integer;
    b: boolean;
begin
    result := '';
    b := false;

    for i := Low(List) to High(List) do
    begin
        if (b) then
        begin
            result := result + '|';
        end;

        result := result + List[i].mask + ';' + List[i].minSize.tostring + ';' + List[i].maxSize.tostring;
        b := true;
    end;
end;

procedure TClassFilesList.setListFromText(const s: string);
var
    rows, row: TExplodeArray;
    a, i: integer;
begin
    setLength(List, 0);

    // a := 1;
    if (pos('|', s) > 0) then
    begin
        rows := Explode('|', s);
        for i := Low(rows) to High(rows) do
        begin
            row := Explode(';', rows[i]);
            setLength(List, i + 1);
            List[i].mask := row[0];
            List[i].minSize := strtoint(row[1]);
            List[i].maxSize := strtoint(row[2]);
        end;

        setLength(row, 0);
        setLength(rows, 0);
    end;
end;

function TClassFilesList.IsInList(const filename: string; const filesize: int64): boolean;
var
    i: integer;
Begin
    result := false;

    for i := Low(List) to High(List) do
    begin
        if MatchesMask(filename, List[i].mask) then
        begin
            if (List[i].maxSize > 0) then
            begin
                if ((filesize > List[i].minSize) and (filesize < List[i].maxSize)) then
                begin
                    result := true;
                    exit;
                end;
            end else begin
                result := true;
                exit;
            end;
        end;
    end;
end;

function TClassFilesList.AddFileEntry(const mask: string; const minSize, maxSize: int64): boolean;
var
    i: integer;
Begin
    result := false;
    i := length(List);
    try
        setLength(List, i + 1);

        List[i].mask := mask;
        List[i].minSize := minSize;
        List[i].maxSize := maxSize;
        result := true;
    except

    end;
end;

function TClassFilesList.EditFileEntry(const position: integer; const mask: string;
    const minSize, maxSize: int64): boolean;
Begin
    result := false;
    List[position].mask := mask;
    List[position].minSize := minSize;
    List[position].maxSize := maxSize;
end;

function TClassFilesList.RemoveFileEntry(const position: integer): boolean;
var
    i: integer;
Begin
    for i := position to High(List) - 1 do
        List[i] := List[i + 1];

    setLength(List, length(List) - 1);
    result := true;
end;

end.
