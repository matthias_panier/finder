unit classComputerReadDB;

interface

uses
    u_DM_type_def,
    system.SysUtils,
    ClassDBBase;

type
    TclassComputerReadDB = class(TClassDBBase)
        DBComputers: array of TDBComputer;
        procedure loadDBComputer;
        procedure addToDBComputers(computer: TDBComputer);
        function getDBComputerPos(const gc: TDBComputer): integer;
    private

    public
        constructor Create(const dbName: string);
        destructor destroy; override;

        function getDBComputerID(currentComputer: TDBComputer): integer;
        function insertComputer(const computerName: string): TDBComputer;
    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////
constructor TclassComputerReadDB.Create(const dbName: string);
begin
    inherited Create(dbName);
    setlength(DBComputers, 0);
    loadDBComputer;
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////
destructor TclassComputerReadDB.destroy;
begin
    inherited;

    setlength(DBComputers, 0);
end;

procedure TclassComputerReadDB.loadDBComputer;
var
    pos: integer;
    t1: cardinal;
begin
    if q.Database.Connected then
    begin
        t1 := starttick;
        qpreset;

        q.SQL.Add('SELECT');
        q.SQL.Add('ID,');
        q.SQL.Add('NAME,');
        q.SQL.Add('ADD_DATE');
        q.SQL.Add('FROM COMPUTER');

        try
            try
                q.open;
                while not q.eof do
                begin
                    pos := q.recNo - 1;
                    setlength(DBComputers, q.recNo);

                    DBComputers[pos].ID := q.fieldbyname('ID').asInteger;
                    DBComputers[pos].name := q.fieldbyname('NAME').asstring;
                    DBComputers[pos].add_date := q.fieldbyname('ADD_DATE').AsLargeInt;

                    q.next;
                end;
            except
                on E: Exception do
                begin

                end;
            end;
        finally
            finishtick(t1, 'loadDBComputer', q.SQL.Text);
            qpreset;
        end;
    end;
end;

function TclassComputerReadDB.getDBComputerID(currentComputer: TDBComputer): integer;
var
    pos: integer;
begin
    pos := getDBComputerPos(currentComputer);

    if (pos > -1) then
    begin
        result := DBComputers[pos].ID;
        exit;
    end;

    currentComputer.ID := insertComputer(currentComputer.name).ID;
end;

function TclassComputerReadDB.getDBComputerPos(const gc: TDBComputer): integer;
var
    i: integer;
begin
    result := -1;

    for i := low(DBComputers) to High(DBComputers) do
        if (DBComputers[i].name = gc.name) then
        begin
            result := i;
            exit;
        end;
end;

/// Adds a given TDBComputer Data set to the internal Array
procedure TclassComputerReadDB.addToDBComputers(computer: TDBComputer);
var
    i: integer;
begin
    // validate that it is a Dataset and not the empty data
    if computer.ID = 0 then
        exit;

    // extend the DB Computer array
    // validate, should not be needed
    i := Length(DBComputers);
    setlength(DBComputers, i + 1);
    DBComputers[i] := computer;
end;

/// ////////////////////////////////
/// Inserts a row in the Database with the given "computerName" and returns the whole row as TDBComputer
/// ////////////////////////////////
function TclassComputerReadDB.insertComputer(const computerName: string): TDBComputer;
var
    ID: integer;
    i: integer;
    uts: int64;
    t1: cardinal;
begin
    finalize(result);

    if (qpreset) then
    begin
        t1 := starttick;
        ID := getNextIDOfTable('COMPUTER');
        uts := getUnixTimeStamp;

        q.Close;
        q.SQL.Text := 'INSERT INTO COMPUTER ("ID", "NAME", "ADD_DATE") VALUES (:id, :name, :datum);';
        q.Params[0].asInteger := ID;
        q.Params[1].asstring := computerName;
        q.Params[2].AsLargeInt := uts;

        try
            try
                q.ExecSQL;
                result.ID := ID;
                result.name := computerName;
                result.add_date := uts;

                // extend dataset
                addToDBComputers(result);
            except
                on E: Exception do
                begin
                end;
            end;
        finally
            finishtick(t1, 'insertComputer', inttostr(ID) + ': ' + computerName);
            qpreset;
        end;
    end;
end;

end.
