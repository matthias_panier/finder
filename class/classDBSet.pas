unit classDBSet;

interface

uses u_DM_type_def, System.SysUtils;

type
    TClassDBSet = class

    private
        // defines the kind of the node and the content
        DataSet: TDataSet;

        Computer: TDBComputer;
        Drive: TDBDrive;
        Folder: TDBFolder;
        Datei: TDBFile;
        Image: TDBImgData;

        PropImageIndex: integer;

        function FGetID: integer;
        function FGetParentID: integer;
        function FRefNo: string;
        function FParentRefNo: string;
        function FGetCaption: string;
        function FisComputer: boolean;
        function FisDrive: boolean;
        function FGetFullPath: string;
        function FIsComputerLocal: boolean;
        function FIsFolderLocal: boolean;
        function FisFile: boolean;
        function FGetDatum: int64;
        function FisFromDB: boolean;
        function FisFromFS: boolean;
        function FisHashed: boolean;
        function FisTagged: boolean;
        procedure FSetCaption(const caption: string);

    public
        constructor create;
        destructor destroy; override;

        property ImageIndex: integer read PropImageIndex write PropImageIndex;
        property caption: string read FGetCaption write FSetCaption;
        property Size: int64 read Datei.FSize;
        property Datum: int64 read FGetDatum;
        property ID: integer read FGetID;
        property ParentID: integer read FGetParentID;

        property ComputerID: integer read Computer.ID;
        property DriveID: integer read Drive.ID;
        property FolderID: integer read Folder.ID;
        property DateiID: integer read Datei.ID;

        property isComputer: boolean read FisComputer;
        property isDrive: boolean read FisDrive;
        property isFile: boolean read FisFile;

        property RefNo: string read FRefNo;
        property ParentRefNo: string read FParentRefNo;
        property FullPath: string read FGetFullPath;

        property isComputerLocal: boolean read FIsComputerLocal;
        property isFolderLocal: boolean read FIsFolderLocal;

        property isFromDB: boolean read FisFromDB;
        property isFromFS: boolean read FisFromFS;
        property isHashed: boolean read FisHashed;
        property isTagged: boolean read FisTagged;

        function setComputer(const givenComputer: TComputer): boolean;
        function setDrive(const givenDrive: TDrive): boolean;
        function setFolder(const givenFolder: TFolder): boolean;
        function setFile(const givenFile: TDatei): boolean;
        function setImageData(const DBImage: TDBImgData): boolean;

        function setFolderID(const ID: integer): boolean;
        function setHash(const hash: string): boolean;

        function getParentSet: TDataSet;
    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////
constructor TClassDBSet.create;
begin
    inherited;

    Finalize(Computer);
    Finalize(Drive);
    Finalize(Folder);
    Finalize(Datei);

    with DataSet.kind do
    begin
        isComputer := false;
        isDrive := false;
        isFolder := false;
        isFile := false;
    end;
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////
destructor TClassDBSet.destroy;
begin
    Finalize(Computer);
    Finalize(Drive);
    Finalize(Folder);
    Finalize(Datei);

    inherited;
end;

function TClassDBSet.setComputer(const givenComputer: TComputer): boolean;
begin
    Computer := givenComputer.Database;
    DataSet := givenComputer.DataSet;

    ImageIndex := givenComputer.ImageIndex;

    result := true;
end;

function TClassDBSet.setDrive(const givenDrive: TDrive): boolean;
begin
    Drive := givenDrive.Database;
    DataSet := givenDrive.DataSet;

    ImageIndex := givenDrive.ImageIndex;

    // additional Information for computer
    Computer.ID := Drive.Computer_ID;

    // additional information for the Folder
    Folder.Computer_ID := Drive.Computer_ID;
    Folder.Drive_ID := Drive.ID;

    Folder.Parent_ID := Drive.Folder_ID;
    Folder.ID := Drive.Folder_ID;
    Folder.Path := Drive.pfad;

    result := true;
end;

function TClassDBSet.setFolder(const givenFolder: TFolder): boolean;
begin
    Folder := givenFolder.Database;
    DataSet := givenFolder.DataSet;

    PropImageIndex := givenFolder.ImageIndex;

    Computer.ID := givenFolder.Database.Computer_ID;
    Drive.ID := givenFolder.Database.Drive_ID;

    result := true;
end;

function TClassDBSet.setFile(const givenFile: TDatei): boolean;
begin
    Datei := givenFile.Database;
    DataSet := givenFile.DataSet;

    ImageIndex := givenFile.ImageIndex;

    // todo set IDs

    result := true;
end;

function TClassDBSet.setImageData(const DBImage: TDBImgData): boolean;
begin
    //
end;

function TClassDBSet.setFolderID(const ID: integer): boolean;
begin
    Folder.ID := ID;
    Datei.Folder_ID := ID;

    result := ID <> 0;
end;

function TClassDBSet.setHash(const hash: string): boolean;
begin
    Datei.Contenthash := hash;

    result := hash <> '';
end;

function TClassDBSet.FParentRefNo: string;
var
    ID: integer;
begin
    result := '';

    if DataSet.kind.isFolder then
    begin
        ID := Folder.Parent_ID;

        if ID > 0 then
        begin
            result := ID.ToString;
        end;
    end;
end;

function TClassDBSet.FRefNo: string;
var
    ID: integer;
begin
    result := '';

    ID := FGetID;

    if ID > 0 then
        result := inttostr(ID);
end;

function TClassDBSet.FGetParentID: integer;
begin
    result := 0;

    if DataSet.kind.isDrive then
        result := Drive.Folder_ID;
    if DataSet.kind.isFolder then
        result := Folder.Parent_ID;
end;

function TClassDBSet.FGetID: integer;
begin
    result := 0;

    if DataSet.kind.isComputer then
        result := Computer.ID;
    if DataSet.kind.isDrive then
        result := Drive.ID;
    if DataSet.kind.isFolder then
        result := Folder.ID;
    if DataSet.kind.isFile then
        result := Datei.ID;
end;

function TClassDBSet.FGetCaption: string;
begin
    if DataSet.kind.isComputer then
        result := Computer.name;
    if DataSet.kind.isDrive then
        result := Drive.caption;
    if DataSet.kind.isFolder then
        result := Folder.caption;
    if DataSet.kind.isFile then
        result := Datei.caption;
end;

procedure TClassDBSet.FSetCaption(const caption: string);
begin
    if DataSet.kind.isComputer then
        Computer.name := caption;
end;

function TClassDBSet.FisComputer: boolean;
begin
    result := DataSet.kind.isComputer;
end;

function TClassDBSet.FisDrive: boolean;
begin
    result := DataSet.kind.isDrive;
end;

function TClassDBSet.FGetFullPath: string;
begin
    result := { IncludeTrailingPathDelimiter } (Folder.Path);
end;

function TClassDBSet.FGetDatum: int64;
begin
    result := 0;

    if DataSet.kind.isFolder then
        result := Folder.create_date;
    if DataSet.kind.isFile then
        result := Datei.create_date;

end;

function TClassDBSet.FIsComputerLocal: boolean;
begin
    // TODO deprecated - use next one
    result := DataSet.Computer.Source.isFromFilesystem;
end;

function TClassDBSet.FIsFolderLocal: boolean;
begin
    result := ((DataSet.kind.isDrive) and (DataSet.Drive.Source.isFromFilesystem)) or
        ((DataSet.kind.isFolder) and (DataSet.Folder.Source.isFromFilesystem));
end;

function TClassDBSet.FisFile: boolean;
begin
    result := DataSet.kind.isFile;
end;

function TClassDBSet.FisFromDB: boolean;
begin
    // TODO alle kombinationen auflisten
    result := (DataSet.kind.isComputer and DataSet.Computer.Source.isFromDB);
end;

function TClassDBSet.FisFromFS: boolean;
begin
    // TODO alle kombinationen auflisten
    result := (DataSet.kind.isComputer and DataSet.Computer.Source.isFromFilesystem);
end;

function TClassDBSet.FisHashed: boolean;
begin
    result := Datei.Contenthash <> '';
end;

function TClassDBSet.FisTagged: boolean;
begin
    result := Datei.isTagged = 1;
end;

function TClassDBSet.getParentSet: TDataSet;
begin
    result := DataSet;
end;

end.
