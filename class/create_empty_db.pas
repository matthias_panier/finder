unit create_empty_db;

interface

uses IBX.IBDatabase, SysUtils;

type
    TClassCreateEmptyDatabase = class
    protected
        FlastError: string;
        function createEmptyDB(const dbpfad: string): boolean;
    public
        constructor create;
        destructor destroy; override;
        property lastErrorMsg: string read FlastError;
    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////

constructor TClassCreateEmptyDatabase.create;
begin
    FlastError := '';
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////

destructor TClassCreateEmptyDatabase.destroy;
begin
    FlastError := '';
end;

function TClassCreateEmptyDatabase.createEmptyDB(const dbpfad: string): boolean;
var
    idb: TIBDatabase;
begin
    result := false;
    FlastError := '';

    try
        idb := TIBDatabase.create(nil);

        try
            idb.DatabaseName := dbpfad;
            idb.SQLDialect := 3;
            idb.Params.Clear;
            idb.Params.Add('USER "SYSDBA"');
            idb.Params.Add('PASSWORD "masterkey"');
            idb.Params.Add('PAGE_SIZE 4096');
            idb.LoginPrompt := false;
            idb.CreateDatabase;

            result := idb.Connected;
            idb.close;
        finally
            idb.Free;
        end;
    except
        on E: exception do
        begin
            FlastError := E.ClassName + ': ' + E.Message
        end;
    end;
end;

end.
