unit classVirtuelDBImages;

interface

uses
    u_DM_type_def,
    classDBBase,
    system.sysUtils,
    ClassExif,
    system.Classes,
    IdHashMessageDigest;

type
    TclassVirtuelDBImages = class(TclassDBBase)

    protected

        Exif: TClassExif;

    public
        constructor Create(const dbName: string);
        destructor Destroy; override;

        function getImageData(f: TDatei): TDatei;
        // function selectImageData(const f: TDatei): TMeta;
        // function insertImageData(const id: integer; const m: TMeta): TMeta;
    end;

implementation

constructor TclassVirtuelDBImages.Create(const dbName: string);
begin
    inherited Create(dbName);
    Exif := TClassExif.Create;

end;

destructor TclassVirtuelDBImages.Destroy;
begin
    Exif.Free;
    inherited;
end;

function TclassVirtuelDBImages.getImageData(f: TDatei): TDatei;
begin
    result := f;
    // result.meta := selectImageData(f);
    //
    // if result.meta.kind = MyKnone then
    // begin
    // Exif.filepath := f.pfad + f.filename; // starts analyse of image data
    // if Exif.isImage then
    // begin
    // result.meta := insertImageData(f.dbid.fileID, Exif.meta);
    // end;
    // end;
end;

// function TclassVirtuelDBImages.selectImageData(const f: TDatei): TMeta;
// begin
// result := getEmptyTMeta;
//
// q.close;
// q.SQL.Clear;
// q.SQL.Add('SELECT');
// q.SQL.Add('LON,LAT,ALT,X,Y,');
// q.SQL.Add('DPIX,DPIY,CAMERA,MAKE,CREATE_DATE ');
// q.SQL.Add('FROM IMGDATA');
// q.SQL.Add('WHERE');
// q.SQL.Add('ID =:id');
//
// q.Params[0].AsInteger := f.dbid.fileID;
//
// try
// q.Open;
// if not q.Eof then
// begin
// result.kind := MyKimage;
// result.Coordinates.longitude := q.Fields[0].AsFloat;
// result.Coordinates.latitude := q.Fields[1].AsFloat;
// result.Coordinates.altitude := q.Fields[2].AsInteger;
// result.dimX := q.Fields[3].AsInteger;
// result.dimy := q.Fields[4].AsInteger;
//
// result.DPIX := q.Fields[5].AsInteger;
// result.DPIY := q.Fields[6].AsInteger;
// result.camera := q.Fields[7].AsAnsiString;
// result.make := q.Fields[8].AsAnsiString;
// result.date := q.Fields[9].AsLargeInt;
//
// result.description := '';
// end;
// finally
// q.SQL.Clear;
// q.Params.Clear;
// q.close;
// end;
//
// end;

// function TclassVirtuelDBImages.insertImageData(const id: integer; const m: TMeta): TMeta;
// begin
// result.kind := MyKnone;
//
// q.close;
// q.SQL.Clear;
// q.SQL.Add('INSERT INTO IMGDATA');
// q.SQL.Add(
// '("ID","LON","LAT","ALT","X","Y","DPIX","DPIY","CAMERA","MAKE","CREATE_DATE","DISPLAY_LON","DISPLAY_LAT","DISPLAY_ALT","DISPLAY_DATE","DISPLAY_CAMERA","DISPLAY_MAKE","DISPLAY_DESC","DESCRIPTION")');
// q.SQL.Add('VALUES');
// q.SQL.Add(
// '(:id, :lon, :lat, :alt, :x, :y, :dpix, :dpiy, :camera,:make, :createdate, :dlon, :dlat, :dalt, :ddate, :dcamera, :dmake, :ddesc, :desc );');
//
// q.Params[0].AsInteger := id;
// q.Params[1].AsFloat := m.Coordinates.longitude;
// q.Params[2].AsFloat := m.Coordinates.latitude;
// q.Params[3].AsInteger := m.Coordinates.altitude;
// q.Params[4].AsInteger := m.dimX;
// q.Params[5].AsInteger := m.dimy;
// q.Params[6].AsInteger := m.DPIX;
// q.Params[7].AsInteger := m.DPIY;
// q.Params[8].AsAnsiString := m.camera;
// q.Params[9].AsAnsiString := m.make;
// q.Params[10].AsLargeInt := m.date;
//
// q.Params[11].AsFloat := m.display.Coordinates.longitude;
// q.Params[12].AsFloat := m.display.Coordinates.latitude;
// q.Params[13].AsInteger := m.display.Coordinates.altitude;
// q.Params[14].AsLargeInt := m.display.date;
// q.Params[15].AsAnsiString := m.display.camera;
// q.Params[16].AsAnsiString := m.display.make;
// q.Params[17].AsAnsiString := m.display.description;
// q.Params[18].AsAnsiString := m.description;
//
// try
// q.ExecSQL;
// result := m;
// result.kind := MyKimage;
// finally
// q.SQL.Clear;
// q.Params.Clear;
// q.close;
// end;
// end;

end.
