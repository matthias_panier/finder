unit classFilter;

interface

uses u_DM_type_def, System.SysUtils, classOptions, classSysParams, Vcl.ComCtrls;

type
    TClassFilter = class(TClassSysParams)

    private

    protected

    public
        options: TClassOptions;

        constructor create;
        destructor destroy; override;
        function addExtension(const extension: string): boolean; overload;
        function addExtension(const extension: string; const active: boolean): boolean; overload;
        function addExtension(const extension: string; const active: boolean; const min, max: int64): boolean; overload;

        function isAddFile(const f: TDatei): boolean;
        function isAddFolder(const f: TFolder): boolean;
        function isAddFileToHash(const f: TDatei): boolean;
        function isAddFindDoubletten(const f: TDatei): boolean;
        function isAddReadExif(const f: TDatei): boolean;
        function isExpandRecursiveImport: boolean;

        procedure LoadOptions;
        procedure SaveOptions;

        procedure SetRecursiveReadList(const items: TListItems);
    end;

implementation

constructor TClassFilter.create;
begin
    inherited create;
    options := TClassOptions.create;
    LoadOptions;
end;

destructor TClassFilter.destroy;
begin
    SaveOptions;

    options.Free;
    inherited destroy;
end;

procedure TClassFilter.LoadOptions;
var
    i: integer;
begin
    options.FolderBlackList.active := strtobool(readFromRegistry('FolderBlackList.active', 'False'));
    options.FolderBlackList.List.Text := readFromRegistry('FolderBlackList.List', '.git');

    options.Display.ShowFolders := strtobool(readFromRegistry('Display.ShowFolders', 'False'));
    options.Display.ExpandDoubletten := strtobool(readFromRegistry('Display.ExpandDoubletten', 'True'));
    options.Display.ExpandFolders := strtobool(readFromRegistry('Display.ExpandFolders', 'True'));
    options.Display.ShowDoubletten := strtobool(readFromRegistry('Display.ShowDoubletten', 'True'));

    options.Display.Filter := strtobool(readFromRegistry('Display.Filter', 'False'));
    options.Display.FilterMask := readFromRegistry('Display.FilterMask', '');

    options.RecursiveRead.UseThread := strtobool(readFromRegistry('RecursiveRead.UseThread', 'False'));
    options.RecursiveRead.hashFiles := strtobool(readFromRegistry('RecursiveRead.HashFiles', 'False'));
    options.RecursiveRead.exifFiles := strtobool(readFromRegistry('RecursiveRead.ExifFiles', 'False'));

    options.RecursiveRead.CombineInFolders := strtobool(readFromRegistry('RecursiveRead.CombineInFolders', 'False'));
    options.RecursiveRead.ExpandCombinedFolders :=
        strtobool(readFromRegistry('RecursiveRead.ExpandCombinedFolders', 'False'));
    options.RecursiveRead.ScollToEnd := strtobool(readFromRegistry('RecursiveRead.ScollToEnd', 'False'));

    options.RecursiveRead.UseMaxFileSize := strtobool(readFromRegistry('RecursiveRead.UseMaxFileSize', 'False'));
    options.RecursiveRead.MaxFileSize := 0;
    if trystrtoint(readFromRegistry('RecursiveRead.MaxFileSize', '0'), i) then
        options.RecursiveRead.MaxFileSize := i;

    options.LiveImport.Folders := strtobool(readFromRegistry('LiveImport.Folders', 'True'));
    options.LiveImport.Files.active := strtobool(readFromRegistry('LiveImport.Files', 'True'));
    options.LiveImport.Files.UseThread := strtobool(readFromRegistry('LiveImport.Files.Threads', 'False'));
    options.LiveImport.Exif.active := strtobool(readFromRegistry('LiveImport.Exif', 'True'));
    options.LiveImport.Exif.UseThread := strtobool(readFromRegistry('LiveImport.Exif.Threads', 'False'));
    options.LiveImport.hash.active := strtobool(readFromRegistry('LiveImport.hash', 'True'));
    options.LiveImport.hash.UseThread := strtobool(readFromRegistry('LiveImport.hash.Threads', 'False'));
    options.LiveImport.UseMaxFileSize := strtobool(readFromRegistry('LiveImport.UseMaxFileSize', 'False'));
    options.LiveImport.MaxFileSize := 0;
    if trystrtoint(readFromRegistry('LiveImport.MaxFileSize', '0'), i) then
        options.LiveImport.MaxFileSize := i;

    options.FilesWhiteList.active := strtobool(readFromRegistry('FilesWhiteList.active', 'False'));
    options.FilesWhiteList.Text := readFromRegistry('FilesWhiteList.List', '');

    options.FilesBlackList.active := strtobool(readFromRegistry('FilesBlackList.active', 'False'));
    options.FilesBlackList.Text := readFromRegistry('FilesBlackList.List', '');

    options.Preload.Drive := strtobool(readFromRegistry('PreloadDrive', 'False'));
    options.Preload.Folders := strtobool(readFromRegistry('PreloadFolders', 'False'));
    options.Preload.Files := strtobool(readFromRegistry('PreloadFiles', 'False'));
end;

procedure TClassFilter.SaveOptions;
begin
    writeToRegistry('FolderBlackList.active', booltostr(options.FolderBlackList.active));
    writeToRegistry('FolderBlackList.List', options.FolderBlackList.List.Text);

    writeToRegistry('Display.ShowFolders', booltostr(options.Display.ShowFolders));
    writeToRegistry('Display.ExpandFolders', booltostr(options.Display.ExpandFolders));
    writeToRegistry('Display.ShowDoubletten', booltostr(options.Display.ShowDoubletten));
    writeToRegistry('Display.ExpandDoubletten', booltostr(options.Display.ExpandDoubletten));

    writeToRegistry('Display.Filter', booltostr(options.Display.Filter));
    writeToRegistry('Display.FilterMask', options.Display.FilterMask);

    writeToRegistry('RecursiveRead.UseThread', booltostr(options.RecursiveRead.UseThread));
    writeToRegistry('RecursiveRead.HashFiles', booltostr(options.RecursiveRead.hashFiles));
    writeToRegistry('RecursiveRead.ExifFiles', booltostr(options.RecursiveRead.exifFiles));

    writeToRegistry('RecursiveRead.CombineInFolders', booltostr(options.RecursiveRead.CombineInFolders));
    writeToRegistry('RecursiveRead.ExpandCombinedFolders', booltostr(options.RecursiveRead.ExpandCombinedFolders));
    writeToRegistry('RecursiveRead.ScollToEnd', booltostr(options.RecursiveRead.ScollToEnd));
    writeToRegistry('RecursiveRead.UseMaxFileSize', booltostr(options.RecursiveRead.UseMaxFileSize));
    writeToRegistry('RecursiveRead.MaxFileSize', options.RecursiveRead.MaxFileSize.ToString);

    writeToRegistry('LiveImport.Folders', booltostr(options.LiveImport.Folders));
    writeToRegistry('LiveImport.Files', booltostr(options.LiveImport.Files.active));
    writeToRegistry('LiveImport.Files.Threads', booltostr(options.LiveImport.Files.UseThread));
    writeToRegistry('LiveImport.Exif', booltostr(options.LiveImport.Exif.active));
    writeToRegistry('LiveImport.Exif.Threads', booltostr(options.LiveImport.Exif.UseThread));
    writeToRegistry('LiveImport.hash', booltostr(options.LiveImport.hash.active));
    writeToRegistry('LiveImport.hash.Threads', booltostr(options.LiveImport.hash.UseThread));
    writeToRegistry('LiveImport.UseMaxFileSize', booltostr(options.LiveImport.UseMaxFileSize));
    writeToRegistry('LiveImport.MaxFileSize', options.LiveImport.MaxFileSize.ToString);

    writeToRegistry('FilesWhiteList.active', booltostr(options.FilesWhiteList.active));
    writeToRegistry('FilesWhiteList.List', options.FilesWhiteList.Text);

    writeToRegistry('FilesBlackList.active', booltostr(options.FilesBlackList.active));
    writeToRegistry('FilesBlackList.List', options.FilesBlackList.Text);

    writeToRegistry('PreloadDrive', booltostr(options.Preload.Drive));
    writeToRegistry('PreloadFolders', booltostr(options.Preload.Folders));
    writeToRegistry('PreloadFiles', booltostr(options.Preload.Files));
end;

procedure TClassFilter.SetRecursiveReadList(const items: TListItems);
var
    i: integer;
begin

    //
end;

function TClassFilter.isAddFile(const f: TDatei): boolean;
begin
    result := (f.DataSet.datei.source.isFromFilesystem) and (options.LiveImport.Files.active);
end;

function TClassFilter.isAddFolder(const f: TFolder): boolean;
begin
    result := (f.DataSet.folder.source.isFromFilesystem) and (options.LiveImport.Folders);
end;

function TClassFilter.isAddFileToHash(const f: TDatei): boolean;
begin
    result := (f.DataSet.datei.source.isFromFilesystem) and (options.LiveImport.hash.active);

    result := (result) and ((f.Database.FSize < 10000000) or (uppercase(f.Database.extension) = '.JPG'));
end;

function TClassFilter.isAddFindDoubletten(const f: TDatei): boolean;
begin
    result := (f.DataSet.datei.source.isFromFilesystem) and (options.Display.ShowDoubletten);
end;

function TClassFilter.isAddReadExif(const f: TDatei): boolean;
begin
    result := (f.DataSet.datei.source.isFromFilesystem) and (options.LiveImport.Exif.active) and
        (uppercase(f.Database.extension) = '.JPG');
end;

function TClassFilter.addExtension(const extension: string): boolean;
begin
    result := addExtension(extension, true, 0, 10000000);
end;

function TClassFilter.addExtension(const extension: string; const active: boolean): boolean;
begin
    result := addExtension(extension, active, 0, 10000000);
end;

function TClassFilter.addExtension(const extension: string; const active: boolean; const min, max: int64): boolean;
var
    l: integer;
begin
    // l := length(options.AutoHashFiles.extensions);
    // setlength(options.AutoHashFiles.extensions, l + 1);
    //
    // options.AutoHashFiles.extensions[l].extension := extension;
    // options.AutoHashFiles.extensions[l].active := active;
    // options.AutoHashFiles.extensions[l].minSize := min;
    // options.AutoHashFiles.extensions[l].maxSize := max;
end;

function TClassFilter.isExpandRecursiveImport: boolean;
begin
    result := options.RecursiveRead.CombineInFolders and options.RecursiveRead.ExpandCombinedFolders;
end;

end.
