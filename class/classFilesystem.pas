﻿unit classFilesystem;

interface

uses SysUtils, Windows, FileCtrl, Classes, IdHashMessageDigest, Forms,
    IBX.IBQuery, ShellApi, Dialogs, System.IOUtils, ComCtrls;

type
    TMyFile = record
        folder: string;
        name: string;
        size: int64;
        timestamp: TDateTime;

        md5: string;
        ID: integer;
    end;

type
    TMyFileList = array of TMyFile;

type
    TMyFolder = record
        folder: string;
        size: int64;
        filesCount: integer;
        ID: integer;
    end;

type
    TMyFolderList = array of TMyFolder;

type
    TMyDrive = record
        letter: char;
        name: string;
        number: string;

        ID: integer;
    end;

type
    TMySystem = record
        name: string;

        ID: integer;
    end;

type
    TClassFileSystem = class
    private
        FInitialDir: string;
        FFolderCount: integer;
        FFilesCount: integer;

        FFilesList: TMyFileList;
        FFolderList: TMyFolderList;
        FDrive: TMyDrive;
        FSystem: TMySystem;

        FFileMask: string;
        FWithSubDirs: boolean;
        FMinSize: int64;
        FMaxSize: int64;

        function getFolderCount: integer;
        function getFilesCount: integer;
        function getVolumeName: string;
        function getVolumeSerialNumber: string;
        function getVolumeLetter: PChar;

        procedure setInitialDir(const dir: string);
        procedure setFileMask(const mask: string);
        procedure setMinFilesize(const size: int64);
        procedure setMaxFilesize(const size: int64);
        procedure setWithSubDirs(const withDirs: boolean);

        // function FILEMD5(const fileName: string): string;
        // function NAMEMD5(const s: string): string;

        // function FileTimeToDateTime(FileTime: TFileTime): TDateTime;
        // function getSizeBySR(sr: TSearchRec): int64;

        // procedure addFolder ( const dir: string; up: TUpdateProgress ) ;
        // procedure addCountFilesOfFolder ( const dir: string; up: TUpdateProgress ) ;
        // function FileTimeToDateTime ( FileTime: TFileTime ) : TDateTime;
        // procedure processDirectory ( action: TMyAction; up: TUpdateProgress ) ;
        // function getSizeBySR ( sr: TSearchRec ) : int64;
        // function getNameMD5ofMyFile ( f: TMyFile ) : string;
        // function getInfoFromSearchRec ( const dir, folder, pcNameAndSerial: string; SR: TSearchRec ) : TMyFile;
        // function UpdateMyFileFromSearchRec ( var f: TMyFile; SR: TSearchRec ) : boolean;

        // function isDriveRoot ( const dir: string ) : boolean;

    public

        constructor create;
        destructor destroy; override;

        // procedure setCancelRequest;
        // procedure setStarting;
        // procedure setRootIconIndex ( index: integer ) ;

        //
        property Directory: string read FInitialDir write setInitialDir;
        property WithSubDirectories: boolean read FWithSubDirs write setWithSubDirs;
        Property FileMask: string read FFileMask write setFileMask;
        Property MinSize: int64 read FMinSize write setMinFilesize;
        property MaxSize: int64 read FMaxSize write setMaxFilesize;
        property VolumeName: string read getVolumeName;
        property VolumeSerialNumber: string read getVolumeSerialNumber;
        property VolumeLetter: PChar read getVolumeLetter;

        property Folders: integer read getFolderCount;
        property Files: integer read getFilesCount;

        /// Returns the logical drive serial number
        function retrieveVolumeSerialNumber(const folder: string): string;
        function retrieveVolumeName(DriveLetter: char): string;
        // function getCountFilesInDir(const dir: string): integer;
        // function getFilesInDir(const dir: string; items: TListItems): boolean;
        function GetSystemName: string;

        // procedure process;
        // function extractFolder ( const path: string ) : string;
        //
        // function FILEMD5 ( const fileName: string ) : string;
        // function NAMEMD5 ( const s: string ) : string;
        //
        // procedure createDirectoryOutline ( action: TMyAction; up: TUpdateProgress ) ;
        //
        // procedure processDirectoryOutline ( action: TMyAction; insert: TInsert; up: TUpdateProgress ) ;
        // procedure processFilesInDir ( action: TMyAction; const dir: string; insert: TInsert; up: TUpdateProgress ) ;
        //
        // procedure execute ( const uri: string ) ;
        //
        // function getHashAndTimeOfFile ( f: TMyFile ) : TMyFile;
        // function DeleteFileWithUndo ( sFileName: string ) : Boolean;
        //
    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////

constructor TClassFileSystem.create;
begin
    inherited create;

    FFileMask := '*.*';
    FWithSubDirs := false;
    FMinSize := -1;
    FMaxSize := -1;
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////

destructor TClassFileSystem.destroy;
begin
    // if assigned(directoryList) then
    // directoryList.destroy;
    FFileMask := '';
    FMinSize := 0;
    FMaxSize := 0;

    inherited destroy;
end;

function TClassFileSystem.GetSystemName: string;
var
    Len: DWORD;
begin
    result := '';

    try
        Len := MAX_COMPUTERNAME_LENGTH + 1;
        SetLength(result, Len);
        if Windows.GetComputerName(PChar(result), Len) then
        begin
            SetLength(result, Len);
        end else begin
            RaiseLastOSError;
        end;
    except
        // _log('Kann den Computernamen nicht ermitteln');
    end;
end;

function TClassFileSystem.getFolderCount: integer;
begin
    if (FFolderCount = -1) and (FInitialDir <> '') then
    begin
        FFolderCount := length(FFolderList);
    end;

    result := FFolderCount;
end;

function TClassFileSystem.getFilesCount: integer;
begin
    if (FFilesCount = -1) and (FInitialDir <> '') then
    begin
        FFilesCount := length(FFilesList);
    end;
    result := FFilesCount;
end;

procedure TClassFileSystem.setInitialDir(const dir: string);
begin
    FInitialDir := IncludeTrailingPathDelimiter(dir);

    FFolderCount := -1;
    FFilesCount := -1;

    SetLength(FFilesList, 0);
    SetLength(FFolderList, 0);

    FDrive.ID := 0;
    FDrive.name := '';
    FDrive.number := '';
    FDrive.letter := dir[1];

    FSystem.ID := 0;
    FSystem.name := '';

    FDrive.letter := dir[1];
    FDrive.name := retrieveVolumeName(dir[1]);
    FDrive.number := retrieveVolumeSerialNumber(dir);

    FSystem.name := GetSystemName;
end;

procedure TClassFileSystem.setFileMask(const mask: string);
begin
    FFileMask := mask;
    FFolderCount := -1;
    FFilesCount := -1;
end;

procedure TClassFileSystem.setMinFilesize(const size: int64);
begin
    FMinSize := size;
    FFolderCount := -1;
    FFilesCount := -1;
end;

procedure TClassFileSystem.setMaxFilesize(const size: int64);
begin
    FMaxSize := size;
    FFolderCount := -1;
    FFilesCount := -1;
end;

procedure TClassFileSystem.setWithSubDirs(const withDirs: boolean);
begin
    FWithSubDirs := withDirs;
    FFolderCount := -1;
    FFilesCount := -1;
end;

function TClassFileSystem.getVolumeName: string;
begin
    if (FDrive.letter <> '') and (FDrive.name <> '') then
    begin
        FDrive.name := retrieveVolumeName(FDrive.letter);
    end;

    result := FDrive.name;
end;

function TClassFileSystem.getVolumeSerialNumber: string;
begin
    if (FDrive.letter <> '') and (FDrive.number = '') then
    begin
        FDrive.number := retrieveVolumeSerialNumber(FInitialDir);
    end;

    result := FDrive.number;
end;

function TClassFileSystem.getVolumeLetter: PChar;
begin
    result := PChar(FDrive.letter);

end;

function TClassFileSystem.retrieveVolumeSerialNumber(const folder: string): string;
var
    drive: string;
    FileSysName, VolName: array [0 .. 255] of char;
    SerialNum, MaxCLength, FileSysFlag: DWORD;
begin
    result := '';
    drive := IncludeTrailingPathDelimiter(extractFileDrive(folder));
    GetVolumeInformation(PChar(drive), VolName, 255, @SerialNum, MaxCLength, FileSysFlag, FileSysName, 255);
    result := IntToHex(SerialNum, 8);
end;

function TClassFileSystem.retrieveVolumeName(DriveLetter: char): string;
var
    dummy: DWORD;
    buffer: array [0 .. MAX_PATH] of char;
    oldmode: LongInt;
begin
    oldmode := SetErrorMode(SEM_FAILCRITICALERRORS);
    try
        GetVolumeInformation(PChar(DriveLetter + ':\'), buffer, SizeOf(buffer), nil, dummy, dummy, nil, 0);
        result := StrPas(buffer);
    finally
        SetErrorMode(oldmode);
    end;
end;

// function TClassFileSystem.getCountFilesInDir(const dir: string): integer;
// var
// path: string;
// tsr: TSearchRec;
// begin
// path := IncludeTrailingPathDelimiter(dir);
// result := 0;
// if SysUtils.FindFirst(path + '*.*', faAnyFile or faDirectory, tsr) = 0 then
// begin
// repeat
// if (tsr.Attr and faDirectory) = 0 then
// inc(result)
// else if (tsr.name <> '.') and (tsr.name <> '..') then
// result := result + getCountFilesInDir(IncludeTrailingBackslash(dir) + tsr.name);
// until SysUtils.FindNext(tsr) <> 0;
// SysUtils.FindClose(tsr);
// end;
// end;

// function TClassFileSystem.getFilesInDir(const dir: string; items: TListItems): boolean;
// var
// path: string;
// tsr: TSearchRec;
// begin
// result := false;
// path := IncludeTrailingPathDelimiter(dir);
// // result := TStringlist.create;
// if SysUtils.FindFirst(path + '*.*', faAnyFile or faDirectory, tsr) = 0 then
// begin
// repeat
// if (tsr.Attr and faDirectory) = 0 then
// begin
// var
// item: TListItem;
// item.Caption := tsr.name; // gr��e datum laufwerk pfad
// item.SubItems.Add(tsr.size.ToString);
// item.SubItems.Add(formatdatetime('c', tsr.timestamp));
// item.SubItems.Add('');
// item.SubItems.Add(dir);
//
// items.AddItem(item);
// end else if (tsr.name <> '.') and (tsr.name <> '..') then
// getFilesInDir(IncludeTrailingBackslash(dir) + tsr.name, items);
//
// until SysUtils.FindNext(tsr) <> 0;
// SysUtils.FindClose(tsr);
// end;
// end;
//
// procedure TClassFileSystem.process;
// var
// path: string;
// tsr: TSearchRec;
// f: TMyFile;
// d: TMyFolder;
// l: integer;
// begin
// path := IncludeTrailingPathDelimiter(FInitialDir);
// if SysUtils.FindFirst(path + '*.*', faAnyFile or faDirectory, tsr) = 0 then
// begin
// l := length(FFolderList);
// SetLength(FFolderList, l + 1);
//
// d.folder := ExtractFilePath(tsr.name);
// d.size := 0;
// d.filesCount := 0;
//
// repeat
// if (tsr.Attr and faDirectory) = 0 then
// begin
// l := length(FFilesList);
// SetLength(FFilesList, l + 1);
// f.name := tsr.name;
// f.folder := ExtractFilePath(tsr.name);
// f.size := tsr.size;
// f.timestamp := tsr.timestamp;
// FFilesList[l] := f;
//
// end else if (tsr.name <> '.') and (tsr.name <> '..') then
// begin
// l := length(FFolderList);
// SetLength(FFolderList, l + 1);
//
// d.folder := ExtractFilePath(tsr.name);
// end;
// until SysUtils.FindNext(tsr) <> 0;
// SysUtils.FindClose(tsr);
// end;
// end;

/// ///////////////////////////
// Gets the MD5 Checksum of a given Filename
/// ///////////////////////////

// function TClassFileSystem.FILEMD5(const fileName: string): string;
// var
// idmd5: TIdHashMessageDigest5;
// fs: TFileStream;
// begin
// result := '';
// idmd5 := TIdHashMessageDigest5.create;
// fs := TFileStream.create(fileName, fmOpenRead or fmShareDenyWrite);
//
// try
// result := idmd5.HashBytesAsHex(idmd5.HashStream(fs));
// // result := idmd5.AsHex(idmd5.HashValue(fs));
// finally
// fs.Free;
// idmd5.Free;
// end;
// end;

/// ///////////////////////////
// Gets the MD5 Checksum of a given string
/// ///////////////////////////

// function TClassFileSystem.NAMEMD5(const s: string): string;
// var
// idmd5: TIdHashMessageDigest5;
// begin
// idmd5 := TIdHashMessageDigest5.create;
//
// try
// result := idmd5.HashBytesAsHex(idmd5.HashString(uppercase(s)));
// finally
// idmd5.Free;
// end;
// end;

// function ClassFS.UpdateMyFileFromSearchRec ( var f: TMyFile; SR: TSearchRec ) : boolean;
// begin
// {
// L? C??? ??AD VSHR
//
// L Windows symling
// C Conpressed
// A Archive
// D Directory
// V Volume
// S System
// H Hidden
// R read Only
//
/// /  TSearchRec = record
/// /    Time: Integer;
/// /    Size: Integer;
/// /    Attr: Integer;
/// /    Name: TFileName;
/// /    ExcludeAttr: Integer;
/// /{$IFDEF MSWINDOWS}
/// /    FindHandle: THandle  platform;
/// /    FindData: TWin32FindData  platform;
/// /{$ENDIF}
/// /{$IFDEF LINUX}
/// /    Mode: mode_t  platform;
/// /    FindHandle: Pointer  platform;
/// /    PathOnly: String  platform;
/// /    Pattern: String  platform;
/// /{$ENDIF}
/// /  end;
//
/// /  _WIN32_FIND_DATAA = record
/// /    dwFileAttributes: DWORD;
/// /    ftCreationTime: TFileTime;
/// /    ftLastAccessTime: TFileTime;
/// /    ftLastWriteTime: TFileTime;
/// /    nFileSizeHigh: DWORD;
/// /    nFileSizeLow: DWORD;
/// /    dwReserved0: DWORD;
/// /    dwReserved1: DWORD;
/// /    cFileName: array[0..MAX_PATH - 1] of AnsiChar;
/// /    cAlternateFileName: array[0..13] of AnsiChar;
/// /  end;
/// /
/// /}
//
// end;
//
//
// function TClassFileSystem.FileTimeToDateTime(FileTime: TFileTime): TDateTime;
// var
// ModifiedTime: TFileTime;
// SystemTime: TSystemTime;
// begin
// try
// FileTimeToLocalFileTime(FileTime, ModifiedTime);
// FileTimeToSystemTime(ModifiedTime, SystemTime);
// result := SystemTimeToDateTime(SystemTime);
// except
// result := Now;
// end;
// end;
//

//
// function TClassFileSystem.getSizeBySR(sr: TSearchRec): int64;
// var
// size: TULargeInteger;
// begin
// size.LowPart := sr.FindData.nFileSizeLow;
// size.HighPart := sr.FindData.nFileSizeHigh;
//
// result := size.QuadPart;
// end;

/// /////////////////////////////
/// / requires
/// /  f.uri
/// /
/// /////////////////////////////
//
// function ClassFS.getHashAndTimeOfFile ( f: TMyFile ) : TMyFile;
// var SR: TSearchRec;
// begin
// //
// result.db.Files.ID := f.db.Files.ID;
// if fileexists ( f.db.folders.path + f.db.files.filename ) then
// begin
//
// if cancelrequest then
// begin
// sysutils.FindClose ( SR ) ;
// exit;
// end;
//
// try
// result.db.files.chash := FILEMD5 ( f.db.folders.path + f.db.files.filename ) ;
// except
// /// fehlermessage
// end;
// if FindFirst ( f.db.folders.path + f.db.files.filename, faAnyFile, SR ) = 0 then
// try
// result.db.files.created := FileTimeToDateTime ( SR.FindData.ftCreationTime ) ;
// result.db.files.changed := FileTimeToDateTime ( SR.FindData.ftLastWriteTime ) ;
// result.db.files.accessed := FileTimeToDateTime ( SR.FindData.ftLastAccessTime ) ;
// finally
// sysutils.FindClose ( SR ) ;
// end;
//
// end;
// end;
//
/// /http://www.swissdelphicenter.ch/en/showcode.php?id=31
//
// function ClassFS.DeleteFileWithUndo ( sFileName: string ) : Boolean;
// var
// fos: TSHFileOpStruct;
// begin
// FillChar ( fos, SizeOf ( fos ) , 0 ) ;
// with fos do
// begin
// wFunc := FO_DELETE;
// pFrom := PChar ( sFileName ) ;
// fFlags := FOF_ALLOWUNDO or FOF_NOCONFIRMATION or FOF_SILENT;
// end;
// Result := ( 0 = ShFileOperation ( fos ) ) ;
// end;

end.
