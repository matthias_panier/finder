unit classVirtuelDBFolder;

interface

uses
    u_DM_type_def,
    classDBBase,
    System.SysUtils;

type
    TClassVirtuelDBFolder = class(TclassDBBase)
        folders: TFolders;

    protected
    public
        constructor create(const DBName: string);
        destructor destroy; override;

        function getIDOfFolder(const folder: TFolder; const addToDB: Boolean): integer;

        function selectIDOfFolder(const folder: TFolder): integer;
        function insertFolder(const folder: TFolder; const asParentFOlder: Boolean): integer; overload;
        function insertFolder(const folder: TFolder): integer; overload;
        function updateParentFolderID(const folderID, parentFolderID: integer): Boolean;

        // function loadSubFolders(const selectedNodeData: PVNodeData): integer;
        function getFolderFromSubFolderList(const path: string): TFolder;
        function getFolderWithNoFileEntry: string;
    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////

constructor TClassVirtuelDBFolder.create(const DBName: string);
begin
    inherited;

    setLength(folders, 0);
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////

destructor TClassVirtuelDBFolder.destroy;
begin
    setLength(folders, 0);

    inherited;
end;

function TClassVirtuelDBFolder.getIDOfFolder(const folder: TFolder; const addToDB: Boolean): integer;
begin
    result := selectIDOfFolder(folder);

    if (result = 0) and (addToDB) then
    begin
        result := insertFolder(folder);
    end;
end;

function TClassVirtuelDBFolder.selectIDOfFolder(const folder: TFolder): integer;
begin
    // result := 0;
    //
    // if DB.Connected = true then
    // begin
    // q.close;
    // q.SQL.Text := 'SELECT ID FROM FOLDERS WHERE DRIVES_ID =:did AND PATH =:pfad';
    //
    // q.Params[0].AsInteger := folder.DBID.DriveID;
    // q.Params[1].asstring := IncludeTrailingPathDelimiter(folder.pfad);
    //
    // try
    // q.Open;
    //
    // if not q.Eof then
    // begin
    // result := q.Fields[0].AsInteger;
    // end;
    // finally
    // q.SQL.Clear;
    // q.Params.Clear;
    // q.close;
    // end;
    // end;
end;

function TClassVirtuelDBFolder.updateParentFolderID(const folderID, parentFolderID: integer): Boolean;
begin
    q.close;
    q.SQL.Text := 'UPDATE FOLDERS SET PARENT_ID = :pid WHERE ID = :id;';
    q.Params[0].AsInteger := parentFolderID;
    q.Params[1].AsInteger := folderID;

    try
        q.ExecSQL;
        result := true;
    finally
        q.SQL.Clear;
        q.Params.Clear;
        q.close;
    end;

end;

function TClassVirtuelDBFolder.insertFolder(const folder: TFolder; const asParentFOlder: Boolean): integer;
//var
//    ID: integer;
begin
    // ID := getNextIDOfTable('FOLDERS');
    //
    // q.close;
    // q.SQL.Text :=
    // 'INSERT INTO FOLDERS ("ID", "PARENT_ID", "DRIVES_ID", "PATH", "FOLDER", "CREATE_DATE") VALUES (:id, :parentID, :driveID, :pfad, :name, :datum);';
    // q.Params[0].AsInteger := ID;
    // if (asParentFOlder) then
    // begin
    // q.Params[1].AsInteger := ID;
    // end else begin
    // q.Params[1].AsInteger := folder.parentFolderID;
    // end;
    // q.Params[2].AsInteger := folder.DBID.DriveID;
    // q.Params[3].asstring := IncludeTrailingPathDelimiter(folder.pfad);
    // q.Params[4].asstring := ExtractFilename(ExcludeTrailingPathDelimiter(folder.pfad));
    // q.Params[5].AsFloat := folder.folder_created;
    //
    // try
    // q.ExecSQL;
    // result := ID;
    // finally
    // q.SQL.Clear;
    // q.Params.Clear;
    // q.close;
    // end;
end;

function TClassVirtuelDBFolder.insertFolder(const folder: TFolder): integer;
begin
    result := insertFolder(folder, false);
end;

// function TClassVirtuelDBFolder.loadSubFolders(const selectedNodeData: PVNodeData): integer;
// begin
// result := 0;
// setLength(folders, 0);
//
// if DB.Connected then
// begin
// q.close;
// q.SQL.Clear;
//
// q.SQL.Add('SELECT');
// q.SQL.Add('ID,');
// q.SQL.Add('FOLDER,');
// q.SQL.Add('PATH,');
// q.SQL.Add('DELETED,');
// q.SQL.Add('CREATE_DATE');
// q.SQL.Add('FROM FOLDERS');
// q.SQL.Add('WHERE PARENT_ID = :parentID AND ID<>PARENT_ID');
//
// q.Params[0].AsInteger := selectedNodeData.DBID.folderID;
//
// try
// q.Open;
// while not q.Eof do
// begin
// setLength(folders, q.RecNo);
//
// folders[q.RecNo - 1].DBID.folderID := q.Fields[0].AsInteger;
// folders[q.RecNo - 1].DBID.ComputerID := selectedNodeData.DBID.ComputerID;
// folders[q.RecNo - 1].DBID.DriveID := selectedNodeData.DBID.DriveID;
// folders[q.RecNo - 1].parentFolderID := selectedNodeData.DBID.folderID;
// folders[q.RecNo - 1].name := q.Fields[1].asstring;
// folders[q.RecNo - 1].pfad := q.Fields[2].asstring;
// folders[q.RecNo - 1].folder_created := q.Fields[3].AsLargeInt;
//
// folders[q.RecNo - 1].DBID.source := sDataBase;
// folders[q.RecNo - 1].DBID.kind := Kfolder;
//
// result := q.RecNo;
//
// q.Next;
// end;
// result := q.RecNo;
// finally
//
// end;
//
// end;
// end;

function TClassVirtuelDBFolder.getFolderFromSubFolderList(const path: string): TFolder;
//var
//    i: integer;
//    pfad: string;
begin
    // result := getEmptyTFolder;
    // pfad := IncludeTrailingPathDelimiter(path);
    //
    // for i := low(folders) to High(folders) do
    // begin
    // if (folders[i].DBID.source = sDataBase) and (folders[i].pfad = pfad) then
    // begin
    // folders[i].DBID.source := sFilesystem;
    // result := folders[i];
    // break;
    // end;
    // end;
end;

function TClassVirtuelDBFolder.getFolderWithNoFileEntry: string;
//var
//    i: integer;
begin
    // result := '';
    //
    // for i := low(folders) to High(folders) do
    // begin
    // if (folders[i].DBID.source = sDataBase) then
    // begin
    // if result <> '' then
    // result := result + #13;
    //
    // result := result + folders[i].pfad;
    // end;
    // end;
end;

end.
