unit classVirtuelDBHashFile;

interface

uses
    u_DM_type_def,
    classDBBase,
    system.sysUtils,
    system.Classes,
    DateUtils,
    IdHashMessageDigest;

type
    TClassVirtuelDBHashFile = class(TclassDBBase)

    protected
        function getFilePath(const f: TDatei): string;
        function selectFileHash(const f: TDatei): string;
        function storeHash(const f: TDatei; const hash: string): boolean;
        function GetMD5OfFile(const path: string): string;

    public
        function hashFile(const f: TDatei; const maxSize: int64): string;
    end;

implementation

/// ////////////////////////////////////////////////////////////////
/// // Stores and returns the MD5 Hash Value of a given File Record
/// ////////////////////////////////////////////////////////////////
function TClassVirtuelDBHashFile.hashFile(const f: TDatei; const maxSize: int64): string;
var
    hash: string;
begin
    result := '';

    if (DB.Connected = True) then
    begin
        result := selectFileHash(f);

        if (result = '') then
        begin
            if (maxSize = 0) or ((maxSize > 0) and (f.Database.FSize < maxSize)) then
            begin
                hash := GetMD5OfFile(getFilePath(f));

                if storeHash(f, hash) then
                    result := hash;

                hash := '';
            end;
        end;
    end;
end;

/// ///////////////////////////////////////////////////////////////////
/// // Reads the MD5 Hash from the DB by given File Record
/// ///////////////////////////////////////////////////////////////////
function TClassVirtuelDBHashFile.selectFileHash(const f: TDatei): string;
begin
    result := '';

    q.sql.clear;
    q.sql.add('SELECT');
    q.sql.add('CONTENTHASH');
    q.sql.add('FROM FILES');
    q.sql.add('WHERE');
    q.sql.add('FILES.ID=:fid');

    q.params[0].asInteger := f.Database.ID;

    try
        q.open;
        if not q.Eof then
        begin
            result := q.Fields[0].asString;
        end;

        q.sql.clear;
        q.params.clear;

    except
        on E: Exception do
        begin

        end;
    end;
end;

/// /////////////////////////////////////////
/// Returns the Path of a given File Record
/// /////////////////////////////////////////
function TClassVirtuelDBHashFile.getFilePath(const f: TDatei): string;
begin
    result := '';

    begin
        q.sql.clear;
        q.sql.add('SELECT');
        q.sql.add('PATH, FILENAME');
        q.sql.add('FROM FILES');
        q.sql.add('LEFT JOIN FOLDERS ON FOLDERS.ID=FILES.FOLDERS_ID');
        q.sql.add('WHERE');
        q.sql.add('FILES.ID=:fid');
        q.sql.add('AND (FILES.HASH_DATE!=FILES.CHANGE_DATE');
        q.sql.add('OR FILES.HASH_DATE is null)');

        q.params[0].asInteger := f.Database.ID;

        try
            q.open;
            if not q.Eof then
            begin
                result := IncludeTrailingPathDelimiter(q.Fields[0].asString) + q.Fields[1].asString;
            end;
            q.sql.clear;
            q.params.clear;

        except
            on E: Exception do
            begin

            end;
        end;
    end;
end;

/// ///////////////////////////////////////////////////////////////////////
/// // Receives the MD5 Checksum of a given Filename
/// ///////////////////////////////////////////////////////////////////////
function TClassVirtuelDBHashFile.GetMD5OfFile(const path: string): string;
var
    MD5: TIdHashMessageDigest5;
    fs: TFileStream;
begin
    // https://support.microsoft.com/de-de/help/889768/how-to-compute-the-md5-or-sha-1-cryptographic-hash-values-for-a-file
    result := '';

    if FileExists(path) then
    begin
        MD5 := TIdHashMessageDigest5.Create;
        fs := TFileStream.Create(path, fmOpenRead or fmShareDenyWrite);

        try
            result := MD5.HashStreamAsHex(fs);
        finally
            fs.Free;
            MD5.Free;
        end;
    end;

end;

/// /////////////////////////////////////////////////////////////////////////////////////
/// // Stores Hash and a Timestamp
/// /////////////////////////////////////////////////////////////////////////////////////
function TClassVirtuelDBHashFile.storeHash(const f: TDatei; const hash: string): boolean;
begin
    result := false;

    if (hash <> '') then
    begin
        q.sql.clear;
        q.sql.add('UPDATE FILES SET');
        q.sql.add('CONTENTHASH = :ch ,');
        q.sql.add('CHANGE_DATE = :now ,');
        q.sql.add('HASH_DATE = :now2 ');
        q.sql.add('WHERE');
        q.sql.add('FILES.ID = :id;');

        q.params[0].asString := hash;
        q.params[1].AsFloat := DateTimeToUnix(now);
        q.params[2].AsFloat := DateTimeToUnix(now);
        q.params[3].asInteger := f.Database.ID;

        try
            q.ExecSQL;
            q.close;
            result := True;
            q.sql.clear;
            q.params.clear;
        except
            on E: Exception do
            begin

            end;
        end;
    end;
end;

end.
