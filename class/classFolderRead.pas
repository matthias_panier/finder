unit classFolderRead;

interface

uses classDBBase, u_DM_type_def, System.SysUtils, classFolderReadDB, Windows, DateUtils, Forms,
    VirtualTrees, classVirtuelDBHashFile,
    System.Classes,
    IdHashMessageDigest;

type
    TclassFolderRead = class(TclassFolderReadDB)
        FaddFolderCallBack: TCBaddFolder;
        FaddFileCallBack: TCBaddDatei;

        folders: TFolders;
        files: TDateien;

        FCallBackOptions: TOptionsUseCallbacks;
        FPreloadOptions: TOptionsPreload;
        FImportOptions: TOptionsLiveImport;

        FParentNode: PVirtualNode;

        procedure addLocalFoldersToList;
        function getFolderFromTSR(const tsr: TSearchRec): TFolder;
        function checkFolderInDB(folder: TFolder): TFolder;
        function prepareDateiWithTSR(const tsr: TSearchRec): TDatei;
        function checkDateiInDB(datei: TDatei): TDatei;

        function addFolder(const currentFolder: TFolder): integer;
        function addFile(const datei: TDatei): integer;

        function GetMD5OfFile(const path: string): string;
    public
        constructor Create(const dbName: string; const aDataSet: TDataSet; ParentNode: PVirtualNode);
        destructor destroy; override;

        procedure setCallbacks(const addFolderCallback: TCBaddFolder; addFileCallback: TCBaddDatei);
        procedure setParentNode(const ParentNode: PVirtualNode);
        procedure setPreloadOptions(const options: TOptionsPreload);
        procedure setImportOptions(const options: TOptionsLiveImport);
        procedure setCallbackOptions(const CB_Options: TOptionsUseCallbacks);

        procedure readLocalFolder;
        procedure addDBFolders;
        procedure addDBFiles;

        function FileCount: integer;
        function FolderCount: integer;
    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////
constructor TclassFolderRead.Create(const dbName: string; const aDataSet: TDataSet; ParentNode: PVirtualNode);
begin
    inherited Create(dbName, aDataSet);
    setlength(folders, 0);
    setlength(files, 0);

    // store Callbacks
    FCallBackOptions.Drive := false;
    FCallBackOptions.folders := false;
    FCallBackOptions.files := false;

    FImportOptions.folders := false;
    FImportOptions.files.active := false;
    FImportOptions.files.useThread := false;
    FImportOptions.hash.active := false;
    FImportOptions.hash.useThread := false;
    FImportOptions.Exif.active := false;
    FImportOptions.Exif.useThread := false;
    FImportOptions.UseMaxFileSize := false;
    FImportOptions.MaxFileSize := 10000000;

    FPreloadOptions.Drive := false;
    FPreloadOptions.folders := false;
    FPreloadOptions.files := false;

    setParentNode(ParentNode);
end;

procedure TclassFolderRead.setCallbacks(const addFolderCallback: TCBaddFolder; addFileCallback: TCBaddDatei);
begin
    FaddFolderCallBack := addFolderCallback;
    FaddFileCallBack := addFileCallback;
end;

procedure TclassFolderRead.setPreloadOptions(const options: TOptionsPreload);
begin
    FPreloadOptions := options;

    if (FPreloadOptions.folders) then
        loadDBFolders;
    if (FPreloadOptions.files) then
        loadDBFiles;
end;

procedure TclassFolderRead.setCallbackOptions(const CB_Options: TOptionsUseCallbacks);
begin
    FCallBackOptions := CB_Options;
end;

procedure TclassFolderRead.setImportOptions(const options: TOptionsLiveImport);
begin
    FImportOptions := options;
end;

procedure TclassFolderRead.setParentNode(const ParentNode: PVirtualNode);
begin
    // store reference node
    FParentNode := ParentNode;
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////
destructor TclassFolderRead.destroy;
begin
    inherited;
end;

procedure TclassFolderRead.readLocalFolder;
begin
    if (DataSet.folder.source.isFromFilesystem) then
    begin
        addLocalFoldersToList;
    end;
end;

procedure TclassFolderRead.addDBFolders;
var
    i, j: integer;
    l, m: integer;
    found: boolean;
    cf: TFolder;
begin
    m := high(folders);

    for i := 0 to high(DBFolders) do
    begin
        found := false;

        for j := 0 to m do
        begin
            if folders[j].Database.path = DBFolders[i].path then
            begin
                found := true;
                break;
            end;
        end;

        if not found then
        begin
            finalize(cf);

            cf.Database := DBFolders[i];
            cf.DataSet := DataSet;
            cf.DataSet.path := DBFolders[i].path;
            cf.DataSet.kind := getKindRecord(false, false, true, false);

            cf.DataSet.folder.ID := DBFolders[i].ID;
            cf.DataSet.folder.source.isFromDB := true;
            cf.DataSet.folder.source.isFromFilesystem := false;

            addFolder(cf);
        end;
    end;
end;

procedure TclassFolderRead.addDBFiles;
var
    i, j: integer;
    l, m: integer;
    found: boolean;
    cf: TDatei;
begin
    m := high(files);

    for i := 0 to high(DBFiles) do
    begin
        found := false;

        for j := 0 to m do
        begin
            if files[j].Database.Filename = DBFiles[i].Filename then
            begin
                found := true;
                break;
            end;
        end;

        if not found then
        begin
            finalize(cf);

            cf.Database := DBFiles[i];
            cf.DataSet := DataSet;
            cf.DataSet.datei.ID := DBFiles[i].ID;
            cf.DataSet.kind := getKindRecord(false, false, false, true);
            cf.DataSet.datei.source.isFromDB := true;
            cf.DataSet.datei.source.isFromFilesystem := false;

            addFile(cf);
        end;
    end;

end;

function TclassFolderRead.addFolder(const currentFolder: TFolder): integer;
begin
    setlength(folders, length(folders) + 1);
    folders[high(folders)] := currentFolder;

    if (FCallBackOptions.folders) then
    begin
        FaddFolderCallBack(currentFolder, FParentNode);
    end;
end;

function TclassFolderRead.addFile(const datei: TDatei): integer;
begin
    setlength(files, length(files) + 1);
    files[high(files)] := datei;

    if (FCallBackOptions.files) then
    begin
        FaddFileCallBack(datei, FParentNode);
    end;
end;

function TclassFolderRead.getFolderFromTSR(const tsr: TSearchRec): TFolder;
begin
    result.Database.ID := 0;
    result.Database.Parent_ID := 0;
    result.Database.Computer_ID := 0;
    result.Database.Drive_ID := 0;
    result.Database.path := '';
    result.Database.create_date := 0;
    result.Database.Caption := '';
    result.Database.Deleted := 0;
    result.Database.add_date := 0;

    result.imageIndex := 0;

    result.DataSet.path := '';
    result.DataSet.kind.isComputer := false;
    result.DataSet.kind.isDrive := false;
    result.DataSet.kind.isFolder := false;
    result.DataSet.kind.isFile := false;

    result.DataSet.Computer.ID := 0;
    result.DataSet.Computer.source.isFromDB := false;
    result.DataSet.Computer.source.isFromFilesystem := false;
    result.DataSet.Drive.ID := 0;
    result.DataSet.Drive.source.isFromDB := false;
    result.DataSet.Drive.source.isFromFilesystem := false;
    result.DataSet.folder.ID := 0;
    result.DataSet.folder.source.isFromDB := false;
    result.DataSet.folder.source.isFromFilesystem := false;
    result.DataSet.datei.ID := 0;
    result.DataSet.datei.source.isFromDB := false;
    result.DataSet.datei.source.isFromFilesystem := false;

    // sleep(10);

    // parent allocation
    result.Database.ID := 0;
    result.Database.Parent_ID := DataSet.folder.ID;
    result.Database.Computer_ID := DataSet.Computer.ID;
    result.Database.Drive_ID := DataSet.Drive.ID;

    // TSR allocations
    result.Database.path := IncludeTrailingPathDelimiter(DataSet.path) + tsr.Name;
    result.Database.Caption := tsr.Name;
    result.Database.create_date := DateTimeToUnix(tsr.TimeStamp);

    // Icon allocation
    result.imageIndex := getPathIconID(result.Database.path);

    result.DataSet := DataSet;
    result.DataSet.path := result.Database.path;
    result.DataSet.folder.ID := 0;

    // Kind allocation
    result.DataSet.kind := getKindRecord(false, false, true, false);

    // Source allocation
    result.DataSet.folder.source.isFromFilesystem := true;
    result.DataSet.folder.source.isFromDB := false;
end;

function TclassFolderRead.checkFolderInDB(folder: TFolder): TFolder;
begin
    result := folder;

    // Database allocation
    result.Database.ID := getFolderID(result.Database, FPreloadOptions.folders, true, FImportOptions.folders);

    if (result.Database.ID > 0) then
    begin
        result.DataSet.folder.ID := result.Database.ID;
        result.DataSet.folder.source.isFromDB := true;
    end;

end;

function TclassFolderRead.prepareDateiWithTSR(const tsr: TSearchRec): TDatei;
begin
    finalize(result);

    // parent allocation
    result.Database.ID := 0;
    result.Database.Computer_ID := DataSet.Computer.ID;
    result.Database.Drive_ID := DataSet.Drive.ID;
    result.Database.Folder_ID := DataSet.folder.ID;
    result.Database.Deleted := 0;
    result.Database.isOriginal := 1;
    result.Database.isDublette := 0;
    result.Database.isBackup := 0;
    result.Database.isTagged := 0;
    result.Database.Ext_ID := 0;
    result.Database.Icon_ID := 0;
    result.Database.hash_date := 0;
    result.Database.Rating := 0;
    result.Database.add_date := 0;

    // TSR allocation
    result.Database.Filename := tsr.Name;
    result.Database.Caption := tsr.Name;
    result.Database.FSize := tsr.Size;
    result.Database.create_date := DateTimeToUnix(tsr.TimeStamp);

    // Icon allocation
    result.URI := IncludeTrailingPathDelimiter(DataSet.path) + tsr.Name;
    result.imageIndex := getPathIconID(result.URI);

    // Kind allocation
    result.DataSet.kind := getKindRecord(false, false, false, true);

    // Source allocation
    result.DataSet.path := DataSet.path;
    result.DataSet.datei.source.isFromFilesystem := true;
    result.DataSet.datei.source.isFromDB := false;
end;

function TclassFolderRead.checkDateiInDB(datei: TDatei): TDatei;
var
    md5: string;
begin
    result := datei;

    if (FImportOptions.files.active) and (FImportOptions.files.useThread = false) or (datei.Database.Folder_ID > 0) then
    begin
        // Database allocation
        result.Database.ID := getFileID(result.Database, FPreloadOptions.files, FImportOptions.files.active, true);

        if result.Database.ID > 0 then
        begin
            result.DataSet.datei.source.isFromDB := true;

            if (FImportOptions.hash.active) and (FImportOptions.hash.useThread = false) and
                (result.Database.Contenthash = '') then
            begin
                if (FImportOptions.UseMaxFileSize = false) or
                    ((FImportOptions.UseMaxFileSize) and (FImportOptions.MaxFileSize > result.Database.FSize)) then
                begin
                    md5 := GetMD5OfFile(datei.URI);
                    if (md5 <> '') then
                    begin
                        result.Database.Contenthash := md5;
                        result.Database.hash_date := getUnixTimeStamp;

                        // storing
                        updateContentHash(result.Database.ID, md5);
                    end;
                end;
            end;
        end;

    end;
end;

procedure TclassFolderRead.addLocalFoldersToList;
var
    path: string;
    tsr: TSearchRec;
    t1: cardinal;
    datei: TDatei;
    folder: TFolder;
begin
    t1 := starttick;

    path := IncludeTrailingPathDelimiter(DataSet.path);

    if System.SysUtils.FindFirst(path + '*.*', faAnyFile or faDirectory, tsr) = 0 then
    begin
        repeat
            if (tsr.Attr and faDirectory) = 0 then
            begin
                datei := prepareDateiWithTSR(tsr);
                datei := checkDateiInDB(datei);
                addFile(datei);
            end else if (tsr.Name <> '.') and (tsr.Name <> '..') then
            begin
                folder := getFolderFromTSR(tsr);
                folder := checkFolderInDB(folder);
                addFolder(folder);
            end;
        until System.SysUtils.FindNext(tsr) <> 0;

        System.SysUtils.FindClose(tsr);
    end;

    finishtick(t1, 'addLocalFoldersToList', 'Folder Read');

    finalize(folder);
    finalize(datei);
    path := '';
end;

function TclassFolderRead.FileCount: integer;
begin
    result := length(files);
end;

function TclassFolderRead.FolderCount: integer;
begin
    result := length(folders);
end;

function TclassFolderRead.GetMD5OfFile(const path: string): string;
var
    md5: TIdHashMessageDigest5;
    fs: TFileStream;
    uts: int64;
    t1: Cardinal;

begin
    // https://support.microsoft.com/de-de/help/889768/how-to-compute-the-md5-or-sha-1-cryptographic-hash-values-for-a-file
    result := '';

    if FileExists(path) then
    begin
        t1 := starttick;

        md5 := TIdHashMessageDigest5.Create;
        fs := TFileStream.Create(path, fmOpenRead or fmShareDenyWrite);

        try
            result := md5.HashStreamAsHex(fs);
        finally
            fs.Free;
            md5.Free;
            finishtick(t1, 'Creating Hash', path);
        end;
    end;

end;




end.
