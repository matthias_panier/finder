unit classVirtuelDBFile;

interface

uses
    u_DM_type_def,
    classDBBase,
    system.sysutils,
    ClipBrd,
    DateUtils;

type
    TClassVirtuelDBFile = class(TclassDBBase)
        dateien: TDateien;
    private
        // function isChanged(const f, DBf: TDatei): boolean;
        //
        // function selectFileFromDB(const f: TDatei): TDatei;
        // function insertFile(const f: TDatei): TDatei;
        // function updateFileDB(const f: TDatei): boolean;

    protected
    public
        constructor create(const DBName: string);
        destructor destroy; override;

        // function getFileDBContent(const f: TDatei; const addToDB: boolean): TDatei;
        function getDoubletten(const f: TDatei): TDateien;

        // function loadSubFolders(const selectedNodeData: PVNodeData): integer;
        // function getFileFromSubFilesList(const filename: string): TDatei;
    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////
constructor TClassVirtuelDBFile.create(const DBName: string);
begin
    inherited;

    setLength(dateien, 0);
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////
destructor TClassVirtuelDBFile.destroy;
begin
    setLength(dateien, 0);

    inherited;
end;

// function TClassVirtuelDBFile.isChanged(const f, DBf: TDatei): boolean;
// begin
// result := false;
// if (f.Database.FSize <> DBf.Database.FSize) then
// begin
// result := true;
// exit;
// end;
// end;

// function TClassVirtuelDBFile.getFileDBContent(const f: TDatei; const addToDB: boolean): TDatei;
// begin
// result := selectFileFromDB(f);
//
// if DB.Connected = true then
// begin
// result.DBID.ComputerID := f.DBID.ComputerID;
// result.DBID.DriveID := f.DBID.DriveID;
// result.DBID.source := f.DBID.source;
// result.DBID.kind := kfile;
// result.Nodes := f.Nodes;
//
// if (result.DBID.FileID > 0) and (isChanged(f, result)) then
// begin
// updateFileDB(f);
// end;
//
// if (uppercase(f.extension) = '.JPG') then
// begin
// result.Meta.kind := MyKimage;
// end;
//
// if (result.DBID.FileID = 0) and (addToDB) then
// begin
// result := insertFile(f);
// end;
// end;
// end;

// relevant data:
// FOLDER_ID
// filename
// function TClassVirtuelDBFile.selectFileFromDB(const f: TDatei): TDatei;
// begin
// result := getEmptyTFile;

// begin
// q.close;
// q.SQL.Clear;
// q.SQL.add('SELECT');

// IDs
// q.SQL.add('FILES.ID FID,'); // 0
// q.SQL.add('FILES.ORIG_FID,');
// q.SQL.add('FILES.FOLDERS_ID,');
// q.SQL.add('FILES.ICON_ID,'); // not in use
//
// // Content
// q.SQL.add('FILES.CONTENTHASH,'); // 4
// q.SQL.add('FILES.FSIZE,');
// q.SQL.add('FILES.DISPLAY_NAME,');
// q.SQL.add('FILES.EXT,');
// //
// q.SQL.add('FILES.ISORIGINAL,'); // 8
// q.SQL.add('FILES.ISDUBLETTE,');
// q.SQL.add('FILES.ISBACKUP,');
// q.SQL.add('FILES.ISTAGGED,'); // 11
// //
// // time stamps
// q.SQL.add('FILES.CREATE_DATE,'); // 12
// q.SQL.add('FILES.CHANGE_DATE,');
// q.SQL.add('FILES.ACCESS_DATE,');
// q.SQL.add('FILES.HASH_DATE,');
// q.SQL.add('FILES.CREATED,'); // 16
// //
// // image data
// // q.SQL.add('IMGDATA.ID IID,'); // 17
// // q.SQL.add('IMGDATA.CREATE_DATE EXIF_DATE,');
// //
// // q.SQL.add('IMGDATA.LON,'); // 19
// // q.SQL.add('IMGDATA.LAT,');
// // q.SQL.add('IMGDATA.ALT,');
// // q.SQL.add('IMGDATA.X,'); // 22
// // q.SQL.add('IMGDATA.Y,'); // 23
// // q.SQL.add('IMGDATA.CAMERA,');
// // q.SQL.add('IMGDATA.MAKE,'); // 25
// //
// // q.SQL.add('IMGDATA.DESCRIPTION,'); // 26
// // q.SQL.add('IMGDATA.DISPLAY_LON,');
// // q.SQL.add('IMGDATA.DISPLAY_LAT,');
// // q.SQL.add('IMGDATA.DISPLAY_ALT,');
// // q.SQL.add('IMGDATA.DISPLAY_DATE,'); // 29
// //
// // q.SQL.add('IMGDATA.DISPLAY_CAMERA,'); // 30
// // q.SQL.add('IMGDATA.DISPLAY_MAKE,');
// // q.SQL.add('IMGDATA.DISPLAY_DESC,');
// q.SQL.add('IMGDATA.DISPLAY_DATE'); // 33
//
// q.SQL.add('FROM FILES');
// q.SQL.add('LEFT JOIN IMGDATA ');
// q.SQL.add('ON IMGDATA.ID = FILES.ID');
// q.SQL.add('WHERE');
// q.SQL.add('FOLDERS_ID =:fid AND FILENAME =:filename');
//
/// /        q.Params[0].AsInteger := f.DBID.FolderID;
/// /        q.Params[1].asstring := f.filename;
//
// try
/// /            q.Open;
//
/// /            if not q.Eof then
/// /            begin
/// /                // take over from request
/// /                result.DBID.FolderID := f.DBID.FolderID;
/// /                result.filename := f.filename;
/// /                result.extension := f.extension;
/// /                result.pfad := f.pfad;
/// /                result.Flags.isReadFromDB := true;
/// /
/// /                // applay DB results
/// /                result.DBID.FileID := q.Fields[0].AsInteger;
/// /                result.contentHash := q.Fields[4].asstring;
/// /                result.size := q.Fields[5].AsLargeInt;
/// /                result.display.filename := q.Fields[6].asstring;
/// /                result.file_created := q.Fields[12].AsLargeInt;
//
// // if (not q.Fields[17].IsNull) then
// // begin
// // result.Meta.kind := MyKimage;
// // result.Meta.date := q.Fields[18].AsLargeInt;
// // result.Meta.Coordinates.longitude := q.Fields[19].AsFloat;
// // result.Meta.Coordinates.latitude := q.Fields[20].AsFloat;
// // result.Meta.Coordinates.altitude := q.Fields[21].AsInteger;
// // result.Meta.dimX := q.Fields[22].AsInteger;
// // result.Meta.dimY := q.Fields[23].AsInteger;
// // result.Meta.camera := q.Fields[24].asstring;
// // result.Meta.make := q.Fields[25].asstring;
// //
// // result.Meta.display.description := q.Fields[26].asstring;
// // result.Meta.display.Coordinates.longitude := q.Fields[27].AsFloat;
// // result.Meta.display.Coordinates.latitude := q.Fields[28].AsFloat;
// // result.Meta.display.Coordinates.altitude := q.Fields[29].AsInteger;
// // result.Meta.display.make := q.Fields[30].asstring;
// // result.Meta.display.camera := q.Fields[31].asstring;
// // result.Meta.display.description := q.Fields[32].asstring;
// // result.Meta.display.date := q.Fields[33].AsInteger;
// // end;
/// /            end;
// finally
/// /            q.SQL.Clear;
/// /            q.Params.Clear;
// q.close;
// end;
// end;
// end;

// function TClassVirtuelDBFile.updateFileDB(const f: TDatei): boolean;
// begin
// result := true;
// // update size and ggf hash
// end;

// function TClassVirtuelDBFile.insertFile(const f: TDatei): TDatei;
// begin
// result := f;


// result.DBID.FileID := getNextIDOfTable('FILES');
//
// q.close;
// q.SQL.Clear;
// q.SQL.add('INSERT INTO FILES');
// q.SQL.add('("ID", "FOLDERS_ID", "FILENAME", "EXT", "FSIZE","CREATE_DATE", "CREATED", "DISPLAY_NAME")');
// q.SQL.add('VALUES');
// q.SQL.add('(:id, :folderID, :name, :ext, :size, :datum, :created, :displayName);');
//
// q.Params[0].AsInteger := result.DBID.FileID;
// q.Params[1].AsInteger := f.DBID.FolderID;
// q.Params[2].asstring := f.filename;
// q.Params[3].asstring := f.extension;
// q.Params[4].AsLargeInt := f.size;
//
// q.Params[5].AsLargeInt := f.file_created;
// q.Params[6].AsLargeInt := DateTimeToUnix(now);
// q.Params[7].asstring := f.display.filename;
//
// try
// q.ExecSQL;
// finally
// q.SQL.Clear;
// q.Params.Clear;
// q.close;
// end;
// end;

function TClassVirtuelDBFile.getDoubletten(const f: TDatei): TDateien;
begin
    setLength(result, 0);
    //
    // q.close;
    // q.SQL.Clear;
    // q.SQL.add('SELECT');
    // q.SQL.add('FILES.ID FID,'); // 0
    // q.SQL.add('FILES.ORIG_FID,');
    // q.SQL.add('FILES.FOLDERS_ID,'); //
    // q.SQL.add('FILES.ICON_ID,'); //
    //
    // // Content
    // q.SQL.add('FILES.FILENAME,'); // 4
    // q.SQL.add('FILES.DISPLAY_NAME,'); // 5
    // q.SQL.add('FILES.EXT,'); // 6
    // q.SQL.add('FILES.FSIZE,'); // 7
    // q.SQL.add('FILES.CONTENTHASH,'); // 8
    // q.SQL.add('FILES.CREATE_DATE,'); // 9
    //
    // q.SQL.add('FILES.ISORIGINAL,'); // 10
    // q.SQL.add('FILES.ISDUBLETTE,'); // 11
    // q.SQL.add('FILES.ISBACKUP,'); // 12
    // q.SQL.add('FILES.ISTAGGED,'); // 13
    //
    // q.SQL.add('COMPUTER.ID CID,');
    // q.SQL.add('COMPUTER.NAME,');
    //
    // q.SQL.add('DRIVES.ID DID,');
    // q.SQL.add('DRIVES.CAPTION,');
    //
    // q.SQL.add('FOLDERS.PATH,');
    //
    // /// IMAGE DATA
    // q.SQL.add('IMGDATA.ID IID,'); // 9 /// REFERENZ ID if image
    // q.SQL.add('IMGDATA.LON,'); // 10
    // q.SQL.add('IMGDATA.LAT,'); // 11
    // q.SQL.add('IMGDATA.ALT,'); // 12
    // q.SQL.add('IMGDATA.DISPLAY_LON,'); // 13
    // q.SQL.add('IMGDATA.DISPLAY_LAT,'); // 14
    // q.SQL.add('IMGDATA.DISPLAY_ALT,'); // 15
    //
    // q.SQL.add('IMGDATA.CREATE_DATE EXIF_DATE,'); // 16
    // q.SQL.add('IMGDATA.DISPLAY_DATE D_EXIF_DATE,'); // 17
    //
    // q.SQL.add('IMGDATA.CAMERA,'); // 18
    // q.SQL.add('IMGDATA.MAKE,'); // 19
    // q.SQL.add('IMGDATA.DESCRIPTION,'); // 20
    // q.SQL.add('IMGDATA.DISPLAY_CAMERA,'); // 21
    // q.SQL.add('IMGDATA.DISPLAY_MAKE,'); // 22
    // q.SQL.add('IMGDATA.DISPLAY_DESC,'); // 23
    //
    // q.SQL.add('IMGDATA.X,'); // 24
    // q.SQL.add('IMGDATA.Y'); // 25
    //
    // q.SQL.add('');
    // q.SQL.add('');
    // q.SQL.add('');
    // q.SQL.add('');
    //
    // q.SQL.add('FROM FILES');
    //
    // q.SQL.add('LEFT JOIN FOLDERS');
    // q.SQL.add('ON FOLDERS.ID=FILES.FOLDERS_ID');
    // q.SQL.add('LEFT JOIN DRIVES');
    // q.SQL.add('ON DRIVES.ID=FOLDERS.DRIVES_ID');
    // q.SQL.add('LEFT JOIN COMPUTER');
    // q.SQL.add('ON COMPUTER.ID=DRIVES.COMPUTER_ID');
    // q.SQL.add('LEFT JOIN IMGDATA ');
    // q.SQL.add('ON IMGDATA.ID = FILES.ID');
    //
    // q.SQL.add('WHERE');
    // q.SQL.add('CONTENTHASH=:hash AND FSIZE=:fsize AND FILES.ID != :id');
    //
    // q.Params[0].asstring := f.contentHash;
    // q.Params[1].AsLargeInt := f.size;
    // q.Params[2].AsInteger := f.DBID.FileID;
    //
    // try
    // q.Open;
    // while not q.Eof do
    // begin
    // setLength(result, q.RecNo);
    //
    // result[q.RecNo - 1].Flags.isReadFromDB := true;
    //
    // // take over from request
    // result[q.RecNo - 1].DBID.kind := kfile;
    // result[q.RecNo - 1].DBID.source := sDatabase;
    //
    // // applay DB results
    // result[q.RecNo - 1].DBID.FileID := q.FieldByName('FID').AsInteger;
    // result[q.RecNo - 1].DBID.ComputerID := q.FieldByName('CID').AsInteger;
    // result[q.RecNo - 1].DBID.DriveID := q.FieldByName('DID').AsInteger;
    // result[q.RecNo - 1].DBID.FolderID := q.FieldByName('FOLDERS_ID').AsInteger;
    //
    // result[q.RecNo - 1].filename := q.FieldByName('FILENAME').asstring;
    // result[q.RecNo - 1].display.filename := q.FieldByName('DISPLAY_NAME').asstring;
    // result[q.RecNo - 1].extension := q.FieldByName('EXT').asstring;
    // result[q.RecNo - 1].size := q.FieldByName('FSIZE').AsLargeInt;
    // result[q.RecNo - 1].contentHash := q.FieldByName('CONTENTHASH').asstring;
    // result[q.RecNo - 1].file_created := q.FieldByName('CREATE_DATE').AsLargeInt;
    //
    // result[q.RecNo - 1].pfad := q.FieldByName('PATH').asstring;
    // result[q.RecNo - 1].pfad_extended := q.FieldByName('PATH').asstring + q.FieldByName('FILENAME').asstring;
    //
    // result[q.RecNo - 1].Flags.isOriginal := q.FieldByName('ISORIGINAL').AsInteger > 0;
    // result[q.RecNo - 1].Flags.isDublette := q.FieldByName('ISDUBLETTE').AsInteger > 0;
    // result[q.RecNo - 1].Flags.isBackup := q.FieldByName('ISBACKUP').AsInteger > 0;
    // result[q.RecNo - 1].Flags.isTagged := q.FieldByName('ISTAGGED').AsInteger > 0;
    //
    // if not q.FieldByName('IID').IsNull then
    // begin
    // result[q.RecNo - 1].Flags.isImage := true;
    // result[q.RecNo - 1].Meta.kind := MyKimage;
    // result[q.RecNo - 1].Meta.dimX := q.FieldByName('X').AsInteger;
    // result[q.RecNo - 1].Meta.dimY := q.FieldByName('Y').AsInteger;
    //
    // result[q.RecNo - 1].Meta.Coordinates.longitude := q.FieldByName('LON').AsFloat;
    // result[q.RecNo - 1].Meta.Coordinates.latitude := q.FieldByName('LAT').AsFloat;
    // result[q.RecNo - 1].Meta.Coordinates.altitude := q.FieldByName('ALT').AsInteger;
    // result[q.RecNo - 1].Meta.display.Coordinates.longitude := q.FieldByName('DISPLAY_LON').AsFloat;
    // result[q.RecNo - 1].Meta.display.Coordinates.latitude := q.FieldByName('DISPLAY_LAT').AsFloat;
    // result[q.RecNo - 1].Meta.display.Coordinates.altitude := q.FieldByName('DISPLAY_ALT').AsInteger;
    //
    // result[q.RecNo - 1].Meta.date := q.FieldByName('EXIF_DATE').AsLargeInt;
    // result[q.RecNo - 1].Meta.display.date := q.FieldByName('D_EXIF_DATE').AsLargeInt;
    //
    // result[q.RecNo - 1].Meta.camera := q.FieldByName('CAMERA').asstring;
    // result[q.RecNo - 1].Meta.make := q.FieldByName('MAKE').asstring;
    // result[q.RecNo - 1].Meta.description := q.FieldByName('DESCRIPTION').asstring;
    // result[q.RecNo - 1].Meta.display.camera := q.FieldByName('DISPLAY_CAMERA').asstring;
    // result[q.RecNo - 1].Meta.display.make := q.FieldByName('DISPLAY_MAKE').asstring;
    // result[q.RecNo - 1].Meta.display.description := q.FieldByName('DISPLAY_DESC').asstring;
    // end;
    //
    // // result[q.RecNo - 1].DBID.ComputerID := q.Fields[18].AsInteger;
    // // result[q.RecNo - 1].DBID.FileID := q.Fields[0].AsInteger;
    // // result[q.RecNo - 1].DBID.DriveID := q.Fields[5].AsInteger;
    // // result[q.RecNo - 1].DBID.FolderID := q.Fields[6].AsInteger;
    // // result[q.RecNo - 1].DBID.kind := kfile;
    // // result[q.RecNo - 1].DBID.source := sDatabase;
    //
    // // result[q.RecNo - 1].filename := q.Fields[1].asstring;
    // // result[q.RecNo - 1].size := q.Fields[2].AsLargeInt;
    // // result[q.RecNo - 1].contentHash := q.Fields[3].asstring;
    // // result[q.RecNo - 1].pfad := IncludeTrailingPathDelimiter(q.Fields[4].asstring);
    // // result[q.RecNo - 1].pfad_extended := q.Fields[16].asstring + ' - ' + q.Fields[17].asstring;
    // // result[q.RecNo - 1].file_created := q.Fields[19].AsLargeInt;
    //
    // // if (not q.Fields[7].IsNull) then
    // // begin
    // // result[q.RecNo - 1].Meta.kind := MyKimage;
    // // result[q.RecNo - 1].Meta.Coordinates.longitude := q.Fields[8].AsFloat;
    // // result[q.RecNo - 1].Meta.Coordinates.latitude := q.Fields[9].AsFloat;
    // // result[q.RecNo - 1].Meta.Coordinates.altitude := q.Fields[10].AsInteger;
    // // result[q.RecNo - 1].Meta.dimX := q.Fields[11].AsInteger;
    // // result[q.RecNo - 1].Meta.dimY := q.Fields[12].AsInteger;
    // // result[q.RecNo - 1].Meta.camera := q.Fields[13].asstring;
    // // result[q.RecNo - 1].Meta.make := q.Fields[14].asstring;
    // // result[q.RecNo - 1].Meta.date := q.Fields[15].AsLargeInt
    // // end;
    //
    // result[q.RecNo - 1].Nodes := f.Nodes;
    // q.Next;
    // end;
    // finally
    // q.SQL.Clear;
    // q.Params.Clear;
    // q.close;
    // end;
end;

// function TClassVirtuelDBFile.loadSubFolders(const selectedNodeData: PVNodeData): integer;
// begin
// result := 0;
// setLength(dateien, 0);
//
// if DB.Connected then
// begin
// q.close;
// q.SQL.Clear;
// q.SQL.add('SELECT');
//
// // IDs
// q.SQL.add('FILES.ID FID,'); // 0
// q.SQL.add('FILES.ORIG_FID,');
// q.SQL.add('FILES.FOLDERS_ID,'); // doppelt, weil suchkriterum
// q.SQL.add('FILES.ICON_ID,'); // not in use
//
// // Content
// q.SQL.add('FILES.FILENAME,'); // 4
// q.SQL.add('FILES.DISPLAY_NAME,'); // 5
// q.SQL.add('FILES.EXT,'); // 6
// q.SQL.add('FILES.FSIZE,'); // 7
// q.SQL.add('FILES.CONTENTHASH,'); // 8
// q.SQL.add('FILES.CREATE_DATE,'); // 9
//
// q.SQL.add('FILES.ISORIGINAL,'); // 10
// q.SQL.add('FILES.ISDUBLETTE,'); // 11
// q.SQL.add('FILES.ISBACKUP,'); // 12
// q.SQL.add('FILES.ISTAGGED,'); // 13
//
// /// IMAGE DATA
// q.SQL.add('IMGDATA.ID IID,'); // 9 /// REFERENZ ID if image
// q.SQL.add('IMGDATA.LON,'); // 10
// q.SQL.add('IMGDATA.LAT,'); // 11
// q.SQL.add('IMGDATA.ALT,'); // 12
// q.SQL.add('IMGDATA.DISPLAY_LON,'); // 13
// q.SQL.add('IMGDATA.DISPLAY_LAT,'); // 14
// q.SQL.add('IMGDATA.DISPLAY_ALT,'); // 15
//
// q.SQL.add('IMGDATA.CREATE_DATE EXIF_DATE,'); // 16
// q.SQL.add('IMGDATA.DISPLAY_DATE D_EXIF_DATE,'); // 17
//
// q.SQL.add('IMGDATA.CAMERA,'); // 18
// q.SQL.add('IMGDATA.MAKE,'); // 19
// q.SQL.add('IMGDATA.DESCRIPTION,'); // 20
// q.SQL.add('IMGDATA.DISPLAY_CAMERA,'); // 21
// q.SQL.add('IMGDATA.DISPLAY_MAKE,'); // 22
// q.SQL.add('IMGDATA.DISPLAY_DESC,'); // 23
//
// q.SQL.add('IMGDATA.X,'); // 24
// q.SQL.add('IMGDATA.Y'); // 25
//
// q.SQL.add('FROM FILES');
// q.SQL.add('LEFT JOIN IMGDATA ');
// q.SQL.add('ON IMGDATA.ID = FILES.ID');
// q.SQL.add('WHERE');
// q.SQL.add('FOLDERS_ID =:fid');
//
// q.Params[0].AsInteger := selectedNodeData.DBID.FolderID;
//
// try
// q.Open;
//
// while not q.Eof do
// begin
// setLength(dateien, q.RecNo);
//
// dateien[q.RecNo - 1].Flags.isReadFromDB := true;
//
// // take over from request
// dateien[q.RecNo - 1].DBID.kind := kfile;
// dateien[q.RecNo - 1].DBID.source := sDatabase;
// dateien[q.RecNo - 1].DBID.ComputerID := selectedNodeData.DBID.ComputerID;
// dateien[q.RecNo - 1].DBID.DriveID := selectedNodeData.DBID.DriveID;
// dateien[q.RecNo - 1].DBID.FolderID := selectedNodeData.DBID.FolderID;
//
// dateien[q.RecNo - 1].pfad := selectedNodeData.pfad;
//
// // applay DB results
// dateien[q.RecNo - 1].DBID.FileID := q.FieldByName('FID').AsInteger;
//
// dateien[q.RecNo - 1].filename := q.FieldByName('FILENAME').asstring;
// dateien[q.RecNo - 1].display.filename := q.FieldByName('DISPLAY_NAME').asstring;
// dateien[q.RecNo - 1].extension := q.FieldByName('EXT').asstring;
// dateien[q.RecNo - 1].size := q.FieldByName('FSIZE').AsLargeInt;
// dateien[q.RecNo - 1].contentHash := q.FieldByName('CONTENTHASH').asstring;
// dateien[q.RecNo - 1].file_created := q.FieldByName('CREATE_DATE').AsLargeInt;
//
// dateien[q.RecNo - 1].Flags.isOriginal := q.FieldByName('ISORIGINAL').AsInteger > 0;
// dateien[q.RecNo - 1].Flags.isDublette := q.FieldByName('ISDUBLETTE').AsInteger > 0;
// dateien[q.RecNo - 1].Flags.isBackup := q.FieldByName('ISBACKUP').AsInteger > 0;
// dateien[q.RecNo - 1].Flags.isTagged := q.FieldByName('ISTAGGED').AsInteger > 0;
//
// if not q.FieldByName('IID').IsNull then
// begin
// dateien[q.RecNo - 1].Flags.isImage := true;
// dateien[q.RecNo - 1].Meta.kind := MyKimage;
// dateien[q.RecNo - 1].Meta.dimX := q.FieldByName('X').AsInteger;
// dateien[q.RecNo - 1].Meta.dimY := q.FieldByName('Y').AsInteger;
//
// dateien[q.RecNo - 1].Meta.Coordinates.longitude := q.FieldByName('LON').AsFloat;
// dateien[q.RecNo - 1].Meta.Coordinates.latitude := q.FieldByName('LAT').AsFloat;
// dateien[q.RecNo - 1].Meta.Coordinates.altitude := q.FieldByName('ALT').AsInteger;
// dateien[q.RecNo - 1].Meta.display.Coordinates.longitude := q.FieldByName('DISPLAY_LON').AsFloat;
// dateien[q.RecNo - 1].Meta.display.Coordinates.latitude := q.FieldByName('DISPLAY_LAT').AsFloat;
// dateien[q.RecNo - 1].Meta.display.Coordinates.altitude := q.FieldByName('DISPLAY_ALT').AsInteger;
//
// dateien[q.RecNo - 1].Meta.date := q.FieldByName('EXIF_DATE').AsLargeInt;
// dateien[q.RecNo - 1].Meta.display.date := q.FieldByName('D_EXIF_DATE').AsLargeInt;
//
// dateien[q.RecNo - 1].Meta.camera := q.FieldByName('CAMERA').asstring;
// dateien[q.RecNo - 1].Meta.make := q.FieldByName('MAKE').asstring;
// dateien[q.RecNo - 1].Meta.description := q.FieldByName('DESCRIPTION').asstring;
// dateien[q.RecNo - 1].Meta.display.camera := q.FieldByName('DISPLAY_CAMERA').asstring;
// dateien[q.RecNo - 1].Meta.display.make := q.FieldByName('DISPLAY_MAKE').asstring;
// dateien[q.RecNo - 1].Meta.display.description := q.FieldByName('DISPLAY_DESC').asstring;
// end;
//
// result := q.RecNo;
//
// q.Next;
// end;
// result := q.RecNo;
// finally
// q.SQL.Clear;
// q.Params.Clear;
// q.close;
// end;
// end;
// end;

// function TClassVirtuelDBFile.getFileFromSubFilesList(const filename: string): TDatei;
// var
// i: integer;
// begin
// result := getEmptyTFile;

// for i := low(dateien) to high(dateien) do
// begin
// if (dateien[i].DBID.source = sDatabase) and (dateien[i].filename = filename) then
// begin
// dateien[i].DBID.source := sFileSystem;
// result := dateien[i];
// break;
// end;
// end;
// end;

end.
