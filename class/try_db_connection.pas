unit try_db_connection;

interface

uses Windows, Registry, sysUtils, IBX.IBDatabase;

type
    TTryConnectDB = class
    private
        sLastErrorMsg: string;
        sPassword: string;
        sUsername: string;
        procedure Setpassword(const Value: string);
        procedure Setusername(const Value: string);
        function pathIsValid(const path: string): boolean;
    public
        constructor Create;
        destructor Destroy; override;
        procedure clear;
        function tryConnectToDBByPath(const path: string): boolean;
        property lastErrorMsg: string read sLastErrorMsg;
        property password: string read sPassword write Setpassword;
        property username: string read sUsername write Setusername;
    end;

implementation

resourcestring
    SDBFileExtension = '.fdb';
    SParamUserName = 'user_name=';
    SParamPassword = 'password=';
    SParamCType = 'lc_ctype=UTF8';

    /// ///////////////////////////////////////////
    /// /// On Create
    /// ///////////////////////////////////////////
constructor TTryConnectDB.Create;
begin
    inherited;
    clear;
end;

/// ///////////////////////////////////////////
/// /// On Destroy
/// ///////////////////////////////////////////
destructor TTryConnectDB.Destroy;
begin
    clear;
    inherited;
end;

/// ///////////////////////////////////////////
/// /// Clear string variables
/// ///////////////////////////////////////////
procedure TTryConnectDB.clear;
begin
    sPassword := '';
    sUsername := '';
    sLastErrorMsg := '';
end;

/// ///////////////////////////////////////////
/// /// Check if path is valid
/// ///////////////////////////////////////////
function TTryConnectDB.pathIsValid(const path: string): boolean;
begin
    result := (ExtractFileName(path) <> '') and (lowercase(ExtractFileExt(path)) = SDBFileExtension);
end;

/// ///////////////////////////////////////////
/// /// Set Password
/// ///////////////////////////////////////////
procedure TTryConnectDB.Setpassword(const Value: string);
begin
    sPassword := Value;
end;

/// ///////////////////////////////////////////
/// /// Set Username
/// ///////////////////////////////////////////
procedure TTryConnectDB.Setusername(const Value: string);
begin
    sUsername := Value;
end;

/// ///////////////////////////////////////////
/// /// Try to connect via given db-path
/// ///////////////////////////////////////////
function TTryConnectDB.tryConnectToDBByPath(const path: string): boolean;
var
    d: TIBDatabase;
begin
    result := false;
    sLastErrorMsg := '';

    if (pathIsValid(path)) then
    begin
        d := TIBDatabase.Create(nil);
        try
            try
                d.connected := false;
                d.LoginPrompt := false;

                d.close;
                d.connected := false;
                d.Params.clear;
                d.Params.Add(SParamUserName + sUsername);
                d.Params.Add(SParamPassword + sPassword);
                d.Params.Add(SParamCType);

                d.DatabaseName := path;
                d.Open;

                result := d.connected;

                d.close;
            except
                on E: Exception do
                    sLastErrorMsg := E.Message;
            end;
        finally
            d.free;
        end;
    end;
end;

end.
