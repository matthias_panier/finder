unit classVSTHandling;

interface

uses SysUtils, Classes, VirtualTrees, u_DM_type_def, classDBSet;

type
    TClassVSTHandling = class

    public
        function isPathInSubnodes(VST: TVirtualStringTree; ParentNode: PVirtualNode; path: string): boolean;

        function isDriveInNode(VST: TVirtualStringTree; ParentNode: PVirtualNode; Drive: TDrive): boolean;
        function isFolderInNode(VST: TVirtualStringTree; ParentNode: PVirtualNode; Folder: TFolder): boolean;

        function addComputer(VST: TVirtualStringTree; var computer: TComputer): boolean;
        function addDrive(VST: TVirtualStringTree; ParentNode: PVirtualNode; var Drive: TDrive): boolean;

        function addFolder(VST: TVirtualStringTree; ParentNode: PVirtualNode; var Folder: TFolder): PVirtualNode;
        function addFile(const VST: TVirtualStringTree; ParentNode: PVirtualNode; var f: TDatei): PVirtualNode;

        // function setFolderID(const VST: TVirtualStringTree; const Node: PVirtualNode; const FID: integer): boolean;
        // function setHash(const VST: TVirtualStringTree; const Node: PVirtualNode; const newHash: string): boolean;
        function setMeta(const VST: TVirtualStringTree; const Node: PVirtualNode; const meta: TMeta): boolean;
        function setDBLookupResponse(const VST: TVirtualStringTree; const Node: PVirtualNode; const f: TDatei): boolean;

        // function addFilter(const VST: TVirtualStringTree; const ParentNode: PVirtualNode; const vData: TVNodeData)
        // : PVirtualNode;

        function checkFolderExists(VST: TVirtualStringTree; const SelectedNode: PVirtualNode): integer;
        function GetItem(VST: TBaseVirtualTree; Node: PVirtualNode): TclassDBSet;
    end;

implementation

// ////////////////////////////////////////// //
// Checks if a path available is in a subnode //
// ////////////////////////////////////////// //
function TClassVSTHandling.isPathInSubnodes(VST: TVirtualStringTree; ParentNode: PVirtualNode; path: string): boolean;
// var
// NodeData: PVNOdeData;
// ChildNode: PVirtualNode;
begin
    Result := false;

    // if (ParentNode <> nil) then
    // begin
    // if (ParentNode.ChildCount > 0) then
    // begin
    // ChildNode := ParentNode.FirstChild;
    //
    // while ChildNode <> nil do
    // begin
    // NodeData := ChildNode.GetData;
    //
    // if NodeData.pfad = path then
    // begin
    // Result := true;
    // break;
    // end;
    //
    // ChildNode := VST.GetNextSibling(ChildNode);
    // end;
    // end;
    // end;
end;

function TClassVSTHandling.isFolderInNode(VST: TVirtualStringTree; ParentNode: PVirtualNode; Folder: TFolder): boolean;
var
    ChildNode: PVirtualNode;
    item: TclassDBSet;
begin
    Result := false;

    if (ParentNode <> nil) then
    begin
        if (ParentNode.ChildCount > 0) then
        begin
            ChildNode := ParentNode.FirstChild;

            while ChildNode <> nil do
            begin
                item := GetItem(VST, ChildNode);

                if // (Folder.Database.ID = 0) and
                    (Folder.Database.path = item.FullPath) then
                begin
                    Result := true;
                    break;
                end;

                ChildNode := VST.GetNextSibling(ChildNode);
            end;
        end;
    end;

end;

function TClassVSTHandling.isDriveInNode(VST: TVirtualStringTree; ParentNode: PVirtualNode; Drive: TDrive): boolean;
var
    ChildNode: PVirtualNode;
    item: TclassDBSet;
begin
    Result := false;

    if (ParentNode <> nil) then
    begin
        if (ParentNode.ChildCount > 0) then
        begin
            ChildNode := ParentNode.FirstChild;

            while ChildNode <> nil do
            begin
                item := GetItem(VST, ChildNode);

                if (Drive.Database.ID = 0) and (Drive.Database.pfad = item.FullPath) then
                begin
                    Result := true;
                    break;
                end;

                if (item.ID = Drive.Database.ID) and (item.caption = Drive.Database.caption) then
                begin
                    Result := true;
                    break;
                end;

                ChildNode := VST.GetNextSibling(ChildNode);
            end;
        end;
    end;
end;

// ////////////////////////////////////// //
// Adds the Main Node COMPUTER to the VST //
// ////////////////////////////////////// //
function TClassVSTHandling.addComputer(VST: TVirtualStringTree; var computer: TComputer): boolean;
var
    DBSet: TclassDBSet;
begin
    DBSet := TclassDBSet.create;
    Result := DBSet.setComputer(computer);

    VST.InsertNode(nil, amAddChildLast, DBSet);
end;

// ////////////////////////////// //
// Adds the Node DRIVE to the VST //
// ////////////////////////////// //
function TClassVSTHandling.addDrive(VST: TVirtualStringTree; ParentNode: PVirtualNode; var Drive: TDrive): boolean;
var
    DBSet: TclassDBSet;
begin
    Result := false;

    if (isDriveInNode(VST, ParentNode, Drive)) then
        exit;

    DBSet := TclassDBSet.create;
    Result := DBSet.setDrive(Drive);

    VST.InsertNode(ParentNode, amAddChildLast, DBSet);
end;

// ///////////////////////////// //
// Adds a FOLDER Node to the VST //
// ///////////////////////////// //
function TClassVSTHandling.addFolder(VST: TVirtualStringTree; ParentNode: PVirtualNode; var Folder: TFolder): PVirtualNode;
var
    DBSet: TclassDBSet;
begin
    Result := nil;

    if (isFolderInNode(VST, ParentNode, Folder)) then
        exit;

    DBSet := TclassDBSet.create;
    DBSet.setFolder(Folder);

    result := VST.InsertNode(ParentNode, amAddChildLast, DBSet);
end;

// /////////////////////////// //
// Adds a FILE Node to the VST //
// /////////////////////////// //
function TClassVSTHandling.addFile(const VST: TVirtualStringTree; ParentNode: PVirtualNode; var f: TDatei): PVirtualNode;
var
    DBSet: TclassDBSet;
begin
    DBSet := TclassDBSet.create;
    DBSet.setFile(f);

    result := VST.InsertNode(ParentNode, amAddChildLast, DBSet);
end;

// /////////////////////////////////////// //
// Updates a Node with the given Folder ID //
// /////////////////////////////////////// //
// function TClassVSTHandling.setFolderID(const VST: TVirtualStringTree; const Node: PVirtualNode;
// const FID: integer): boolean;
// begin
// GetItem(VST,Node).setFolderID(FID);
// end;

// ////////////////////////////////// //
// Updates a Node with the given HASH //
// ////////////////////////////////// //
// function TClassVSTHandling.setHash(const VST: TVirtualStringTree; const Node: PVirtualNode;
// const newHash: string): boolean;
// var
// Data: PVNOdeData;

// begin
// Result := false;

// if Node <> nil then
// begin
// Data := VST.GetNodeData(Node);
// Data.hash := newHash;

// VST.RepaintNode(Node);

// Result := true;
// end;
// end;

// ////////////////////////////////// //
// Updates a Node with the given META //
// ////////////////////////////////// //
function TClassVSTHandling.setMeta(const VST: TVirtualStringTree; const Node: PVirtualNode; const meta: TMeta): boolean;
// var
// Data: PVNOdeData;
begin
    Result := false;

    // if Node <> nil then
    // begin
    // Data := VST.GetNodeData(Node);
    // Data.meta := meta;
    //
    // VST.RepaintNode(Node);
    //
    // Result := true;
    // end;
end;

// ////////////////////////////////// //
// Updates a Node with the given FILE //
// ////////////////////////////////// //
function TClassVSTHandling.setDBLookupResponse(const VST: TVirtualStringTree; const Node: PVirtualNode;
    const f: TDatei): boolean;
// var
// Data: PVNOdeData;
begin
    Result := false;

    // if Node <> nil then
    // begin
    // Data := VST.GetNodeData(Node);
    // Data.dbID := f.dbID;
    // Data.hash := f.contentHash;
    // Data.meta := f.meta;
    //
    // VST.RepaintNode(Node);
    //
    // Result := true;
    // end;
end;

// function TClassVSTHandling.addFilter(const VST: TVirtualStringTree; const ParentNode: PVirtualNode;
// const vData: TVNodeData): PVirtualNode;
// var
// Data: PVNOdeData;
// begin
//
// Result := VST.AddChild(ParentNode);
// Data := VST.GetNodeData(Result);
//
// Data^ := vData;
// end;

function TClassVSTHandling.checkFolderExists(VST: TVirtualStringTree; const SelectedNode: PVirtualNode): integer;
// var
// NodeData: PVNOdeData;
// ChildNode: PVirtualNode;
begin
    Result := 0;

    // if (SelectedNode <> nil) then
    // begin
    // if (SelectedNode.ChildCount > 0) then
    // begin
    // ChildNode := SelectedNode.FirstChild;
    //
    // while ChildNode <> nil do
    // begin
    // NodeData := ChildNode.GetData;
    //
    // if (NodeData.dbID.kind = kFolder) and (not DirectoryExists(NodeData.pfad)) then
    // begin
    // VST.DeleteNode(ChildNode);
    // Result := Result + 1;
    // end;
    //
    // ChildNode := VST.GetNextSibling(ChildNode);
    // end;
    // end;
    // end;
end;

function TClassVSTHandling.GetItem(VST: TBaseVirtualTree; Node: PVirtualNode): TclassDBSet;
var
    NodeData: Pointer;
    DBSet: TclassDBSet;
begin
    Result := nil;
    if not Assigned(Node) then
    begin
        Result := TclassDBSet.create;
        exit;
    end;

    NodeData := VST.GetNodeData(Node);

    if Assigned(NodeData) then
    begin
        Result := TclassDBSet(NodeData^);
    end else begin
        Result := TclassDBSet.create;
    end;
end;

end.
