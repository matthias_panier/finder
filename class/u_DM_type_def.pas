unit u_DM_type_def;

// http://www.alberton.info/firebird_sql_meta_info.html
interface

uses Controls, Graphics, ExtCtrls, Forms, classes, Contnrs, SysUtils, Messages, VirtualTrees, CCR.Exif, Windows,
    CCR.Exif.TiffUtils, ShellAPI;

const
    WM_AFTER_SHOW = WM_USER + 300; // custom message
    WM_AFTER_CREATE = WM_USER + 301; // custom message

    T_COMPUTER = 'COMPUTER';
    T_LAUFWERK = 'DRIVE';
    T_ORDNER = 'FOLDER';
    T_DATEIEN = 'FILE';
    T_ERWEITERUNGEN = 'EXT';
    T_ICON = 'ICON';
    T_TAG = 'TAG';
    T_KATEGORIE = 'KATEGORIE';
    T_BILDER = 'IMGDATA';
    T_CAMMANUFACTOR = 'CAMMAN';
    T_CAMMODELL = 'CAMMOD';

type
    TMyCallBack = function(const s: string): boolean; stdcall;

type
    TQueryTick = record
        funktion: string;
        msg: string;
        start: Int64;
        finish: Int64;
    end;

type
    TQueryTicks = array of TQueryTick;

type
    TFilesListEntry = record
        mask: string;
        minSize: integer;
        maxSize: integer;
    end;

type
    TLogSources = (lsNone, lsMain, lsDm, lsDb, lsSystem, lsThread);
    TLogLevel = (llNone, llInfo, llWarning, llError, llException);

    TDataSource = record
        isFromDB: boolean;
        isFromFilesystem: boolean;
    end;

    TDataKind = record
        isComputer, isDrive, isFolder, isFile: boolean;
    end;

    TMetaKind = (MyKnone, MyKaudio, MyKvideo, MyKimage);

    TMyNodeLinks = record
        Vfolder: PVirtualNode;
        Vfiles: PVirtualNode;
        VParent: PVirtualNode;
    end;

type
    /// /////////////////////////////
    /// / Database corresponding ////
    /// /////////////////////////////
    TDBCAMMAN = record
        ID: integer;
        Manufactor: string; // 64 Zeichen
    end;

    TDBCamMod = record
        ID: integer;
        Modell: string; // 64 Zeichen
    end;

    TDBComputer = record
        ID: integer;
        name: string; // 32

        add_date: Int64;
    end;

    TDBDrive = record
        ID: integer;
        Computer_ID: integer;
        Folder_ID: integer;

        DSize: Int64;
        SerialNR: string; // 32
        Caption: string; // 64

        add_date: Int64;

        pfad: string; // no db representation in db.drives
        folder: string; // no db representation in db.drives
    end;

    TDBFolder = record
        ID: integer;
        Parent_ID: integer;
        Computer_ID: integer;
        Drive_ID: integer;

        Path: string; // full path    // 255
        create_date: Int64; // create date of folder

        Caption: string; // name  //128
        Deleted: integer;
        add_date: Int64;
    end;

    TDBFile = record
        ID: integer;
        Computer_ID: integer;
        Drive_ID: integer;
        Folder_ID: integer;
        Icon_ID: integer;
        Ext_ID: integer;
        Rating: integer;
        extension: string; // not in DB.File

        Filename: string; // 150
        Caption: string; // 150

        Contenthash: string; // 64

        FSize: Int64;

        isOriginal: integer;
        isDublette: integer;
        isBackup: integer;
        isTagged: integer;
        Deleted: integer;

        create_date: Int64; // file create date
        hash_date: Int64; // date when file was hashed

        add_date: Int64; // create Date in Database
    end;

    TDBExt = record
        ID: integer;
        ext: string; // 32
    end;

    TDBImgData = record
        ID: integer; // same as file_ID
        add_date: Int64; // when added to DB
        change_date: Int64; // when changed in DB

        exif_date: Int64;
        exif_camman_id: integer;
        exif_cammod_id: integer;
        exif_desc: string; // 250
        exif_lat: real;
        exif_lon: real;
        exif_alt: integer;
        exif_x: integer;
        exif_y: integer;
        file_x: integer;
        file_y: integer;

        date: Int64;
        camman_id: integer; // camman_ID
        cammod_id: integer; // cammod_ID
        desc: string; // 250

        x: integer;
        y: integer;
        alt: integer;
        lat: real;
        lon: real;
    end;

    TDataSet = record
        Path: string;
        kind: TDataKind;

        Computer: record
            ID: integer;
            source: TDataSource;
        end;

        Drive: record
            ID: integer;
            source: TDataSource;
        end;

        folder: record
            ID: integer;
            source: TDataSource;
        end;

        datei: record
            ID: integer;
            source: TDataSource;
        end;
    end;

    TComputer = record
        Database: TDBComputer;
        DataSet: TDataSet;

        imageIndex: integer;
    end;

    TDrive = record
        Database: TDBDrive;
        DataSet: TDataSet;

        imageIndex: integer;
    end;

    TFolder = record
        Database: TDBFolder;
        DataSet: TDataSet;

        imageIndex: integer;
    end;

    TDatei = record
        Database: TDBFile;
        DataSet: TDataSet;
        URI:string;

        imageIndex: integer;
    end;

    TComputers = array of TComputer;
    TDrives = array of TDrive;
    TFolders = array of TFolder;
    TDateien = array of TDatei;

    TGeoCoordinates = record
        longitude, latitude: Extended;
        altitude: integer;
    end;

    TThreadFolder = record
        folder: TFolder;

        nodes: record
            Vfolder, Vfiles: PVirtualNode;
        end;
    end;

    TMetaDisplay = record
        Description: string;
        date: Int64;
        Camera: string;
        Make: string;
        Coordinates: TGeoCoordinates;
    end;

    TMeta = record
        kind: TMetaKind;
        dimX: integer;
        dimY: integer;
        DPIX: integer;
        DPIY: integer;
        Description: string;
        Camera: string;
        Make: string;
        date: Int64;
        Coordinates: TGeoCoordinates;
        display: TMetaDisplay;

        imageIndex: integer; // windows image index
        name: string; // DB.computer.name[32]
    end;

    { LOG }
    TLogEntry = record
        msg: string;
        timestamp: double;
        source: TLogSources;
        level: TLogLevel;
    end;

    TLog = record
        position: integer;
        laenge: integer;
        entry: array of TLogEntry;
    end;

    TAutoHashFilesExtensions = record
        extension: string;
        active: boolean;
        minSize: Int64;
        maxSize: Int64;
    end;

    TAutoHashFiles = record
        active: boolean;
        minSize: Int64;
        maxSize: Int64;

        useFilter: boolean;
        extensions: array of TAutoHashFilesExtensions;
    end;

    dfID = record
        driveID: integer;
        folderID: integer;
    end;

    TActiveThreads = record
        active:boolean;
        useThread:boolean;
    end;

    TOptionsDisplay = record
        ShowFolders: boolean;
        Filter: boolean;
        FilterMask: string;
        ExpandDoubletten: boolean;
        ExpandFolders: boolean;
        ShowDoubletten: boolean;
    end;

    TOptionsLiveImport = record
        Folders: boolean;
        Files: TActiveThreads;
        hash: TActiveThreads;
        Exif: TActiveThreads;
        UseMaxFileSize: boolean;
        MaxFileSize: Int64;
    end;

    TOptionsPreload = record
        Drive: boolean;
        Folders: boolean;
        Files: boolean;
    end;

    TOptionsUseCallbacks = record
        Drive: boolean;
        Folders: boolean;
        Files: boolean;
    end;

    TOptionsRecursiveRead = record

        UseThread: boolean;
        hashFiles: boolean;
        exifFiles: boolean;
        CombineInFolders: boolean;
        ExpandCombinedFolders: boolean;
        ScollToEnd: boolean;
        UseMaxFileSize: boolean;
        MaxFileSize: Int64;
    end;

    TOptionsFolderList = record
        active: boolean;
        List: TStringlist;
    end;

    TExplodeArray = array of String;

    TCBaddComputer = procedure(Computer: TComputer); stdcall;
    TCBaddDrive = procedure(Drive: TDrive; ParentNode: PVirtualNode); stdcall;
    TCBaddFolder = procedure(folder: TFolder; ParentNode: PVirtualNode); stdcall;
    TCBaddDatei = procedure(datei: TDatei; ParentNode: PVirtualNode); stdcall;

function Explode(const cSeparator, vString: String): TExplodeArray;
function Implode(const cSeparator: String; const cArray: TExplodeArray): String;
function OccurrencesOfChar(const s: string; const C: char): integer;

function BoolToString(b: boolean): string;
function StringToBool(s: string): boolean;

function getEmptyTMeta: TMeta;

function retrieveVolumeName(DriveLetter: char): string;
function retrieveVolumeSerialNumber(const folder: string): string;
function IsNodeVisibleInClientRect(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex = NoColumn)
    : boolean;

function GPSLatToReal(const l: TGPSLatitude): real;
function GPSLonToReal(const l: TGPSLongitude): real;
function GPSAltToInt(const l: TExifFraction): integer;

function GetDefaultSystemIcon2(ASiid: integer): integer;
function getPathIconID(const Path: string): integer;
function getKindRecord(const isComputer, isDrive, isFolder, isFile: boolean): TDataKind;

implementation

function BoolToString(b: boolean): string;
begin
    if b then
        result := 'True'
    else
        result := 'False';
end;

function StringToBool(s: string): boolean;
begin
    result := s = 'True';
end;

function GPSLatToReal(const l: TGPSLatitude): real;
var
    ref: integer;
begin
    // d:= ExifData.GPSLatitude.Degrees.Quotient; {Degrees}
    // m:= ExifData.GPSLatitude.Minutes.Quotient; {Minutes}
    // s:= ExifData.GPSLatitude.Seconds.Quotient; {Seconds}

    result := 0;

    { Now determine the sign }
    if l.Direction = ltNorth then
        ref := 1
    else
        ref := -1;

    { Convert to decimal }
    result := ref * (l.degrees.Quotient + (l.Minutes.Quotient / 60) + (l.Seconds.Quotient / 60 / 60));

end;

function GPSLonToReal(const l: TGPSLongitude): real;
var
    ref: integer;
begin
    result := 0;

    { Now determine the sign }
    if l.Direction = lnEast then
        ref := 1
    else
        ref := -1;

    { Convert to decimal }
    result := ref * (l.degrees.Quotient + (l.Minutes.Quotient / 60) + (l.Seconds.Quotient / 60 / 60));

end;

function GPSAltToInt(const l: TExifFraction): integer;
begin
    result := 0;
    if (l.Denominator <> 0) then
        result := trunc(l.Numerator / l.Denominator);
end;

function convertLLA: real;
begin
end;

function getEmptyTMeta: TMeta;
begin
    result.kind := MyKnone;
    result.dimX := 0;
    result.dimY := 0;
    result.DPIX := 0;
    result.DPIY := 0;
    result.Description := '';
    result.Camera := '';
    result.Make := '';
    result.date := 0;
    result.Coordinates.longitude := 0;
    result.Coordinates.latitude := 0;
    result.Coordinates.altitude := 0;
end;

function retrieveVolumeName(DriveLetter: char): string;
var
    dummy: DWORD;
    buffer: array [0 .. MAX_PATH] of char;
    oldmode: LongInt;
begin
    oldmode := SetErrorMode(SEM_FAILCRITICALERRORS);
    try
        GetVolumeInformation(PChar(DriveLetter + ':\'), buffer, SizeOf(buffer), nil, dummy, dummy, nil, 0);
        result := StrPas(buffer);
    finally
        SetErrorMode(oldmode);
    end;
end;

function retrieveVolumeSerialNumber(const folder: string): string;
var
    Drive: string;
    FileSysName, VolName: array [0 .. 255] of char;
    SerialNum, MaxCLength, FileSysFlag: DWORD;
begin
    result := '';
    Drive := IncludeTrailingPathDelimiter(extractFileDrive(folder));
    GetVolumeInformation(PChar(Drive), VolName, 255, @SerialNum, MaxCLength, FileSysFlag, FileSysName, 255);
    result := IntToHex(SerialNum, 8);
end;

function IsNodeVisibleInClientRect(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex = NoColumn)
    : boolean;
begin
    result := Tree.IsVisible[Node] and Tree.GetDisplayRect(Node, Column, false).IntersectsWith(Tree.ClientRect);
end;

function Implode(const cSeparator: String; const cArray: TExplodeArray): String;
var
    i: integer;
begin
    result := '';
    for i := 0 to length(cArray) - 1 do
    begin
        result := result + cSeparator + cArray[i];
    end;
    System.Delete(result, 1, length(cSeparator));
end;

function Explode(const cSeparator, vString: String): TExplodeArray;
var
    i: integer;
    s: String;
begin
    s := vString;
    setLength(result, 0);
    i := 0;
    while Pos(cSeparator, s) > 0 do
    begin
        setLength(result, length(result) + 1);
        result[i] := Copy(s, 1, Pos(cSeparator, s) - 1);
        Inc(i);
        s := Copy(s, Pos(cSeparator, s) + length(cSeparator), length(s));
    end;
    setLength(result, length(result) + 1);
    result[i] := Copy(s, 1, length(s));
    s := '';
end;

function OccurrencesOfChar(const s: string; const C: char): integer;
var
    i: integer;
begin
    result := 0;
    for i := 1 to length(s) do
        if s[i] = C then
            Inc(result);
end;

function GetDefaultSystemIcon2(ASiid: integer): integer;
var
    sInfo: TSHStockIconInfo;
begin
    sInfo.cbSize := SizeOf(TSHStockIconInfo);
    if S_OK = SHGetStockIconInfo(ASiid, SHGSI_SYSICONINDEX, sInfo) then
        result := sInfo.iSysImageIndex
    else
        result := -1;
end;

function getPathIconID(const Path: string): integer;
var
    fi: TSHFileInfo;
begin
    result := -1;
    try
        ZeroMemory(@fi, SizeOf(fi));
        SHGetFileInfo(PChar(Path), 0, fi, SizeOf(fi), SHGFI_ICON or SHGFI_SYSICONINDEX or SHGFI_TYPENAME);

        result := fi.iIcon;

        if fi.iIcon <> 0 then
            DestroyIcon(fi.hIcon);
    except

    end;
end;

function getKindRecord(const isComputer, isDrive, isFolder, isFile: boolean): TDataKind;
begin
    result.isComputer := isComputer;
    result.isDrive := isDrive;
    result.isFolder := isFolder;
    result.isFile := isFile;
end;

end.
