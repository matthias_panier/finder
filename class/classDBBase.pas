unit classDBBase;

interface

uses
    IBX.IBQuery,
    IBX.IBCustomDataSet,
    IBX.IBDatabase,
    u_DM_type_def,
    Windows,
    DateUtils,
    System.SysUtils;

type
    TClassDBBase = class
        DB: TIBDatabase;
        t: TIBTransaction;
        q: TIBQuery;
        q2: TIBQuery;
        lastQueries: TQueryTicks;
    protected
    public
        constructor create(const DBName: string);
        destructor destroy; override;

        procedure createDBandConnect(const DBName: string);
        function getNextIDOfTable(const table: string): integer;

        function QPreset: boolean;
        function Q2Preset: boolean;

        function starttick: cardinal;
        function finishtick(const t1: cardinal; const fnct: string; const msg: string): cardinal;

        function getLastQueries: TQueryTicks;
        function getUnixTimeStamp: int64;

    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////

constructor TClassDBBase.create(const DBName: string);
begin
    // inherited;
    setLength(lastQueries, 0);

    createDBandConnect(DBName);
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////

destructor TClassDBBase.destroy;
begin
    inherited;
    setLength(lastQueries, 0);

    if (assigned(q)) then
        q.free;
    if (assigned(q2)) then
        q2.free;
    if (assigned(t)) then
        t.free;
    if (assigned(DB)) then
        DB.free;

end;

procedure TClassDBBase.createDBandConnect(const DBName: string);
begin
    try
        DB := TIBDatabase.create(nil);
        try
            t := TIBTransaction.create(nil);
            try
                q := TIBQuery.create(nil);
                try
                    q2 := TIBQuery.create(nil);
                    try
                        DB.Params.Clear;
                        DB.Params.Add('user_name=sysdba');
                        DB.Params.Add('password=masterkey');
                        DB.Params.Add('lc_ctype=UTF8');

                        DB.DatabaseName := DBName;
                        DB.LoginPrompt := False;
                        DB.DefaultTransaction := t;
                        DB.IdleTimer := 0;
                        DB.SQLDialect := 3;
                        DB.TraceFlags := [];

                        t.Active := False;
                        t.DefaultDatabase := DB;
                        t.AutoStopAction := saCommit;

                        q.Database := DB;
                        q.Transaction := t;
                        q.CachedUpdates := False;
                        q.BufferChunks := 1000;

                        DB.Open;

                    except
                        on E: Exception do
                        begin
                            //
                        end;
                    end;

                    q2.Database := DB;
                    q2.Transaction := t;
                    q2.CachedUpdates := False;
                    q2.BufferChunks := 1000;
                except
                    on E: Exception do
                    begin
                        // Form1.addErrorMsg('THREAD: q2 not created');
                    end;
                end;

            except
                on E: Exception do
                begin
                    // Form1.addErrorMsg('THREAD: q not created');
                end;
            end;
        except
            on E: Exception do
            begin
                // Form1.addErrorMsg('THREAD: t not created');
            end;
        end;
    except
        on E: Exception do
        begin
            // Form1.addErrorMsg('THREAD: db not created');
        end;
    end;

end;

// receive the next ID of the given Table
function TClassDBBase.getNextIDOfTable(const table: string): integer;
begin
    Result := 0;

    Q2Preset;
    q2.sql.text := 'SELECT ' + table + '_ID FROM INDIZES WHERE ID=1';
    try
        try
            q2.Open;

            if not q2.Eof then
            begin
                Result := q2.Fields[0].AsInteger;
            end;
        except
            on E: Exception do
            begin
                // showmessage(e.Message);
            end;
        end;
    finally
    end;

    Q2Preset;

    q2.sql.text := 'UPDATE INDIZES SET ' + table + '_ID = ' + table + '_ID +1 WHERE ID=1';

    try
        try
            q2.ExecSQL;
        except
            on E: Exception do
            begin
                // showmessage(e.Message);
            end;
        end;
    finally
        Q2Preset;
    end;
end;

function TClassDBBase.QPreset: boolean;
begin
    Result := False;

    if q.Database.Connected then
    begin
        q.close;
        q.sql.Clear;
        q.Params.Clear;

        Result := true;
    end;
end;

function TClassDBBase.Q2Preset: boolean;
begin
    Result := False;
    if q2.Database.Connected then
    begin
        q2.close;
        q2.sql.Clear;
        q2.Params.Clear;

        Result := true;
    end;
end;

function TClassDBBase.starttick: cardinal;
begin
    Result := gettickcount;
end;

function TClassDBBase.finishtick(const t1: cardinal; const fnct: string; const msg: string): cardinal;
var
    l: integer;
begin
    Result := 0;

    l := length(lastQueries);
    setLength(lastQueries, l + 1);

    lastQueries[l].funktion := fnct;
    lastQueries[l].msg := msg;
    lastQueries[l].start := t1;
    lastQueries[l].finish := gettickcount;

    Result := lastQueries[l].finish - lastQueries[l].start;
end;

function TClassDBBase.getLastQueries: TQueryTicks;
begin
    Result := lastQueries;
end;

function TClassDBBase.getUnixTimeStamp: int64;
begin
    Result := DateTimeToUnix(now);
end;

end.
