unit classComputerRead;

interface

uses classComputerReadDB, u_DM_type_def, System.SysUtils, ShellAPI, Windows;

type
    TclassComputerRead = class(TclassComputerReadDB)
    private
        Computers: TComputers;

        FuseCallback: boolean;
        FAddComputerCallBack: TCBaddComputer;

        procedure getLocalComputer;
        function PrepareComputerEntry(computerName: string): TComputer;
        function addComputer(const currentComputer: TComputer): integer;

        function GetComputerName: string;
        procedure addDBComputerToList;
    public
        constructor Create(const dbPfad: string; AddComputerToVST: TCBaddComputer);
        destructor destroy; override;

        procedure readLocalComputer(const useCallback: boolean);
        procedure addDBComputer(const useCallback: boolean);
    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////
constructor TclassComputerRead.Create(const dbPfad: string; AddComputerToVST: TCBaddComputer);
begin
    inherited Create(dbPfad);
    SetLength(Computers, 0);

    // store callback function
    FAddComputerCallBack := AddComputerToVST;
    FuseCallback := false;
end;

procedure TclassComputerRead.readLocalComputer(const useCallback: boolean);
begin
    FuseCallback := useCallback;

    getLocalComputer;
end;

procedure TclassComputerRead.addDBComputer(const useCallback: boolean);
begin
    FuseCallback := useCallback;

    addDBComputerToList;
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////
destructor TclassComputerRead.destroy;
begin
    inherited destroy;
end;

procedure TclassComputerRead.getLocalComputer;
begin
    addComputer(PrepareComputerEntry(GetComputerName));
end;

function TclassComputerRead.PrepareComputerEntry(computerName: string): TComputer;
begin
    Finalize(result);

    result.Database.name := computerName;
    result.Database.ID := getDBComputerID(result.Database);

    result.DataSet.Path := '';
    result.DataSet.kind := getKindRecord(true, false, false, false);
    result.DataSet.Computer.ID := result.Database.ID;
    result.DataSet.Computer.source.isFromDB := result.Database.ID > 0;
    result.DataSet.Computer.source.isFromFilesystem := true;

    result.imageIndex := GetDefaultSystemIcon2(SIID_DESKTOPPC);
end;

function TclassComputerRead.addComputer(const currentComputer: TComputer): integer;
var
    l: integer;
begin
    l := Length(Computers);
    SetLength(Computers, l + 1);

    Computers[l] := currentComputer;

    if (FuseCallback) and (assigned(FAddComputerCallBack)) then
        FAddComputerCallBack(currentComputer);

    result := l;
end;

procedure TclassComputerRead.addDBComputerToList;
var
    i: integer;
    l: integer;
begin
    for i := 0 to high(DBComputers) do
    begin
        if DBComputers[i].name = Computers[0].Database.name then
            continue;

        l := Length(Computers);

        SetLength(Computers, l + 1);

        Computers[l].Database := DBComputers[i];
        Computers[l].DataSet.kind := getKindRecord(true, false, false, false);

        Computers[l].DataSet.Computer.ID := DBComputers[i].ID;
        Computers[l].DataSet.Computer.source.isFromDB := true;
        Computers[l].DataSet.Computer.source.isFromFilesystem := false;

        Computers[l].imageIndex := Computers[0].imageIndex;
    end;
end;

function TclassComputerRead.GetComputerName: string;
var
    Len: DWORD;
begin
    result := '';
    try
        Len := MAX_COMPUTERNAME_LENGTH + 1;
        SetLength(result, Len);
        if Windows.GetComputerName(PChar(result), Len) then
        begin
            SetLength(result, Len);
        end else begin
            RaiseLastOSError;
        end;
    except
    end;
end;

end.
