unit classOptions;

interface

uses u_DM_type_def, Classes, classFilesList, System.SysUtils, System.Masks;

type
    TClassOptions = class
    public
//        useLocalComputerCallBack: boolean;
//        loadDBComputer: boolean;
//        useDBComputerCallback: boolean;

        Display: TOptionsDisplay;
        RecursiveRead: TOptionsRecursiveRead;
        LiveImport: TOptionsLiveImport;
        Preload: TOptionsPreload;
        UseCallBacks: TOptionsUseCallbacks;

        FolderBlackList: TOptionsFolderList;
        FilesBlackList: TClassFilesList;
        FilesWhiteList: TClassFilesList;

        constructor create;
        destructor destroy; override;

        function MatchFolderMask(const folder: string): boolean;
        function MatchFileMask(tsr: System.SysUtils.TSearchRec): boolean;

    end;

implementation

constructor TClassOptions.create;
begin
    inherited create;

    Display.ShowDoubletten := true;
    Display.ExpandDoubletten := true;
    Display.ExpandFolders := true;
    Display.ShowFolders := false;

    Display.Filter := false;
    Display.FilterMask := '';

    RecursiveRead.UseThread := false;
    RecursiveRead.hashFiles := true;
    RecursiveRead.exifFiles := true;
    RecursiveRead.CombineInFolders := true;
    RecursiveRead.ExpandCombinedFolders := true;
    RecursiveRead.ScollToEnd := true;
    RecursiveRead.UseMaxFileSize := true;
    RecursiveRead.MaxFileSize := 100000000;

    LiveImport.Folders := true;
    LiveImport.Files.active := true;
    LiveImport.Files.UseThread := false;
    LiveImport.Exif.active := true;
    LiveImport.Exif.UseThread := false;
    LiveImport.hash.active := true;
    LiveImport.hash.UseThread := false;
    LiveImport.UseMaxFileSize := true;
    LiveImport.MaxFileSize := 20000000;

    FolderBlackList.active := true;
    FolderBlackList.List := TStringList.create();

    FilesBlackList := TClassFilesList.create;
    FilesWhiteList := TClassFilesList.create;

    Preload.Drive := false;
    Preload.Folders := false;
    Preload.Files := false;

    UseCallBacks.Drive := true;
    UseCallBacks.Folders := true;
    UseCallBacks.Files := true;

//    useLocalComputerCallBack := true;
//    loadDBComputer := true;
//    useDBComputerCallback := true;

end;

destructor TClassOptions.destroy;
begin
    Display.FilterMask := '';
    FolderBlackList.List.Free;

    FilesBlackList.Free;
    FilesWhiteList.Free;

    inherited destroy;
end;

function TClassOptions.MatchFolderMask(const folder: string): boolean;
var
    i: integer;
begin
    result := false;

    if FolderBlackList.active then
    begin
        for i := 0 to FolderBlackList.List.Count - 1 do
        begin
            if (MatchesMask(folder, FolderBlackList.List[i])) then
            begin
                result := true;
                break;
            end;
        end;
    end;
    //
end;

function TClassOptions.MatchFileMask(tsr: System.SysUtils.TSearchRec): boolean;
var
    r: boolean;
    i: integer;
    isWhitelist: boolean;
    isBlacklist: boolean;
begin
    isWhitelist := false;
    isBlacklist := false;

    // is file in whitelist?
    if FilesWhiteList.active then
    begin
        isWhitelist := FilesWhiteList.IsInList(tsr.Name, tsr.Size);
    end else begin
        isWhitelist := true;
    end;;

    // is file in blacklist?
    if FilesBlackList.active then
    begin
        isBlacklist := FilesBlackList.IsInList(tsr.Name, tsr.Size);
    end;

    // result := true;

    r := isWhitelist and not isBlacklist;
    result := r;
end;

end.
