unit classLogging;

interface

uses
    System.SysUtils, u_DM_type_def;

type
    TClassLogging = class
    private
        procedure clear;
    protected
        log: TLog;
    public
        constructor create;
        destructor destroy; override;

        function add(const level: TLogLevel; const source: TLogSources; const msg: string): TLogEntry;

    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////

constructor TClassLogging.create;
begin
    inherited;
    clear;
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////

destructor TClassLogging.destroy;
begin
    clear;
    inherited;
end;

procedure TClassLogging.clear;
begin
    log.laenge := 0;
    log.position := 0;
    SetLength(log.entry, 0);
end;

function TClassLogging.add(const level: TLogLevel; const source: TLogSources; const msg: string): TLogEntry;
begin
    log.position := log.laenge;
    inc(log.laenge);
    SetLength(log.entry, log.laenge);

    log.entry[log.position].msg := msg;
    log.entry[log.position].level := level;
    log.entry[log.position].source := source;
    log.entry[log.position].timestamp := now;

    result := log.entry[log.position];
end;

end.
