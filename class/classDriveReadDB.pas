unit classDriveReadDB;

interface

uses classDBBase, u_DM_type_def, System.SysUtils, Winapi.Windows;

type
    TclassDriveReadDB = class(TclassDBBase)
        DBDrives: array of TDBDrive;
        dataSet: TDataSet;
        FuseDBPreload: boolean;

        procedure loadDBDrives;

        function updateMainFolder(const driveID, folderID: integer): boolean;
        function addMainFolder(const givendrive: TDBDrive): TDBDrive;
        function getDBDrivesPos(const givendrive: TDBDrive): integer;
    protected
        function insertDrive(driveToAdd: TDBDrive): TDBDrive;
        function getDatabaseEntries(var givendrive: TDBDrive): boolean;
        function addDBDrive(const givendrive: TDBDrive): integer;

    public
        constructor Create(const dbName: string; const aDataSet: TDataSet);
        destructor destroy; override;

        function getDBDriveID(currentDrive: TDBDrive): dfID;
        function getDBFolderID(const currentDrive: TDBDrive): integer;
        function readDBDriveID(currentDrive: TDBDrive): dfID;
        procedure useDBPreload(const usePreload: boolean);
    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////
constructor TclassDriveReadDB.Create(const dbName: string; const aDataSet: TDataSet);
begin
    inherited Create(dbName);
    setlength(DBDrives, 0);

    dataSet := aDataSet;
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////
destructor TclassDriveReadDB.destroy;
begin
    inherited;

    setlength(DBDrives, 0);
end;

procedure TclassDriveReadDB.useDBPreload(const usePreload: boolean);
begin
    FuseDBPreload := usePreload;

    if FuseDBPreload then
    begin
        loadDBDrives;
    end;
end;

/// //////////////////////////////
/// Computer_ID is MANDATORY  ///
/// //////////////////////////////
function TclassDriveReadDB.getDBDriveID(currentDrive: TDBDrive): dfID;
var
    pos: integer;
    dID: dfID;
begin
    result.driveID := 0;
    result.folderID := 0;

    if FuseDBPreload then
    begin

        pos := getDBDrivesPos(currentDrive);

        if (pos > -1) then
        begin
            result.driveID := DBDrives[pos].ID;
            result.folderID := DBDrives[pos].Folder_ID;
            exit;
        end;
    end;

    dID := readDBDriveID(currentDrive);
    if (dID.driveID > 0) then
    begin
        currentDrive.ID := dID.driveID;
        currentDrive.Folder_ID := dID.folderID;
    end else begin
        currentDrive.ID := insertDrive(currentDrive).ID;
        currentDrive := addMainFolder(currentDrive);
    end;

    result.driveID := currentDrive.ID;
    result.folderID := currentDrive.Folder_ID;
end;

function TclassDriveReadDB.addDBDrive(const givendrive: TDBDrive): integer;
var
    l: integer;
begin
    l := length(DBDrives);
    setlength(DBDrives, l + 1);
    DBDrives[l] := givendrive;

    result := l;
end;

function TclassDriveReadDB.getDBFolderID(const currentDrive: TDBDrive): integer;
var
    pos: integer;
begin
    result := 0;

    pos := getDBDrivesPos(currentDrive);

    if (pos > -1) then
    begin
        if currentDrive.pfad <> DBDrives[pos].pfad then
            DBDrives[pos].pfad := currentDrive.pfad;

        if (DBDrives[pos].Folder_ID = 0) then
        begin
            DBDrives[pos] := addMainFolder(DBDrives[pos])
        end;

        result := DBDrives[pos].Folder_ID;
        exit;
    end;

    result := addMainFolder(currentDrive).Folder_ID;
end;

function TclassDriveReadDB.readDBDriveID(currentDrive: TDBDrive): dfID;
var
    pos: integer;
    t1: cardinal;
begin
    result.driveID := 0;
    result.folderID := 0;

    if q.Database.Connected then
    begin
        t1 := starttick;
        qpreset;

        q.SQL.Add('SELECT');
        q.SQL.Add('DRIVE.ID,');
        q.SQL.Add('DRIVE.FOLDER_ID');

        q.SQL.Add('FROM DRIVE');
        q.SQL.Add('WHERE');
        q.SQL.Add('DRIVE.' + T_COMPUTER + '_ID = :cid');
        q.SQL.Add('AND');
        q.SQL.Add('DRIVE.DSIZE = :size');
        q.SQL.Add('AND');
        q.SQL.Add('DRIVE.SERIALNR = :sn');

        q.Params[0].AsInteger := currentDrive.Computer_ID;
        q.Params[1].AsLargeInt := currentDrive.DSize;
        q.Params[2].AsString := currentDrive.SerialNR;

        try
            try
                q.Open;
                if not q.Eof then
                begin
                    result.driveID := q.Fields[0].AsInteger;
                    result.folderID := q.Fields[1].AsInteger;
                end;
            except
            end;
        finally
            finishtick(t1, 'loadDBDrives', 'Single Lookup: ' + inttostr(dataSet.Drive.ID) + ': ' + dataSet.Path);
            qpreset;
        end;

    end;
end;

procedure TclassDriveReadDB.loadDBDrives;
var
    t1: cardinal;
begin
    if q.Database.Connected then
    begin
        t1 := starttick;
        qpreset;

        q.SQL.Add('SELECT');
        q.SQL.Add('DRIVE.ID,');
        q.SQL.Add('DRIVE.' + T_COMPUTER + '_ID,');
        q.SQL.Add('DRIVE.' + T_ORDNER + '_ID,');

        q.SQL.Add('DRIVE.DSIZE,');
        q.SQL.Add('DRIVE.SERIALNR,');
        q.SQL.Add('DRIVE.CAPTION,');
        q.SQL.Add('DRIVE.ADD_DATE,');
        q.SQL.Add('FOLDER.PATH');

        q.SQL.Add('FROM DRIVE');
        q.SQL.Add('LEFT JOIN ' + T_ORDNER + ' ON ' + T_ORDNER + '.ID = DRIVE.' + T_ORDNER + '_ID');
        q.SQL.Add('WHERE');
        q.SQL.Add('DRIVE.' + T_COMPUTER + '_ID = :cid');

        q.Params[0].AsInteger := dataSet.Computer.ID;

        try
            try
                q.Open;
                while not q.Eof do
                begin
                    setlength(DBDrives, q.RecNo);
                    // from DB.Drive.*
                    DBDrives[q.RecNo - 1].ID := q.FieldByName('ID').AsInteger;
                    DBDrives[q.RecNo - 1].Computer_ID := q.FieldByName(T_COMPUTER + '_ID').AsInteger;
                    DBDrives[q.RecNo - 1].Folder_ID := q.FieldByName(T_ORDNER + '_ID').AsInteger;
                    DBDrives[q.RecNo - 1].DSize := q.FieldByName('DSIZE').AsLargeInt;
                    DBDrives[q.RecNo - 1].SerialNR := q.FieldByName('SERIALNR').AsString;
                    DBDrives[q.RecNo - 1].caption := q.FieldByName('CAPTION').AsString;
                    DBDrives[q.RecNo - 1].add_date := q.FieldByName('ADD_DATE').AsLargeInt;
                    // from DB.folder.path
                    DBDrives[q.RecNo - 1].pfad := q.FieldByName('PATH').AsString;

                    q.Next;
                end;
            except
                //
            end;
        finally
            finishtick(t1, 'loadDBDrives', inttostr(dataSet.Drive.ID) + ': ' + dataSet.Path);
            qpreset;
        end;

    end;
end;

function TclassDriveReadDB.insertDrive(driveToAdd: TDBDrive): TDBDrive;
var
    ID: integer;
    uts: int64;
    t1: cardinal;

begin
    result := driveToAdd;

    if q.Database.Connected then
    begin
        t1 := starttick;
        qpreset;

        ID := getNextIDOfTable(T_LAUFWERK);
        uts := getUnixTimeStamp;

        q.SQL.Add('INSERT INTO ' + T_LAUFWERK);
        q.SQL.Add('("ID", "' + T_COMPUTER + '_ID", "DSIZE", "SERIALNR", "CAPTION", "ADD_DATE")');
        q.SQL.Add('VALUES');
        q.SQL.Add('(:id, :cid, :dsize, :sn, :caption, :datum);');

        q.Params[0].AsInteger := ID;
        q.Params[1].AsInteger := driveToAdd.Computer_ID;
        q.Params[2].AsLargeInt := driveToAdd.DSize;
        q.Params[3].AsString := driveToAdd.SerialNR;
        q.Params[4].AsString := driveToAdd.caption;
        q.Params[5].AsFloat := uts;

        try
            try
                q.ExecSQL;
                result.ID := ID;
                result.add_date := uts;
            except
                //
            end;
        finally
            finishtick(t1, 'insertDrive', inttostr(ID) + ': ' + driveToAdd.caption);
            qpreset;
        end;

    end;
end;

function TclassDriveReadDB.updateMainFolder(const driveID, folderID: integer): boolean;
var
    t1: cardinal;
begin
    result := false;

    if q.Database.Connected then
    begin
        t1 := starttick;

        qpreset;
        q.Close;
        q.SQL.Text := 'UPDATE ' + T_LAUFWERK + ' SET ' + T_ORDNER + '_ID= :fid WHERE ID = :id';
        q.Params[0].AsInteger := folderID;
        q.Params[1].AsInteger := driveID;

        try
            q.ExecSQL;
            result := true;
        finally
            finishtick(t1, 'updateMainFolder', inttostr(driveID) + ': new FolderID: ' + inttostr(folderID));
            qpreset
        end;
    end;
end;

function TclassDriveReadDB.addMainFolder(const givendrive: TDBDrive): TDBDrive;
var
    ID: integer;
    uts: int64;
    t1: cardinal;
begin
    result := givendrive;

    if q.Database.Connected then
    begin
        t1 := starttick;
        ID := getNextIDOfTable(T_ORDNER);
        uts := getUnixTimeStamp;

        qpreset;

        q.SQL.Add('INSERT INTO ' + T_ORDNER);
        q.SQL.Add('("ID","' + T_COMPUTER + '_ID", "PARENT_ID", "' + T_LAUFWERK + '_ID", "PATH", "ADD_DATE")');
        q.SQL.Add('VALUES');
        q.SQL.Add('(:id, :computerID, :parentID, :driveID, :pfad, :datum);');

        q.ParamByName('id').AsInteger := ID;
        q.ParamByName('computerID').AsInteger := givendrive.Computer_ID;
        q.ParamByName('parentID').AsInteger := 0;
        q.ParamByName('driveID').AsInteger := givendrive.ID;
        q.ParamByName('pfad').AsString := ExcludeTrailingPathDelimiter(givendrive.pfad);
        q.ParamByName('datum').AsLargeInt := uts;

        try
            q.ExecSQL;
            result.Folder_ID := ID;
            updateMainFolder(givendrive.ID, ID);
        finally
            finishtick(t1, 'addMainFolder', inttostr(ID) + ': ' + givendrive.pfad);
            qpreset;
        end;
    end;
end;

function TclassDriveReadDB.getDBDrivesPos(const givendrive: TDBDrive): integer;
var
    i: integer;
begin
    result := -1;

    for i := Low(DBDrives) to High(DBDrives) do
    begin
        if DBDrives[i].DSize <> givendrive.DSize then
            continue;

        if DBDrives[i].SerialNR = givendrive.SerialNR then
        begin
            result := i;
            exit;
        end;
    end;
end;

function TclassDriveReadDB.getDatabaseEntries(var givendrive: TDBDrive): boolean;
begin
    result := true;
    // TODO
end;

end.
