unit registry_connection;

interface

uses Windows, Registry;

type
    TClassRegistry = class
    private
        FRegMainFolder: string;
        function getRegistryFullFolder: string;
        procedure SetRegMainFolder(const Value: string);
    protected
        property RegMainFolder: string read FRegMainFolder write SetRegMainFolder;
    public
        constructor create;
        destructor destroy; override;

        function ReadFromRegistry(const key: string; const default: string): string;
        function WriteToRegistry(const key: string; const Value: string): boolean;
    end;

implementation

const
    SCRegistryBasePath = '\SOFTWARE\Individualentwicklungen\';

    /// ///////////////////////////
    // on creation
    /// ///////////////////////////
constructor TClassRegistry.create;
begin
    inherited;
    //
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////
destructor TClassRegistry.destroy;
begin
    FRegMainFolder := '';

    inherited;
end;

procedure TClassRegistry.SetRegMainFolder(const Value: string);
begin
    FRegMainFolder := Value;
end;

function TClassRegistry.getRegistryFullFolder: string;
begin
    result := SCRegistryBasePath + FRegMainFolder;
end;

function TClassRegistry.ReadFromRegistry(const key: string; const default: string): string;
var
    __RegIni: TRegistry;
begin
    result := default;

    __RegIni := TRegistry.create(KEY_READ);
    try
        __RegIni.RootKey := HKEY_CURRENT_USER;
        __RegIni.OpenKey(getRegistryFullFolder, true);

        if __RegIni.ValueExists(key) then
        begin
            result := __RegIni.ReadString(key);
        end;
    finally
        __RegIni.Free;
    end;
end;

function TClassRegistry.WriteToRegistry(const key: string; const Value: string): boolean;
var
    __RegIni: TRegistry;
begin
    result := false;

    __RegIni := TRegistry.create(KEY_ALL_ACCESS);
    try
        __RegIni.RootKey := HKEY_CURRENT_USER;
        __RegIni.OpenKey(getRegistryFullFolder, true);

        if (__RegIni.ReadString(key) <> Value) then
        begin

            __RegIni.WriteString(key, Value);

            if (__RegIni.ValueExists(key) and (__RegIni.ReadString(key) = Value)) then
            begin
                result := true;
            end else begin
            end;
        end;
    finally
        __RegIni.Free;
    end;
end;

end.
