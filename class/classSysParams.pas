﻿unit classSysParams;

interface

uses SysUtils, Windows, Classes, registry_connection;

type
    TClassSysParams = class(TClassRegistry)
    private
        FalphaBlend: boolean;
        FalphaBlendValue: integer;
        FisDebugMode: boolean;
        FisDevMode: boolean;
        FisShiftState: boolean;
        FPCName: string;
        FuserName: string;
        Fversion: string;
        procedure checkStartParameter;
        procedure GetBuildInfo(var V1, V2, V3, V4: word);
        procedure GetBuildVersion;
        procedure GetComputerName;
        procedure GetUsername;
        procedure preset;
    public
        constructor create;
        destructor destroy; override;
        property alphaBlend: boolean read FalphaBlend;
        property alphaBlendValue: integer read FalphaBlendValue;
        property debugMode: boolean read FisDebugMode;
        property devMode: boolean read FisDevMode;
        property pcName: string read FPCName;
        property shiftState: boolean read FisShiftState;
        property version: string read Fversion;
        property windowsUser: string read FuserName;

    end;

implementation

const
    { Hauptordner für gemeinsame Dateien unter Windows }
    CSIDL_COMMON_APPDATA = $0023;

    /// ///////////////////////////
    // on creation
    /// ///////////////////////////
constructor TClassSysParams.create;
begin
    inherited create;

    RegMainFolder := 'Finder';

    preset;

    checkStartParameter;
    GetBuildVersion;
    GetUsername;
    GetComputerName;
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////
destructor TClassSysParams.destroy;
begin
    preset;

    inherited destroy;
end;

/// ///////////////////////////
// Checks the given parameter at start
/// ///////////////////////////

procedure TClassSysParams.checkStartParameter;
var
    i: integer;
    param: string;
begin

    for i := 0 to paramCount do
    begin
        param := lowercase(paramstr(i));

        if param = '/dev' then
        begin
            FisDevMode := true;
        end;

        if param = '/debug' then
        begin
            FisDebugMode := true;
        end;

    end;

    FisShiftState := GetKeyState(VK_SHIFT) < 0;
    // https://www.delphi-treff.de/tipps-tricks/system/tastatur-und-maus/status-der-strg-alt-shift-tasten-erfragen/
end;

procedure TClassSysParams.GetBuildInfo(var V1, V2, V3, V4: word);
var
    VerInfoSize, VerValueSize, Dummy: DWORD;
    VerInfo: Pointer;
    VerValue: PVSFixedFileInfo;
begin
    VerInfoSize := GetFileVersionInfoSize(PChar(paramstr(0)), Dummy);
    if VerInfoSize > 0 then
    begin
        GetMem(VerInfo, VerInfoSize);
        try
            try
                if GetFileVersionInfo(PChar(paramstr(0)), 0, VerInfoSize, VerInfo) then
                begin
                    VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
                    with VerValue^ do
                    begin
                        V1 := dwFileVersionMS shr 16;
                        V2 := dwFileVersionMS and $FFFF;
                        V3 := dwFileVersionLS shr 16;
                        V4 := dwFileVersionLS and $FFFF;
                    end;
                end;
            except
                on E: Exception do
                begin
                    // showmessage(e.Message);
                end;
            end;
        finally
            FreeMem(VerInfo, VerInfoSize);
        end;
    end;
end;

/// ///////////////////////////
// Get Build Info
/// ///////////////////////////

procedure TClassSysParams.GetBuildVersion;
var
    V1, V2, V3, V4: word;
begin
    GetBuildInfo(V1, V2, V3, V4);
    Fversion := IntToStr(V1) + '.' + IntToStr(V2) + '.' + IntToStr(V3) + '.' + IntToStr(V4);
end;

/// ///////////////////////////
// Get Windows PC Name
/// ///////////////////////////

procedure TClassSysParams.GetComputerName;
var
    Len: DWORD;
begin
    FPCName := '';
    try
        Len := MAX_COMPUTERNAME_LENGTH + 1;
        SetLength(FPCName, Len);
        if Windows.GetComputerName(PChar(pcName), Len) then
        begin
            SetLength(FPCName, Len);
        end else begin
            RaiseLastOSError;
        end;
    except
    end;
end;

/// ///////////////////////////
// Get Windows User Name
/// ///////////////////////////

procedure TClassSysParams.GetUsername;
var
    Buffer: array [0 .. 255] of Char;
    Size: DWORD;
begin
    FuserName := '';
    try
        Size := SizeOf(Buffer);
        if not Windows.GetUsername(Buffer, Size) then
        begin
            RaiseLastOSError;
        end;
        SetString(FuserName, Buffer, Size - 1);
    except
    end;
end;

/// ///////////////////////////
// Sets the internal variables to their initals
/// ///////////////////////////

procedure TClassSysParams.preset;
begin
    FisDevMode := false;
    FisDebugMode := false;
    FisShiftState := false;

    Fversion := '0';
    FalphaBlend := true;

    Fversion := '';

    FuserName := '';
    FPCName := '';

    FalphaBlendValue := 140;
end;

end.
