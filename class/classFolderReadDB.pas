unit classFolderReadDB;

interface

uses classDBBase, u_DM_type_def, System.SysUtils;

type
    TclassFolderReadDB = class(TclassDBBase)
        initialFolderCount: integer;
        initialFileCount: integer;

        DBFolders: array of TDBFolder;

        DBFiles: array of TDBFile;
        DataSet: TDataSet;

        procedure loadDBFolders;
        procedure loadDBFiles;

        function addDBFolder(const givenFolder: TDBFolder): integer;
        function insertFolder(const folderToAdd: TDBFolder): TDBFolder;

        function getFolderID(givenFolder: TDBFolder; const usePreload, useLookup, createEntry: boolean): integer;
        function getDBFolderPos(const givenFolder: TDBFolder): integer;
        function getDBFolderID(givenFolder: TDBFolder): integer;

        function getFileID(givenFile: TDBFile; const usePreload, useLookup, createEntry: boolean): integer;
        function getDBFilePos(const givenFile: TDBFile): integer;
        function insertFile(const fileToAdd: TDBFile): TDBFile;
        function addDBFile(const givenFile: TDBFile): integer;
        function getDBFileID(const givenFile: TDBFile): integer;

        function updateContentHash(const ID: integer; const hash: string): boolean;

    protected
    public

        constructor Create(const dbName: string; const aDataSet: TDataSet);
        destructor destroy; override;

    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////
constructor TclassFolderReadDB.Create(const dbName: string; const aDataSet: TDataSet);
begin
    inherited Create(dbName);
    setlength(DBFolders, 0);
    setlength(DBFiles, 0);
    initialFolderCount := 0;
    initialFileCount := 0;

    DataSet := aDataSet;
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////
destructor TclassFolderReadDB.destroy;
begin
    // parentSet.Free;

    inherited;
end;

function TclassFolderReadDB.getFileID(givenFile: TDBFile; const usePreload, useLookup, createEntry: boolean): integer;
var
    pos, ID: integer;
begin
    result := 0;

    // Preload
    if usePreload then
    begin
        pos := getDBFilePos(givenFile);
        if (pos > -1) then
        begin
            result := DBFiles[pos].ID;
            exit;
        end;
    end;

    // DB Lookup
    if useLookup then
    begin
        ID := getDBFileID(givenFile);
        if ID > 0 then
        begin
            result := ID;
            exit;
        end;

    end;

    // creation
    if (createEntry) and (givenFile.Folder_ID > 0) then
    begin
        givenFile.ID := insertFile(givenFile).ID;
        addDBFile(givenFile);

        result := givenFile.ID;
    end;
end;

function TclassFolderReadDB.getDBFilePos(const givenFile: TDBFile): integer;
var
    i: integer;
    t1: Cardinal;
begin
    result := -1;
    t1 := starttick;

    if initialFileCount = 0 then
    begin
        exit;
    end;

    for i := low(DBFiles) to high(DBFiles) do
    begin
        // validation of same folder by path
        if (DBFiles[i].Filename = givenFile.Filename) then
        begin
            result := i;
            finishtick(t1, 'getDBFilePos', 'found: #' + inttostr(i) + ' ' + givenFile.Filename);
            exit;
        end;
    end;
    finishtick(t1, 'getDBFilePos', 'none');
end;

function TclassFolderReadDB.getFolderID(givenFolder: TDBFolder;
    const usePreload, useLookup, createEntry: boolean): integer;
var
    ID, pos: integer;
begin
    result := 0;

    // Preload
    if usePreload then
    begin
        pos := getDBFolderPos(givenFolder);
        if (pos > -1) then
        begin
            result := DBFolders[pos].ID;
            exit;
        end;
    end;

    // DB Lookup
    if useLookup then
    begin
        ID := getDBFolderID(givenFolder);
        if ID > 0 then
        begin
            result := ID;
            exit;
        end;
    end;

    if (createEntry) and (givenFolder.Parent_ID > 0) then
    begin
        // create entry
        givenFolder.ID := insertFolder(givenFolder).ID;
        addDBFolder(givenFolder);

        result := givenFolder.ID;
    end;
end;

function TclassFolderReadDB.getDBFolderPos(const givenFolder: TDBFolder): integer;
var
    i: integer;
    t1: Cardinal;
begin
    result := -1;
    t1 := starttick;

    if initialFolderCount = 0 then
    begin
        exit;
    end;

    for i := low(DBFolders) to high(DBFolders) do
    begin
        // validation of same folder by path
        if (DBFolders[i].Path = givenFolder.Path) then
        begin
            result := i;
            finishtick(t1, 'getDBFolderPos', 'found: #' + inttostr(i) + ' ' + givenFolder.Path);
            exit;
        end;
    end;
    finishtick(t1, 'getDBFolderPos', 'none');
end;

function TclassFolderReadDB.getDBFolderID(givenFolder: TDBFolder): integer;
var
    uts: int64;
    t1: Cardinal;
begin
    result := 0;

    if q.database.Connected then
    begin
        t1 := starttick;
        qpreset;

        uts := getUnixTimeStamp;

        q.SQL.Add('SELECT');
        q.SQL.Add('ID');
        q.SQL.Add('FROM ' + T_ORDNER);
        q.SQL.Add('WHERE');
        q.SQL.Add('PARENT_ID= :pid AND');
        q.SQL.Add('COMPUTER_ID= :cid AND');
        q.SQL.Add('DRIVE_ID= :did AND');
        q.SQL.Add('PATH = :path');

        q.Params[0].AsInteger := givenFolder.Parent_ID;
        q.Params[1].AsInteger := givenFolder.Computer_ID;
        q.Params[2].AsInteger := givenFolder.Drive_ID;
        q.Params[3].AsString := givenFolder.Path;

        try
            q.Open;
            if not q.Eof then
                result := q.Fields[0].AsInteger;

        finally
            finishtick(t1, 'GetFolder', inttostr(result) + ': ' + givenFolder.Path);
            qpreset;
        end;

    end;
end;

function TclassFolderReadDB.insertFolder(const folderToAdd: TDBFolder): TDBFolder;
var
    ID: integer;
    uts: int64;
    t1: Cardinal;
begin
    result := folderToAdd;

    if q.database.Connected then
    begin
        t1 := starttick;
        qpreset;

        ID := getNextIDOfTable(T_ORDNER);
        uts := getUnixTimeStamp;

        q.SQL.Add('INSERT INTO ' + T_ORDNER);
        q.SQL.Add('("ID", "PARENT_ID", "' + T_COMPUTER + '_ID", "' + T_LAUFWERK +
                '_ID", "PATH", "CAPTION", "DELETED", "CREATE_DATE", "ADD_DATE")');
        q.SQL.Add('VALUES');
        q.SQL.Add('(:id, :pid, :cid, :did, :path, :caption, :deleted, :datum, :datum2 );');

        q.Params[0].AsInteger := ID;
        q.Params[1].AsInteger := folderToAdd.Parent_ID;
        q.Params[2].AsInteger := folderToAdd.Computer_ID;
        q.Params[3].AsInteger := folderToAdd.Drive_ID;
        q.Params[4].AsString := folderToAdd.Path;
        q.Params[5].AsString := folderToAdd.Caption;
        q.Params[6].AsInteger := 0;
        q.Params[7].AsLargeInt := folderToAdd.create_date;
        q.Params[8].AsLargeInt := uts;

        try
            try
                q.ExecSQL;
                result.ID := ID;
                result.add_date := uts;
            except
                on E: Exception do
                begin

                end;
            end;
        finally
            finishtick(t1, 'insertFolder', inttostr(ID) + ': ' + folderToAdd.Path);
            qpreset;
        end;
    end;
end;

function TclassFolderReadDB.addDBFolder(const givenFolder: TDBFolder): integer;
var
    l: integer;
begin
    l := length(DBFolders);
    setlength(DBFolders, l + 1);
    DBFolders[l] := givenFolder;

    result := l;
end;

procedure TclassFolderReadDB.loadDBFolders;
var
    t1: Cardinal;
begin
    if q.database.Connected then
    begin
        t1 := starttick;
        qpreset;

        q.SQL.Add('SELECT');
        q.SQL.Add('ID,');
        q.SQL.Add('PARENT_ID,');
        q.SQL.Add(T_COMPUTER + '_ID,');
        q.SQL.Add(T_LAUFWERK + '_ID,');
        q.SQL.Add('PATH,');
        q.SQL.Add('CAPTION,');
        q.SQL.Add('DELETED,');
        q.SQL.Add('CREATE_DATE,');
        q.SQL.Add('ADD_DATE');
        q.SQL.Add('FROM ' + T_ORDNER);
        q.SQL.Add('WHERE');
        q.SQL.Add('PARENT_ID = :parentID');

        q.Params[0].AsInteger := DataSet.folder.ID;

        try
            try
                q.Open;

                while not q.Eof do
                begin
                    setlength(DBFolders, q.RecNo);

                    DBFolders[q.RecNo - 1].ID := q.FieldByName('ID').AsInteger;
                    DBFolders[q.RecNo - 1].Parent_ID := q.FieldByName('PARENT_ID').AsInteger;
                    DBFolders[q.RecNo - 1].Computer_ID := q.FieldByName(T_COMPUTER + '_ID').AsInteger;
                    DBFolders[q.RecNo - 1].Drive_ID := q.FieldByName(T_LAUFWERK + '_ID').AsInteger;
                    DBFolders[q.RecNo - 1].Path := q.FieldByName('PATH').AsString;
                    DBFolders[q.RecNo - 1].Caption := q.FieldByName('CAPTION').AsString;
                    DBFolders[q.RecNo - 1].Deleted := q.FieldByName('DELETED').AsInteger;
                    DBFolders[q.RecNo - 1].create_date := q.FieldByName('CREATE_DATE').AsLargeInt;
                    DBFolders[q.RecNo - 1].add_date := q.FieldByName('ADD_DATE').AsLargeInt;

                    q.Next;
                end;
                initialFolderCount := q.RecordCount;
            except
                on E: Exception do
                begin

                end;
            end;
        finally
            finishtick(t1, 'loadDBFolders', inttostr(DataSet.folder.ID) + ': ' + DataSet.Path + ' #' +
                    inttostr(q.RecordCount));
            qpreset;

        end;

    end;
end;

procedure TclassFolderReadDB.loadDBFiles;
var
    t1: Cardinal;
begin
    if q.database.Connected then
    begin
        t1 := starttick;
        qpreset;

        q.SQL.Add('SELECT');
        q.SQL.Add('ID,');
        q.SQL.Add(T_COMPUTER + '_ID,');
        q.SQL.Add(T_LAUFWERK + '_ID,');
        q.SQL.Add(T_ORDNER + '_ID,');
        q.SQL.Add(T_ICON + '_ID,');
        q.SQL.Add(T_ERWEITERUNGEN + '_ID,');
        q.SQL.Add('RATING,');
        q.SQL.Add('FILENAME,');
        q.SQL.Add('FSIZE,');
        q.SQL.Add('CAPTION,');
        q.SQL.Add('CONTENTHASH,');
        q.SQL.Add('ISORIGINAL,');
        q.SQL.Add('ISDUBLETTE,');
        q.SQL.Add('ISBACKUP,');
        q.SQL.Add('ISTAGGED,');
        q.SQL.Add('DELETED,');
        q.SQL.Add('CREATE_DATE,');
        q.SQL.Add('HASH_DATE,');
        q.SQL.Add('ADD_DATE');
        q.SQL.Add('FROM');
        q.SQL.Add(T_DATEIEN);
        q.SQL.Add('WHERE');
        q.SQL.Add(T_ORDNER + '_ID = :ordnerID');

        q.Params[0].AsInteger := DataSet.folder.ID;

        try
            try
                q.Open;
                while not q.Eof do
                begin
                    setlength(DBFiles, q.RecNo);

                    DBFiles[q.RecNo - 1].ID := q.FieldByName('ID').AsInteger;
                    DBFiles[q.RecNo - 1].Computer_ID := q.FieldByName(T_COMPUTER + '_ID').AsInteger;
                    DBFiles[q.RecNo - 1].Drive_ID := q.FieldByName(T_LAUFWERK + '_ID').AsInteger;
                    DBFiles[q.RecNo - 1].Folder_ID := q.FieldByName(T_ORDNER + '_ID').AsInteger;
                    DBFiles[q.RecNo - 1].Icon_ID := q.FieldByName(T_ICON + '_ID').AsInteger;
                    DBFiles[q.RecNo - 1].Ext_ID := q.FieldByName(T_ERWEITERUNGEN + '_ID').AsInteger;
                    DBFiles[q.RecNo - 1].Rating := q.FieldByName('RATING').AsInteger;
                    DBFiles[q.RecNo - 1].Filename := q.FieldByName('FILENAME').AsString;
                    DBFiles[q.RecNo - 1].FSize := q.FieldByName('ID').AsInteger;
                    DBFiles[q.RecNo - 1].Caption := q.FieldByName('CAPTION').AsString;
                    DBFiles[q.RecNo - 1].Contenthash := q.FieldByName('CONTENTHASH').AsString;
                    DBFiles[q.RecNo - 1].isOriginal := q.FieldByName('ISORIGINAL').AsInteger;
                    DBFiles[q.RecNo - 1].isDublette := q.FieldByName('ISDUBLETTE').AsInteger;
                    DBFiles[q.RecNo - 1].isBackup := q.FieldByName('ISBACKUP').AsInteger;
                    DBFiles[q.RecNo - 1].isTagged := q.FieldByName('ISTAGGED').AsInteger;
                    DBFiles[q.RecNo - 1].Deleted := q.FieldByName('DELETED').AsInteger;
                    DBFiles[q.RecNo - 1].create_date := q.FieldByName('CREATE_DATE').AsLargeInt;
                    DBFiles[q.RecNo - 1].hash_date := q.FieldByName('HASH_DATE').AsLargeInt;
                    DBFiles[q.RecNo - 1].add_date := q.FieldByName('ADD_DATE').AsLargeInt;

                    q.Next;
                end;
                initialFileCount := q.RecordCount;
            except
                on E: Exception do
                begin

                end;
            end;
        finally
            finishtick(t1, 'loadDBFiles', inttostr(DataSet.folder.ID) + ': ' + DataSet.Path + ' #' +
                    inttostr(q.RecordCount));
            qpreset;
        end;

    end;
end;

function TclassFolderReadDB.insertFile(const fileToAdd: TDBFile): TDBFile;
var
    ID: integer;
    uts: int64;
    t1: Cardinal;

begin
    result := fileToAdd;

    if q.database.Connected then
    begin
        t1 := starttick;
        qpreset;

        ID := getNextIDOfTable(T_DATEIEN);
        uts := getUnixTimeStamp;

        q.SQL.Add('INSERT INTO ' + T_DATEIEN);
        q.SQL.Add('("ID", "' + T_COMPUTER + '_ID", "' + T_LAUFWERK + '_ID", "' + T_ORDNER + '_ID","' + T_ICON + '_ID","'
                + T_ERWEITERUNGEN +
                '_ID",  "RATING", "FILENAME", "FSIZE", "CAPTION", "CONTENTHASH","ISORIGINAL","ISDUBLETTE","ISBACKUP","ISTAGGED","DELETED","CREATE_DATE","HASH_DATE","ADD_DATE")');
        q.SQL.Add('VALUES');
        q.SQL.Add(
            '(:id, :cid, :did, :oid, :iid, :eid, :rating, :filename, :fsize, :caption, :contenthash, :isoriginal, :isdublette, :isbackup, :istagged, :deleted, :create_date, :hash_date, :add_date);');

        q.Params[0].AsInteger := ID;
        q.Params[1].AsInteger := fileToAdd.Computer_ID;
        q.Params[2].AsInteger := fileToAdd.Drive_ID;
        q.Params[3].AsInteger := fileToAdd.Folder_ID;
        q.Params[4].AsInteger := fileToAdd.Icon_ID;
        q.Params[5].AsInteger := fileToAdd.Ext_ID;
        q.Params[6].AsInteger := fileToAdd.Rating;
        q.Params[7].AsString := fileToAdd.Filename;
        q.Params[8].AsLargeInt := fileToAdd.FSize;
        q.Params[9].AsString := fileToAdd.Caption;
        q.Params[10].AsString := fileToAdd.Contenthash;
        q.Params[11].AsInteger := fileToAdd.isOriginal;
        q.Params[12].AsInteger := fileToAdd.isDublette;
        q.Params[13].AsInteger := fileToAdd.isBackup;
        q.Params[14].AsInteger := fileToAdd.isTagged;
        q.Params[15].AsInteger := fileToAdd.Deleted;
        q.Params[16].AsLargeInt := fileToAdd.create_date;
        q.Params[17].AsLargeInt := fileToAdd.hash_date;
        q.Params[18].AsLargeInt := uts;

        try
            try
                q.ExecSQL;
                result.ID := ID;
                result.add_date := uts;
            except
                on E: Exception do
                begin

                end;
            end;
        finally
            finishtick(t1, 'insertFile', inttostr(ID) + ': ' + fileToAdd.Filename);
            qpreset;
        end;
    end;
end;

function TclassFolderReadDB.updateContentHash(const ID: integer; const hash: string): boolean;
var
    uts: int64;
    t1: Cardinal;
begin
    if q.database.Connected then
    begin
        t1 := starttick;
        qpreset;

        uts := getUnixTimeStamp;

        q.SQL.Add('UPDATE ' + T_DATEIEN);
        q.SQL.Add('SET CONTENTHASH= :hash, HASH_DATE = :datum');
        q.SQL.Add('WHERE');
        q.SQL.Add('ID = :id');

        q.Params[0].AsString := hash;
        q.Params[1].AsLargeInt := uts;
        q.Params[2].AsInteger := id;

        try
            try
                q.ExecSQL;
            except

            end;
        finally
            finishtick(t1, 'Update Hash', inttostr(ID) + ': ' + hash);
            qpreset;
        end;


    end;
end;

function TclassFolderReadDB.addDBFile(const givenFile: TDBFile): integer;
var
    l: integer;
begin
    l := length(DBFiles);
    setlength(DBFiles, l + 1);
    DBFiles[l] := givenFile;

    result := l;
end;

function TclassFolderReadDB.getDBFileID(const givenFile: TDBFile): integer;
var
    uts: int64;
    t1: Cardinal;
begin
    result := 0;

    if q.database.Connected then
    begin
        t1 := starttick;
        qpreset;

        uts := getUnixTimeStamp;

        q.SQL.Add('SELECT');
        q.SQL.Add('ID');
        q.SQL.Add('FROM ' + T_DATEIEN);
        q.SQL.Add('WHERE');
        q.SQL.Add(T_ORDNER + '_ID= :fid AND');
        q.SQL.Add(T_COMPUTER + '_ID= :cid AND');
        q.SQL.Add(T_LAUFWERK + '_ID= :did AND');
        q.SQL.Add('FILENAME = :filename');

        q.Params[0].AsInteger := givenFile.Folder_ID;
        q.Params[1].AsInteger := givenFile.Computer_ID;
        q.Params[2].AsInteger := givenFile.Drive_ID;
        q.Params[3].AsString := givenFile.Filename;

        try
            q.Open;
            if not q.Eof then
                result := q.Fields[0].AsInteger;

        finally
            finishtick(t1, 'GetFolder', inttostr(result) + ': ' + givenFile.Filename);
            qpreset;
        end;

    end;
end;

end.
