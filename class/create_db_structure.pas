﻿unit create_db_structure;

interface

uses IBX.IBDatabase, IBX.IBQuery, create_empty_db, u_DM_type_def;

type
    TClassCreateDBStructure = class(TClassCreateEmptyDatabase)
    private
        function addDBIndex(iq: tibquery; const table, field: string): boolean;
        function createTable_Computer(iq: tibquery): boolean;
        function createTable_Drive(iq: tibquery): boolean;
        function createTable_Folder(iq: tibquery): boolean;
        function createTable_Extensions(iq: tibquery): boolean;
        function createTable_File(iq: tibquery): boolean;
        // function createTable_FilesHistory(iq: tibquery): boolean;
        function createTable_Icons(iq: tibquery): boolean;
        function createTable_TagKategorie(iq: tibquery): boolean;
        function createTable_Tags(iq: tibquery): boolean;
        function createTable_imgdata(iq: tibquery): boolean;
        function createTable_CamManufactor(iq: tibquery): boolean;
        function createTable_CamModell(iq: tibquery): boolean;
        function createTable_Index(iq: tibquery): boolean;

    public
        function createDB(const pfad: string): boolean;
    end;

implementation

function TClassCreateDBStructure.createDB(const pfad: string): boolean;
var
    db: TIBDatabase;
    t: TIBTransaction;
    q: tibquery;
begin
    result := false;

    if createEmptyDB(pfad) then
    begin
        db := TIBDatabase.create(nil);
        try
            t := TIBTransaction.create(nil);
            try
                q := tibquery.create(nil);
                try
                    db.Params.Clear;
                    db.Params.Add('user_name=sysdba');
                    db.Params.Add('password=masterkey');
                    db.Params.Add('lc_ctype=UTF8');

                    db.DatabaseName := pfad;
                    db.LoginPrompt := false;
                    db.DefaultTransaction := t;
                    db.IdleTimer := 0;
                    db.SQLDialect := 3;
                    db.TraceFlags := [];

                    t.Active := false;
                    t.DefaultDatabase := db;
                    t.AutoStopAction := saCommit;

                    q.Database := db;
                    q.Transaction := t;
                    q.CachedUpdates := false;
                    q.BufferChunks := 1000;

                    db.Open;

                    createTable_Index(q);
                    createTable_Computer(q);
                    createTable_Drive(q);
                    createTable_Folder(q);
                    createTable_Extensions(q);
                    createTable_File(q);
                    // createTable_FilesHistory(q);
                    createTable_Icons(q);
                    createTable_Tags(q);
                    createTable_TagKategorie(q);

                    createTable_imgdata(q);
                    createTable_CamManufactor(q);
                    createTable_CamModell(q);

                    result := db.Connected;

                finally
                    q.SQL.Clear;
                    q.Params.Clear;
                    q.Free;
                end;
            finally
                t.Free;
            end;
        finally
            db.Free;
        end;
    end;
end;

function TClassCreateDBStructure.addDBIndex(iq: tibquery; const table, field: string): boolean;
begin
    result := false;

    iq.SQL.Text := 'CREATE INDEX ' + table + '_' + field + ' ON ' + table + '(' + field + ');';

    try
        iq.ExecSQL;
        result := true;
        iq.Close;
        iq.SQL.Clear;
    except

    end;
end;

function TClassCreateDBStructure.createTable_Computer(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "' + T_COMPUTER + '" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"NAME" VARCHAR(32) CHARACTER SET "UTF8",');

    iq.SQL.Add('"ADD_DATE" BIGINT');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        iq.SQL.Clear;

        addDBIndex(iq, T_COMPUTER, 'ID');
        addDBIndex(iq, T_COMPUTER, 'NAME');

        result := true;
    except

    end;

end;

function TClassCreateDBStructure.createTable_Drive(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "' + T_LAUFWERK + '" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"' + T_COMPUTER + '_ID" INTEGER,');
    iq.SQL.Add('"' + T_ORDNER + '_ID" INTEGER,');

    iq.SQL.Add('"DSIZE" BIGINT,');
    iq.SQL.Add('"SERIALNR" VARCHAR(32) CHARACTER SET "UTF8",');
    iq.SQL.Add('"CAPTION" VARCHAR(64) CHARACTER SET "UTF8",');

    iq.SQL.Add('"ADD_DATE" BIGINT');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        iq.SQL.Clear;

        addDBIndex(iq, T_LAUFWERK, 'ID');
        addDBIndex(iq, T_LAUFWERK, 'SERIALNR');

        result := true;
    except

    end;
end;

function TClassCreateDBStructure.createTable_Folder(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "' + T_ORDNER + '" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"PARENT_ID" INTEGER,');
    iq.SQL.Add('"' + T_COMPUTER + '_ID" INTEGER,');
    iq.SQL.Add('"' + T_LAUFWERK + '_ID" INTEGER,');

    iq.SQL.Add('"PATH" VARCHAR(255) CHARACTER SET "UTF8",');

    iq.SQL.Add('"CAPTION" VARCHAR(128) CHARACTER SET "UTF8",');
    iq.SQL.Add('"DELETED" INTEGER,'); // marks this Folder as Deleted

    iq.SQL.Add('"CREATE_DATE" BIGINT ,');
    iq.SQL.Add('"ADD_DATE" BIGINT');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        iq.SQL.Clear;

        addDBIndex(iq, T_ORDNER, 'ID');
        addDBIndex(iq, T_ORDNER, 'PARENT_ID');
        addDBIndex(iq, T_ORDNER, 'DELETED');

        result := true;
    except

    end;
end;

function TClassCreateDBStructure.createTable_File(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "' + T_DATEIEN + '" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"' + T_COMPUTER + '_ID" INTEGER,');
    iq.SQL.Add('"' + T_LAUFWERK + '_ID" INTEGER,');
    iq.SQL.Add('"' + T_ORDNER + '_ID" INTEGER,');
    iq.SQL.Add('"' + T_ICON + '_ID" INTEGER,');
    iq.SQL.Add('"' + T_ERWEITERUNGEN + '_ID" INTEGER,');
    iq.SQL.Add('"RATING" INTEGER,');

    iq.SQL.Add('"FILENAME" VARCHAR(150) CHARACTER SET "UTF8" ,');
    iq.SQL.Add('"FSIZE" BIGINT ,');

    iq.SQL.Add('"CAPTION" VARCHAR(150) CHARACTER SET "UTF8" ,');
    iq.SQL.Add('"CONTENTHASH" VARCHAR(64) CHARACTER SET "UTF8" ,');

    iq.SQL.Add('"ISORIGINAL" INTEGER,');
    iq.SQL.Add('"ISDUBLETTE" INTEGER,');
    iq.SQL.Add('"ISBACKUP" INTEGER,');
    iq.SQL.Add('"ISTAGGED" INTEGER,');
    iq.SQL.Add('"DELETED" INTEGER,'); // marks this Folder as Deleted

    iq.SQL.Add('"CREATE_DATE" BIGINT,');
    iq.SQL.Add('"HASH_DATE" BIGINT,');

    iq.SQL.Add('"ADD_DATE" BIGINT');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        iq.SQL.Clear;

        addDBIndex(iq, T_DATEIEN, 'ID');
        addDBIndex(iq, T_DATEIEN, T_ORDNER + '_ID');
        addDBIndex(iq, T_DATEIEN, 'RATING');
        addDBIndex(iq, T_DATEIEN, 'DELETED');
        // addDBIndex(iq, T_DATEIEN, T_ORDNER+'_ID');
        addDBIndex(iq, T_DATEIEN, 'FSIZE');
        addDBIndex(iq, T_DATEIEN, 'CAPTION');

        iq.SQL.Text := 'CREATE INDEX FID_FN ON ' + T_DATEIEN + '(FILENAME);';
        try
            iq.ExecSQL;
            iq.Close;
            iq.SQL.Clear;
        except

        end;
        iq.SQL.Text := 'CREATE INDEX FID_CH ON ' + T_DATEIEN + '(CONTENTHASH);';
        try
            iq.ExecSQL;
            iq.Close;
            iq.SQL.Clear;
        except

        end;
        iq.SQL.Text := 'CREATE INDEX FID_SCH ON ' + T_DATEIEN + '(FSIZE,CONTENTHASH);'; // hash size ID
        try
            iq.ExecSQL;
            iq.Close;
            iq.SQL.Clear;
        except

        end;

        result := true;
    except

    end;
end;

function TClassCreateDBStructure.createTable_Extensions(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "' + T_ERWEITERUNGEN + '" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"EXT" VARCHAR(32) CHARACTER SET "UTF8"');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        iq.SQL.Clear;

        addDBIndex(iq, T_ERWEITERUNGEN, 'ID');
        addDBIndex(iq, T_ERWEITERUNGEN, 'EXT');

        result := true;
    except

    end;
end;

function TClassCreateDBStructure.createTable_Icons(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "' + T_ICON + '" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"' + T_ERWEITERUNGEN + '_ID" INTEGER ,');
    iq.SQL.Add('"DESCRIPTION" VARCHAR(64) CHARACTER SET "UTF8" ,');
    iq.SQL.Add('"RAW" BLOB');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        iq.SQL.Clear;

        addDBIndex(iq, T_ICON, 'ID');
        addDBIndex(iq, T_ICON, T_ERWEITERUNGEN + '_ID');
    except

    end;

end;

function TClassCreateDBStructure.createTable_Tags(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "' + T_TAG + '" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"' + T_DATEIEN + '_ID" INTEGER,');
    iq.SQL.Add('"' + T_KATEGORIE + '_ID" INTEGER,');
    iq.SQL.Add('"CAPTION" VARCHAR(24) CHARACTER SET "UTF8"');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;

        addDBIndex(iq, T_TAG, 'ID');
        addDBIndex(iq, T_TAG, T_DATEIEN + '_ID');
        addDBIndex(iq, T_TAG, T_KATEGORIE + '_ID');
        addDBIndex(iq, T_TAG, 'CAPTION');

        result := true;
    except

    end;

end;

function TClassCreateDBStructure.createTable_TagKategorie(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "' + T_KATEGORIE + '" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"CAPTION" VARCHAR(16) CHARACTER SET "UTF8"');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        addDBIndex(iq, T_KATEGORIE, 'ID');
        addDBIndex(iq, T_KATEGORIE, 'CAPTION');

        result := true;
    except

    end;
end;

function TClassCreateDBStructure.createTable_imgdata(iq: tibquery): boolean;
begin
    result := false;
    {
      Aufnahmedatum
      Höhe & Breite in Pixeln
      Auflösung
      Bittiefe
      Kamerahersteller
      Kameramodell
      Brennweite
      Blende
      Belichtungszeit
      ISO-Wert
      Blitzeinstellung
      Weißabgleich
    }
    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "' + T_BILDER + '" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"SERIES_REF_ID" INTEGER,');
    iq.SQL.Add('"EXIF_DATE" BIGINT,');
    iq.SQL.Add('"EXIF_' + T_CAMMANUFACTOR + '_ID" INTEGER,');
    iq.SQL.Add('"EXIF_' + T_CAMMODELL + '_ID" INTEGER,');
    iq.SQL.Add('"EXIF_LON" FLOAT,');
    iq.SQL.Add('"EXIF_LAT" FLOAT,');
    iq.SQL.Add('"EXIF_ALT" INTEGER,');
    iq.SQL.Add('"EXIF_X" INTEGER,');
    iq.SQL.Add('"EXIF_Y" INTEGER,');
    iq.SQL.Add('"EXIF_DESC" VARCHAR(250) CHARACTER SET "UTF8",');
    iq.SQL.Add('"FILE_X" INTEGER,');
    iq.SQL.Add('"FILE_Y" INTEGER,');

    iq.SQL.Add('"EXIF_BRENN" VARCHAR(12) CHARACTER SET "UTF8",');
    iq.SQL.Add('"EXIF_BLENDE" VARCHAR(12) CHARACTER SET "UTF8",');
    iq.SQL.Add('"EXIF_BELICH" VARCHAR(12) CHARACTER SET "UTF8",');
    iq.SQL.Add('"EXIF_ISO" INTEGER,');

    iq.SQL.Add('"DATUM" BIGINT,');
    iq.SQL.Add('"' + T_CAMMANUFACTOR + '_ID" INTEGER,');
    iq.SQL.Add('"' + T_CAMMODELL + '_ID" INTEGER,');
    iq.SQL.Add('"DESC" VARCHAR(250) CHARACTER SET "UTF8" ,');

    iq.SQL.Add('"LON" FLOAT,');
    iq.SQL.Add('"LAT" FLOAT,');
    iq.SQL.Add('"ALT" INTEGER,');
    iq.SQL.Add('"X" INTEGER,');
    iq.SQL.Add('"Y" INTEGER,');

    iq.SQL.Add('"CHANGE_DATE" BIGINT,');
    iq.SQL.Add('"ADD_DATE" BIGINT ');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;

        addDBIndex(iq, T_BILDER, 'ID');
        addDBIndex(iq, T_BILDER, T_CAMMANUFACTOR + '_ID');
        addDBIndex(iq, T_BILDER, T_CAMMODELL + '_ID');
        addDBIndex(iq, T_BILDER, 'DATUM');
        addDBIndex(iq, T_BILDER, 'LON');
        addDBIndex(iq, T_BILDER, 'LAT');
        addDBIndex(iq, T_BILDER, 'ALT');
        addDBIndex(iq, T_BILDER, 'X');
        addDBIndex(iq, T_BILDER, 'Y');

        iq.SQL.Text := 'CREATE INDEX GID_LLA ON ' + T_BILDER + '(LON,LAT,ALT);';
        try
            iq.ExecSQL;
            iq.Close;
        except

        end;

        iq.SQL.Text := 'CREATE INDEX GID_LL ON ' + T_BILDER + '(LON,LAT);';
        try
            iq.ExecSQL;
            iq.Close;
        except

        end;

        iq.SQL.Text := 'CREATE INDEX GID_XY ON ' + T_BILDER + '(X,Y);';
        try
            iq.ExecSQL;
            iq.Close;
        except

        end;

        result := true;
    except

    end;
end;

function TClassCreateDBStructure.createTable_CamManufactor(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "' + T_CAMMANUFACTOR + '" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"MANUFACTOR" VARCHAR(100) CHARACTER SET "UTF8"');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;

        addDBIndex(iq, T_CAMMANUFACTOR, 'ID');
        addDBIndex(iq, T_CAMMANUFACTOR, 'MANUFACTOR');
    finally

    end;
end;

function TClassCreateDBStructure.createTable_CamModell(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "' + T_CAMMODELL + '" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"MODELL" VARCHAR(100) CHARACTER SET "UTF8"');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;

        addDBIndex(iq, T_CAMMODELL, 'ID');
        addDBIndex(iq, T_CAMMODELL, 'MODELL');
    finally

    end;
end;

function TClassCreateDBStructure.createTable_Index(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "INDIZES" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"' + T_COMPUTER + '_ID" INTEGER,');
    iq.SQL.Add('"DRIVE_ID" INTEGER,');
    iq.SQL.Add('"FOLDER_ID" INTEGER,');
    iq.SQL.Add('"FILE_ID" INTEGER,');
    iq.SQL.Add('"EXT_ID" INTEGER,');
    iq.SQL.Add('"ICON_ID" INTEGER,');
    iq.SQL.Add('"CAMMAN_ID" INTEGER,');
    iq.SQL.Add('"CAMMOD_ID" INTEGER,');
    iq.SQL.Add('"TAG_ID" INTEGER,');
    iq.SQL.Add('"KATEGORIE_ID" INTEGER');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;

        addDBIndex(iq, 'INDIZES', 'ID');
    finally

        iq.SQL.Clear;
        iq.SQL.Add('INSERT INTO INDIZES');
        iq.SQL.Add('("ID","' + T_COMPUTER +
                '_ID","DRIVE_ID","FOLDER_ID","FILE_ID","EXT_ID","ICON_ID","CAMMAN_ID","CAMMOD_ID","TAG_ID","KATEGORIE_ID")');
        iq.SQL.Add('VALUES');
        iq.SQL.Add('(:ID, :' + T_COMPUTER +
                '_ID, :DRIVE_ID, :FOLDER_ID, :FILE_ID, :EXT_ID, :ICON_ID, :CAMMAN_ID, :CAMMOD_ID, :TAG_ID, :KATEGORIE_ID);');

        iq.Params[0].AsInteger := 1;
        iq.Params[1].AsInteger := 1;
        iq.Params[2].AsInteger := 1;
        iq.Params[3].AsInteger := 1;
        iq.Params[4].AsInteger := 1;
        iq.Params[5].AsInteger := 1;
        iq.Params[6].AsInteger := 1;
        iq.Params[7].AsInteger := 1;
        iq.Params[8].AsInteger := 1;
        iq.Params[9].AsInteger := 1;
        iq.Params[10].AsInteger := 1;
        try
            iq.ExecSQL;
            iq.Close;
        finally

        end;

    end;
end;

end.
