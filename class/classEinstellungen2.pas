unit classFilesystem;

interface

uses SysUtils, Windows, FileCtrl, Classes, IdHashMessageDigest, Forms,
    IBX.IBQuery, ShellApi, Dialogs, System.IOUtils;

type TMyFile = record
        folder:string;
        name:string;
        size:int64;
        timestamp : TDateTime;

        md5:string;
        ID:integer;
        end;

type TMyFileList = array of TMyFile;

type TMyFolder = record
        folder:string;
        size:int64;
        filesCount:int;
        ID:integer;
        end;

type TMyFolderList = array of TMyFolder;

type TMyDrive = record  
        letter:char;
        name:string;
        number:string;

        ID:integer;
        end;

type TMySystem = record
        name:string;

        ID:integer;
        end;

type
    TClassFileSystem = class
    private
        FInitialDir:string;
        FFolderCount : integer;
        FFilesCount :integer;

        FFilesList : TMyFileList;
        FFolderList: TMyFolderList;
        FDrive: TMyDrive;
        FSystem:TMySystem;

        FFileMask : string;
        FWithSubDirs:boolean;
        FMinSize:integer;
        FMaxSize:integer;

        function getFolderCount:integer:
        function getFilesCount:integer;
        function getVolumeName:string;
        function getVolumeSerialNumber:string;
        
        procedure setInitialDir(const dir:string);
        procedure setFileMask(const mask:string);
        procedure setMinFilesize(const size:int64);
        procedure setMaxFilesize(const size:int64);
        procedure setWithSubDirs(const withDirs:boolean);


        // procedure addFolder ( const dir: string; up: TUpdateProgress ) ;
        // procedure addCountFilesOfFolder ( const dir: string; up: TUpdateProgress ) ;
        // function FileTimeToDateTime ( FileTime: TFileTime ) : TDateTime;
        // procedure processDirectory ( action: TMyAction; up: TUpdateProgress ) ;
        // function getSizeBySR ( sr: TSearchRec ) : int64;
        // function getNameMD5ofMyFile ( f: TMyFile ) : string;
        // function getInfoFromSearchRec ( const dir, folder, pcNameAndSerial: string; SR: TSearchRec ) : TMyFile;
        // function UpdateMyFileFromSearchRec ( var f: TMyFile; SR: TSearchRec ) : boolean;

        // function isDriveRoot ( const dir: string ) : boolean;

    public

        constructor create;
        destructor destroy; override;

        // procedure setCancelRequest;
        // procedure setStarting;
        // procedure setRootIconIndex ( index: integer ) ;

        //
        property Directory: string read FInitialDir write setInitialDir;
        property WithSubDirectories: boolean read FWithSubDirs write setWithSubDirs;
        Property FileMask: string read FFileMask write setFileMask;
        Property MinSize: int64 read FMinSize write setMinFilesize;
        property MaxSize:int64 read FMaxSize write setMaxFilesize;
        property VolumeName: string read getVolumeName;
        property VolumeSerialNumber: string getVolumeSerialNumber;

        property Folders: integer read getFolderCount;
        property Files:integer read getFilesCount;

        /// Returns the logical drive serial number
        function retrieveDriveSerialNumber(const folder: string): string;
        function retrieveVolumeName(DriveLetter: Char): string;
        function getCountFilesInDir(const dir: string): integer;
        function getFilesInDir(const dir: string): TStringlist;

        procedure process;
        // function extractFolder ( const path: string ) : string;
        //
        // function FILEMD5 ( const fileName: string ) : string;
        // function NAMEMD5 ( const s: string ) : string;
        //
        // procedure createDirectoryOutline ( action: TMyAction; up: TUpdateProgress ) ;
        //
        // procedure processDirectoryOutline ( action: TMyAction; insert: TInsert; up: TUpdateProgress ) ;
        // procedure processFilesInDir ( action: TMyAction; const dir: string; insert: TInsert; up: TUpdateProgress ) ;
        //
        // procedure execute ( const uri: string ) ;
        //
        // function getHashAndTimeOfFile ( f: TMyFile ) : TMyFile;
        // function DeleteFileWithUndo ( sFileName: string ) : Boolean;
        //
    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////

constructor TClassFileSystem.create;
begin
    inherited create;

    FFileMask := '*.*';
    FWithSubDirs := false;
    FMinSize := -1;
    FMaxSize := -1;
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////

destructor TClassFileSystem.destroy;
begin
    if assigned(directoryList) then
        directoryList.destroy;
    FFileMask := '';
    FMinSize := 0;
    FMaxSize := 0;

    inherited destroy;
end;

function TClassFileSystem.getFolderCount:integer;
begin
    if FFolderCount = -1 and FInitialDir!='' then
    begin
        FFolderCount := length(FFolderList);
    end;

    result := FFolderCount;
end;

function TClassFileSystem.getFilesCount:integer;
begin
    if FFilesCount= -1 and FInitialDir!='' then
    begin
        FFilesCount := length(FFilesList);
    end;
    result := FFilesCount;
end;

procedure TClassFileSystem.setInitialDir(const dir:string);
begin
    FInitialDir := IncludeTrailingPathDelimiter(dir);

    FFolderCount := -1;
    FFilesCount := -1;

    setLength(FFilesList,0);
    setLength(FFolderList,0);

    FDrive.ID :=0;
    FDrive.name:='';
    FDrive.number := '';
    FDrive.letter:= dir[1];

    FSystem.ID:=0;
    FSystem.name := '';
end;


procedure TClassFileSystem.setFileMask(const mask:string);
begin
    FFileMask := mask;
    FFolderCount := -1;
    FFilesCount := -1;    
end;

procedure TClassFileSystem.setMinFilesize(const size:int64);
begin
    FMinSize := size;
    FFolderCount := -1;
    FFilesCount := -1;
end;

procedure TClassFileSystem.setMaxFilesize(const size:int64);
begin
    FMaxSize := size;
    FFolderCount := -1;
    FFilesCount := -1;
end;

procedure TClassFileSystem.setWithSubDirs(const withDirs:boolean);
begin
    FWithSubDirs := withDirs;
    FFolderCount := -1;
    FFilesCount := -1;
end;

        function TClassFileSystem.getVolumeName:string;
        begin
            if(FDrive.name!='')then
            begin
                FDrive.name := retrieveVolumeName(FDrive.letter);                
            end;
            result := FDrive.name;
        end;
        
        function TClassFileSystem.getVolumeSerialNumber:string;

// procedure ClassFS.setCancelRequest;
// begin
// cancelrequest := true;
// end;
//
// procedure ClassFS.setStarting;
// begin
// cancelrequest := false;
// end;
//
// procedure ClassFS.setRootIconIndex ( index: integer ) ;
// begin
// rootIconIndex := index;
// end;
/// ///////////////////////////
// Reading the logical serial number of the drive of the given folder
// Result varies after formatting, Number is given by OS
/// ///////////////////////////

function TClassFileSystem.retrieveDriveSerialNumber(const folder: string): string;
var
    drive: string;
    FileSysName, VolName: array [0 .. 255] of Char;
    SerialNum, MaxCLength, FileSysFlag: DWORD;
begin
    result := '';
    drive := IncludeTrailingPathDelimiter(extractFileDrive(folder));
    GetVolumeInformation(PChar(drive), VolName, 255, @SerialNum, MaxCLength,
        FileSysFlag, FileSysName, 255);
    result := IntToHex(SerialNum, 8);
end;

function TClassFileSystem.retrieveVolumeName(DriveLetter: Char): string;
var
    dummy: DWORD;
    buffer: array [0 .. MAX_PATH] of Char;
    oldmode: LongInt;
begin
    oldmode := SetErrorMode(SEM_FAILCRITICALERRORS);
    try
        GetVolumeInformation(PChar(DriveLetter + ':\'), buffer, SizeOf(buffer),
            nil, dummy, dummy, nil, 0);
        result := StrPas(buffer);
    finally
        SetErrorMode(oldmode);
    end;
end;

/// ///////////////////////////
// Returns the amount of files in given direktory,
// not recursive
/// ///////////////////////////
//
// function TClassFileSystem.getCountFilesInDir ( const dir: string ) : integer;
// var
// d: TDirectory;
// begin
// result := 0;
//
// if SysUtils.directoryexists ( dir ) then
// begin
//
// try
// /// get amount of files in dir
// d := TDirectory.Create ( nil ) ;
// d.BeginUpdate;
// d.FileMask := '*.*';
// d.Location := ( Dir ) ;
// d.FileType := [ ftReadOnly, ftHidden, ftSystem, ftArchive, ftNormal ] ;
// d.EndUpdate;
//
// result := d.count;
// finally
// d.Free;
// end;
// end;
// end;

function TClassFileSystem.getCountFilesInDir(const dir: string): integer;
// function CountFilesInFolder ( path: string ): integer;
var
    path: string;
    tsr: TSearchRec;
begin
    path := IncludeTrailingPathDelimiter(dir);
    result := 0;
    if SysUtils.FindFirst(path + '*.*', faAnyFile or faDirectory, tsr) = 0 then
    begin
        repeat
            if (tsr.Attr and faDirectory) = 0 then
                inc(result)
            else if (tsr.Name <> '.') and (tsr.Name <> '..') then
                result := result + getCountFilesInDir
                    (IncludeTrailingBackslash(dir) + tsr.Name);
        until SysUtils.FindNext(tsr) <> 0;
        SysUtils.FindClose(tsr);
    end;
end;

function TClassFileSystem.getFilesInDir(const dir: string): TStringlist;
var
    path: string;
    tsr: TSearchRec;
begin
    path := IncludeTrailingPathDelimiter(dir);
    result := TStringlist.create;
    if SysUtils.FindFirst(path + '*.*', faAnyFile or faDirectory, tsr) = 0 then
    begin
        repeat
            if (tsr.Attr and faDirectory) = 0 then
                result.Add(tsr.Name + '; ' + tsr.Size.ToString)
            else if (tsr.Name <> '.') and (tsr.Name <> '..') then
                result.AddStrings(getFilesInDir(IncludeTrailingBackslash(dir) +
                            tsr.Name));

        until SysUtils.FindNext(tsr) <> 0;
        SysUtils.FindClose(tsr);
    end;
end;


procedure TClassFileSystem.process;
begin
//
end;
/// ///////////////////////////
// Returns the last folder of a given path
/// ///////////////////////////

// function ClassFS.extractFolder ( const path: string ) : string;
// var i: integer;
// begin
// result := '';
//
// if ( length ( path ) > 3 ) then
// begin
// for i := length ( path ) downto 0 do
// begin
// if ( path [ i ] = '\' ) then
// begin
// result := copy ( path, i + 1, length ( path ) - length ( result ) ) ;
// break;
// end;
// end;
// end;
// end;

/// ///////////////////////////
// Gets the MD5 Checksum of a given Filename
/// ///////////////////////////

// function ClassFS.FILEMD5 ( const fileName: string ) : string;
// var idmd5: TIdHashMessageDigest5;
// fs: TFileStream;
// begin
// result := '';
// idmd5 := TIdHashMessageDigest5.Create;
// fs := TFileStream.Create ( fileName, fmOpenRead or fmShareDenyWrite ) ;
//
// try
// result := idmd5.AsHex ( idmd5.HashValue ( fs ) ) ;
// finally
// fs.Free;
// idmd5.Free;
// end;
// end;

/// ///////////////////////////
// Gets the MD5 Checksum of a given string
/// ///////////////////////////

// function ClassFS.NAMEMD5 ( const s: string ) : string;
// var idmd5: TIdHashMessageDigest5;
// begin
// idmd5 := TIdHashMessageDigest5.Create;
//
// try
// result := idmd5.AsHex ( idmd5.HashValue ( uppercase ( s ) ) ) ;
// finally
// idmd5.Free;
// end;
// end;

/// ///////////////////////////
// Adds a folder to the directory List and fires a User_Message
/// ///////////////////////////

// procedure ClassFS.addFolder ( const dir: string; up: TUpdateProgress ) ;
// begin
// directoryList.Add ( dir ) ;
// inc ( folderCount ) ;
//
// // callback
// up ( 1, dir, folderCount ) ;
// end;

/// ///////////////////////////
// Adds the amount of files in given dir and fires a User_Message
/// ///////////////////////////

// procedure ClassFS.addCountFilesOfFolder ( const dir: string; up: TUpdateProgress ) ;
// begin
// filesCount := filesCount + getCountFilesInDir ( dir ) ;
//
// // callback
// up ( 2, '', filesCount ) ;
// end;

/// ///////////////////////////
// Creates a recursive directory List and counts the containing files
/// ///////////////////////////

// procedure ClassFS.processDirectory ( action: TMyAction; up: TUpdateProgress ) ;
// var
// SearchRec: TSearchRec;
// nextAction: TMyAction;
// begin
// action.folders.path := IncludeTrailingPathDelimiter ( action.folders.path ) ;
//
// addFolder ( action.folders.path, up ) ;
// addCountFilesOfFolder ( action.folders.path, up ) ;
//
// /// search for next folder
// if FindFirst ( action.folders.path + '*.*', faDirectory, SearchRec ) = 0 then
// try
// repeat
// if cancelrequest then
// begin
// sysutils.FindClose ( SearchRec ) ;
// exit;
// end;
//
// if ( action.subdirectories ) and ( ( SearchRec.Attr and faDirectory ) <> 0 ) then
// begin
// if ( SearchRec.Name <> '.' ) and ( SearchRec.Name <> '..' ) then
// begin
// nextAction := action;
// nextaction.folders.path := nextaction.folders.path + searchrec.Name;
// processDirectory ( nextaction, up ) ;                       // recursive call!
// end;
// end;
// until FindNext ( SearchRec ) <> 0;
// finally
// SysUtils.FindClose ( SearchRec ) ;
// end;
// end;

/// ///////////////////////////
// Creates the directory list for further processing
/// ///////////////////////////

// procedure ClassFS.createDirectoryOutline ( action: TMyAction; up: TUpdateProgress ) ;
// begin
// /// preparation
//
// folderCount := 0;
// filesCount := 0;
//
// if not assigned ( directoryList ) then
// begin
// directoryList := TStringlist.create;
// end else begin
// directoryList.Clear;
// end;
//
// processDirectory ( action, up ) ;
//
// end;
//
// procedure ClassFS.processDirectoryOutline ( action: TMyAction; insert: TInsert; up: TUpdateProgress ) ;
// var i: integer;
// begin
// filesCount := 0;
//
// if assigned ( directoryList ) then
// begin
//
// for i := 0 to directoryList.Count - 1 do
// begin
// up ( 3, directoryList [ i ] , i + 1 ) ;
//
// processFilesInDir ( action, directoryList [ i ] , insert, up ) ;
//
// end;
//
// end;
// end;
//
// procedure ClassFS.processFilesInDir ( action: TMyAction; const dir: string; insert: TInsert; up: TUpdateProgress ) ;
// var
// SR: TSearchRec;
// folder: string;
// f: TMyFile;
// Size: TULargeInteger;
// begin
//
// if FindFirst ( IncludeTrailingPathDelimiter ( dir ) + '*.*', faAnyFile or faDirectory, SR ) = 0 then
// begin
// try
// folder := extractFolder ( ExcludeTrailingPathDelimiter ( dir ) ) ;
// repeat
//
// if cancelrequest then
// begin
// sysutils.FindClose ( SR ) ;
// exit;
// end;
//
// if ( SR.Attr and faDirectory ) = 0 then
// begin
// inc ( filesCount ) ;
// up ( 4, dir + sr.Name, filesCount ) ;
//
// Size.LowPart := sr.FindData.nFileSizeLow;
// Size.HighPart := sr.FindData.nFileSizeHigh;
//
// f.db.Computer.ID := action.computer.ID;
// f.db.Drives.ID := action.drive.id;
//
// f.db.files.filename := sr.Name;
//
// f.db.files.ext := extractFileExt ( sr.Name ) ;
// f.db.extensions.ext := f.db.files.ext;
//
// f.db.folders.folder := folder;
// f.db.folders.path := IncludeTrailingPathDelimiter ( dir ) ;
// f.db.drives.caption := action.drive.caption;
// f.db.drives.serialNR := action.drive.serialNR;
// f.db.computer.name := action.computer.name;
//
// f.db.files.fsize := Size.QuadPart;
//
// f.db.files.created := FileTimeToDateTime ( SR.FindData.ftCreationTime ) ;
// f.db.files.changed := FileTimeToDateTime ( SR.FindData.ftLastWriteTime ) ;
// f.db.files.accessed := FileTimeToDateTime ( SR.FindData.ftLastAccessTime ) ;
//
// f.db.files.nhash := NAMEMD5 ( action.computer.name + action.drive.serialNR + f.db.folders.path + f.db.files.filename ) ;
// f.db.files.chash := '';
//
// f.attributes.anzahl := 0;
// f.attributes.flag.is_summary := false;
// f.attributes.flag.is_drive := false;
// f.attributes.flag.is_dir := false;
// f.attributes.flag.is_symlink := false;
// f.attributes.flag.is_file := true;
//
// insert ( f ) ;
// end;
// until FindNext ( Sr ) <> 0;
// finally
// SysUtils.FindClose ( SR ) ;
// end;
// end;
//
// end;
//
// function ClassFS.UpdateMyFileFromSearchRec ( var f: TMyFile; SR: TSearchRec ) : boolean;
// begin
// {
// L? C??? ??AD VSHR
//
// L Windows symling
// C Conpressed
// A Archive
// D Directory
// V Volume
// S System
// H Hidden
// R read Only
//
/// /  TSearchRec = record
/// /    Time: Integer;
/// /    Size: Integer;
/// /    Attr: Integer;
/// /    Name: TFileName;
/// /    ExcludeAttr: Integer;
/// /{$IFDEF MSWINDOWS}
/// /    FindHandle: THandle  platform;
/// /    FindData: TWin32FindData  platform;
/// /{$ENDIF}
/// /{$IFDEF LINUX}
/// /    Mode: mode_t  platform;
/// /    FindHandle: Pointer  platform;
/// /    PathOnly: String  platform;
/// /    Pattern: String  platform;
/// /{$ENDIF}
/// /  end;
//
/// /  _WIN32_FIND_DATAA = record
/// /    dwFileAttributes: DWORD;
/// /    ftCreationTime: TFileTime;
/// /    ftLastAccessTime: TFileTime;
/// /    ftLastWriteTime: TFileTime;
/// /    nFileSizeHigh: DWORD;
/// /    nFileSizeLow: DWORD;
/// /    dwReserved0: DWORD;
/// /    dwReserved1: DWORD;
/// /    cFileName: array[0..MAX_PATH - 1] of AnsiChar;
/// /    cAlternateFileName: array[0..13] of AnsiChar;
/// /  end;
/// /
/// /}
//
// end;
//
// function ClassFS.isDriveRoot ( const dir: string ) : boolean;
// begin
// result := ExtractFileDrive ( dir ) = ExcludeTrailingPathDelimiter ( dir ) ;
// end;
//
// function ClassFS.FileTimeToDateTime ( FileTime: TFileTime ) : TDateTime;
// var
// ModifiedTime: TFileTime;
// SystemTime: TSystemTime;
// begin
// try
// FileTimeToLocalFileTime ( FileTime, ModifiedTime ) ;
// FileTimeToSystemTime ( ModifiedTime, SystemTime ) ;
// Result := SystemTimeToDateTime ( SystemTime ) ;
// except
// Result := Now;
// end;
// end;
//
// procedure ClassFS.readRootFolderToVST ( f: TMyFile; CallBack: TVSTAdd ) ;
// var SR: TSearchRec;
// i: integer;
// begin
//
// f.db.files.fsize := -1;
// f.attributes.flag.is_drive := f.db.drives.letter = ExcludeTrailingPathDelimiter ( f.db.folders.path ) ;
// f.attributes.flag.is_dir := true;
// f.attributes.flag.is_symlink := false;
// f.attributes.flag.is_summary := true;
// f.attributes.flag.is_file := false;
// f.attributes.anzahl := 0;
// f.attributes.attr := $00000008;
//
// if not f.attributes.flag.is_drive then
// begin
// if SysUtils.FindFirst ( ExcludeTrailingPathDelimiter ( f.db.folders.path ) , faAnyFile, SR ) = 0 then
// begin
// f.db.files.created := FileTimeToDateTime ( SR.FindData.ftCreationTime ) ;
// f.db.files.changed := FileTimeToDateTime ( SR.FindData.ftLastWriteTime ) ;
// f.db.files.accessed := FileTimeToDateTime ( SR.FindData.ftLastAccessTime ) ;
// end;
// end;
//
// /// callback create node
// CallBack ( f, nil ) ;
//
// end;
//
// function ClassFS.getSizeBySR ( sr: TSearchRec ) : int64;
// var Size: TULargeInteger;
// begin
// Size.LowPart := sr.FindData.nFileSizeLow;
// Size.HighPart := sr.FindData.nFileSizeHigh;
//
// result := size.QuadPart;
// end;
//
// function ClassFS.getNameMD5ofMyFile ( f: TMyFile ) : string;
// begin
// result := NameMD5 ( f.db.computer.name + f.db.drives.serialNR + f.db.folders.path + f.db.files.filename ) ;
// end;
//
// procedure ClassFS.readFilesOfFoldertoVST ( f: TMyFile; CallBack: TVSTAdd ) ;
// var SR: TSearchRec;
// begin
// if FindFirst ( f.db.folders.path + '*.*', faAnyFile, SR ) = 0 then
// try
// repeat
// if cancelrequest then
// begin
// sysutils.FindClose ( SR ) ;
// exit;
// end;
//
// if ( sr.name <> '.' ) and ( sr.name <> '..' ) then
// begin
// f.db.files.filename := sr.Name;
// f.db.files.ext := ExtractFileExt ( SR.Name ) ;
// f.db.extensions.ext := f.db.files.ext;
// f.db.files.fsize := getSizeBySR ( sr ) ;
//
// f.db.files.created := FileTimeToDateTime ( SR.FindData.ftCreationTime ) ;
// f.db.files.changed := FileTimeToDateTime ( SR.FindData.ftLastWriteTime ) ;
// f.db.files.accessed := FileTimeToDateTime ( SR.FindData.ftLastAccessTime ) ;
//
// f.attributes.attr := sr.Attr;                                   //.dwFileAttributes;
// f.db.files.nhash := getNameMD5ofMyFile ( f ) ;
// f.db.files.chash := '';
//
// f.attributes.anzahl := 0;
// f.attributes.flag.is_summary := false;
// f.attributes.flag.is_drive := false;
//
// f.attributes.flag.is_symlink := ( sr.Attr and 8192 ) = 8192;
// f.attributes.flag.is_dir := ( sr.Attr and 16 ) = 16;
// f.attributes.flag.is_file := ( sr.Attr and 16 ) = 0;
//
// /// callback create node
// CallBack ( f, nil ) ;
// end;
//
// until FindNext ( Sr ) <> 0;
//
// finally
// sysutils.FindClose ( SR ) ;
// end;
//
// end;
//
/// /////////////////////////////
/// / Loads the folder to the VST
/// /////////////////////////////
//
// procedure ClassFS.readFolderToVST ( f: TMyFile; CallBack: TVSTAdd ) ;
// begin
//
// /// creates the Parent Node
// readRootFolderToVST ( f, CallBack ) ;
//
// /// read all files to the vst
// readFilesOfFoldertoVST ( f, CallBack ) ;
// end;
//
/// /////////////////////////////
/// / Windows Execute given URI
/// /////////////////////////////
//
// procedure ClassFS.showInExplorer ( pf: PTreeEntry ) ;
// begin
// ShellExecute ( application.Handle,
// nil,
// PChar ( 'explorer' ) ,
// PChar ( '/e,"' + pf.db.folders.path + pf.db.files.filename + '",/select,"' + pf.db.folders.path + pf.db.files.filename + '"' ) ,
// nil,
// SW_SHOW )
// end;
//
// procedure ClassFS.execute ( const uri: string ) ;
// begin
// ShellExecute ( Application.Handle,
// 'open',
// PChar ( uri ) ,
// nil, nil, SW_NORMAL )
// end;
//
// procedure ClassFS.hashFile ( var pf: PTreeEntry; ih: TInsertHash ) ;
// var f: TMyFile;
// begin
//
// /// hash only files
// if pf.attributes.flag.is_file then
// begin
// f.db := pf.db;
// f.attributes := pf.attributes;
//
// f := getHashAndTimeOfFile ( f ) ;
//
// if ih ( f ) then
// begin
// pf^.db.files.ishashed := 1;
// pf^.db.files.updated := now;
// end;
//
// end;
// end;
//
/// /////////////////////////////
/// / requires
/// /  f.uri
/// /
/// /////////////////////////////
//
// function ClassFS.getHashAndTimeOfFile ( f: TMyFile ) : TMyFile;
// var SR: TSearchRec;
// begin
// //
// result.db.Files.ID := f.db.Files.ID;
// if fileexists ( f.db.folders.path + f.db.files.filename ) then
// begin
//
// if cancelrequest then
// begin
// sysutils.FindClose ( SR ) ;
// exit;
// end;
//
// try
// result.db.files.chash := FILEMD5 ( f.db.folders.path + f.db.files.filename ) ;
// except
// /// fehlermessage
// end;
// if FindFirst ( f.db.folders.path + f.db.files.filename, faAnyFile, SR ) = 0 then
// try
// result.db.files.created := FileTimeToDateTime ( SR.FindData.ftCreationTime ) ;
// result.db.files.changed := FileTimeToDateTime ( SR.FindData.ftLastWriteTime ) ;
// result.db.files.accessed := FileTimeToDateTime ( SR.FindData.ftLastAccessTime ) ;
// finally
// sysutils.FindClose ( SR ) ;
// end;
//
// end;
// end;
//
/// /http://www.swissdelphicenter.ch/en/showcode.php?id=31
//
// function ClassFS.DeleteFileWithUndo ( sFileName: string ) : Boolean;
// var
// fos: TSHFileOpStruct;
// begin
// FillChar ( fos, SizeOf ( fos ) , 0 ) ;
// with fos do
// begin
// wFunc := FO_DELETE;
// pFrom := PChar ( sFileName ) ;
// fFlags := FOF_ALLOWUNDO or FOF_NOCONFIRMATION or FOF_SILENT;
// end;
// Result := ( 0 = ShFileOperation ( fos ) ) ;
// end;

end.
