unit Unit_Whitelist;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
    TForm5 = class(TForm)
        GroupBox1: TGroupBox;
        Label1: TLabel;
        Label3: TLabel;
        Edit1: TEdit;
        Edit2: TEdit;
        Edit3: TEdit;
        Button1: TButton;
        Label4: TLabel;
        Button2: TButton;
        procedure Button1Click(Sender: TObject);
        procedure Button2Click(Sender: TObject);
        procedure FormShow(Sender: TObject);
    private
        { Private-Deklarationen }
    public
        { Public-Deklarationen }
        addNewEntry: boolean;
        ListIndex: integer;
        procedure SetFileParams(const i: integer; const e1, e2, e3: string);
    end;

var
    Form5: TForm5;

implementation

{$R *.dfm}

uses Unit_Einstellungen;

procedure TForm5.SetFileParams(const i: integer; const e1: string; const e2: string; const e3: string);
begin
    ListIndex := i;
    Edit1.Text := e1;
    Edit2.Text := e2;
    Edit3.Text := e3;
end;

procedure TForm5.Button1Click(Sender: TObject);
begin
    if addNewEntry then
    begin
        form4.addListItem(ListIndex, Edit1.Text, Edit2.Text, Edit3.Text);
    end else begin
        form4.updateListItem(ListIndex, Edit1.Text, Edit2.Text, Edit3.Text);
    end;

    close;
end;

procedure TForm5.Button2Click(Sender: TObject);
begin
    close;
end;

procedure TForm5.FormShow(Sender: TObject);
begin
    Edit1.SetFocus;
end;

end.
