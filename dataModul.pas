﻿unit dataModul;

interface

uses
    System.SysUtils, System.Classes, System.IOUtils, registry_connection, Data.DB,
    IBX.IBCustomDataSet, IBX.IBQuery, IBX.IBDatabase, ClassFilter, classFilesystem, ShellAPI,
    Windows, u_DM_type_def, classLogging, vcl.forms,
    ThreadGetFileIDs,
    ThreadGetFolderIDs,
    ThreadReadExif,
    ThreadHashFiles,
    ThreadFindDoublette,
    VirtualTrees, vcl.Samples.Gauges,
    dialogs, DateUtils, classVSTHandling, ClassDBSet, classComputerRead, classDriveRead, classFolderRead;

type
    Tdm = class(TDataModule)
        DB: TIBDatabase;
        t: TIBTransaction;
        q: TIBQuery;
        q2: TIBQuery;
        procedure DataModuleDestroy(Sender: TObject);
        procedure DataModuleCreate(Sender: TObject);
    private
        DBPfad: string;
        ExtensionList: TStringlist;
        ExtensionListID: array of integer;

        ThreadGetFileIDs: TThreadGetFileIDs;
        ThreadGetFolderIDs: TThreadGetFolderIDs;
        ThreadGetHash: TThreadHashFiles;
        ThreadGetDoubletten: TThreadFindDoublette;
        ThreadGetExif: TThreadReadExif;

        running: boolean;

        procedure createThreadGetFileIDs;
        procedure createThreadGetFolderIDs;
        procedure createThreadHashFiles;
        procedure createThreadGetDoubletten;
        procedure createThreadGetExif;

        procedure ThreadAddTFile(f: TDatei);
        procedure ThreadAddTFileToHash(f: TDatei);
        procedure ThreadAddTFolder(f: TThreadFolder);
        procedure ThreadAddFindDoubletten(f: TDatei);
        procedure ThreadAddReadExif(f: TDatei);

        procedure ThreadResponseGetFileID(const Response: TDatei);
        procedure ThreadResponseGetFolderID(const Response: TThreadFolder);
        Procedure ThreadResponseGetHash(const Response: TDatei);
        procedure ThreadResponseGetDoubletten(const Response: TDateien);
        procedure ThreadResponseGetExif(const Response: TDatei);

    public
        cfg: TClassFilter;
        logging: TClassLogging;
        VSTHandling: TClassVSTHandling;

        procedure restartAllThreads;
        procedure terminateAllThreads;
        procedure setRunning(const b: boolean);
        function isRunning: boolean;

        procedure terminateThreadGetFileIDs;
        procedure terminateThreadGetFolderIDs;
        procedure terminateThreadHashFiles;
        procedure terminateThreadGetDoubletten;
        procedure terminateThreadGetExif;

        function loadDrives(const ParentNode: PVirtualNode; addDriveCB: TCBaddDrive): integer;
        function loadFolders(const ParentNode: PVirtualNode; Callback_AddFolder: TCBaddFolder;
            Callback_addFile: TCBaddDatei): integer; stdcall;

        function loadComputer(addComputerCallback: TCBAddComputer): integer;
        function loadFolderRecursive(DestVST: TVirtualStringTree; const folder: TFolder): integer;

        function speichereDBPfad(pfad: string): boolean;
        function leseDBPfad: string;
        function GetDefaultSystemIcon(ASiid: integer): integer;
        function getPathIconID(const path: string): integer;
        function getFileIconID(const ext: string): integer;

        procedure reloadDatabase;
        function isVSTFillUp: boolean;

        function DBCountFolders: string;
        function DBCountFiles: string;
        function DBCountHahses: string;
        function DBCountFileSize: string;
        function GetVSTItem(DestVST: TBaseVirtualTree; Node: PVirtualNode): TclassDBSet;
    end;

var
    dm: Tdm;

implementation

{$R *.dfm}

uses main, Unit_logging;

{ *************************************************
  *
  * STARTUP FINISH
  *
  *
  *
  ************************************************* }
procedure Tdm.DataModuleCreate(Sender: TObject);
begin
    DBPfad := '';
    logging := TClassLogging.Create;
    cfg := TClassFilter.Create;
    ExtensionList := TStringlist.Create;
    setlength(ExtensionListID, 0);
end;

procedure Tdm.DataModuleDestroy(Sender: TObject);
begin
    terminateAllThreads;
    DBPfad := '';
    logging.Free;
    cfg.Free;

    ExtensionList.Free;

    setlength(ExtensionListID, 0);
end;

{ *************************************************
  *
  * SHORTINGS FOR REGISTRY INTERACTION
  *
  *
  *
  ************************************************* }
function Tdm.speichereDBPfad(pfad: string): boolean;
begin
    Result := cfg.WriteToRegistry('Datenbank', pfad);

    if Result then
        DBPfad := pfad;
end;

function Tdm.leseDBPfad: string;
begin
    if DBPfad = '' then
        DBPfad := cfg.ReadFromRegistry('Datenbank', '');

    Result := DBPfad;
end;

{ *************************************
  *  GET ICONS
  *
  *
  *
  *
  ************************************ }
function Tdm.GetDefaultSystemIcon(ASiid: integer): integer;
var
    sInfo: TSHStockIconInfo;
begin
    sInfo.cbSize := sizeof(TSHStockIconInfo);
    if S_OK = SHGetStockIconInfo(ASiid, SHGSI_SYSICONINDEX, sInfo) then
        Result := sInfo.iSysImageIndex
    else
        Result := -1;
end;

function Tdm.getPathIconID(const path: string): integer;
var
    fi: TSHFileInfo;
begin
    Result := -1;
    try
        ZeroMemory(@fi, sizeof(fi));
        SHGetFileInfo(PChar(path), 0, fi, sizeof(fi), SHGFI_ICON or SHGFI_SYSICONINDEX or SHGFI_TYPENAME);

        Result := fi.iIcon;

        if fi.iIcon <> 0 then
            DestroyIcon(fi.hIcon);
    except

    end;
end;

function Tdm.getFileIconID(const ext: string): integer;
var
    index: integer;
    fi: TSHFileInfo;
begin
    Result := -1;

    index := ExtensionList.IndexOf(ext);

    if (index > -1) then
    begin
        Result := ExtensionListID[index];
    end else begin
        if SHGetFileInfo(PChar(ext), FILE_ATTRIBUTE_NORMAL, fi, sizeof(fi), SHGFI_SYSICONINDEX or SHGFI_SMALLICON or
                SHGFI_USEFILEATTRIBUTES) <> 0 then
        begin
            index := fi.iIcon;

            if fi.iIcon <> 0 then
                DestroyIcon(fi.hIcon);

            ExtensionList.add(ext);
            setlength(ExtensionListID, ExtensionList.Count);

            ExtensionListID[ExtensionList.Count - 1] := index;

            Result := index;
        end;
    end;
end;

{ *************************************************
  *
  * MAIN DATABASE ACTIONS
  *
  *
  *
  ************************************************* }
procedure Tdm.reloadDatabase;
begin
    //
    DB.Connected := false;
    DB.LoginPrompt := false;

    DB.close;
    DB.Params.Clear;
    DB.Params.add('user_name=SYSDBA');
    DB.Params.add('password=masterkey');
    DB.Params.add('lc_ctype=UTF8');
    DB.DatabaseName := leseDBPfad;
    DB.SQLDialect := 3;

    try
        try
            DB.Open;

        except
            on E: Exception do
            begin
                showmessage(E.Message);
            end;
        end;
    finally

    end;
end;

{ *************************************************
  *
  * Threads
  *
  *
  *
  ************************************************* }

procedure Tdm.restartAllThreads;
begin
    terminateThreadGetFileIDs;
    createThreadGetFileIDs;

    terminateThreadGetFolderIDs;
    createThreadGetFolderIDs;

    terminateThreadHashFiles;
    createThreadHashFiles;

    terminateThreadGetDoubletten;
    createThreadGetDoubletten;

    terminateThreadGetExif;
    createThreadGetExif;
end;

procedure Tdm.terminateAllThreads;
begin
    terminateThreadGetFileIDs;
    terminateThreadGetFolderIDs;
    terminateThreadHashFiles;
    terminateThreadGetDoubletten;
    terminateThreadGetExif;
end;

procedure Tdm.createThreadGetFileIDs;
begin
    ThreadGetFileIDs := TThreadGetFileIDs.Create(dm.leseDBPfad);
    ThreadGetFileIDs.OnResponse := ThreadResponseGetFileID;
end;

procedure Tdm.createThreadGetFolderIDs;
begin
    ThreadGetFolderIDs := TThreadGetFolderIDs.Create(dm.leseDBPfad);
    ThreadGetFolderIDs.OnResponse := ThreadResponseGetFolderID;
end;

procedure Tdm.createThreadHashFiles;
begin
    ThreadGetHash := TThreadHashFiles.Create(dm.leseDBPfad);
    ThreadGetHash.OnResponse := ThreadResponseGetHash;
end;

procedure Tdm.createThreadGetDoubletten;
begin
    ThreadGetDoubletten := TThreadFindDoublette.Create(dm.leseDBPfad);
    ThreadGetDoubletten.OnResponse := ThreadResponseGetDoubletten;
end;

procedure Tdm.createThreadGetExif;
begin
    ThreadGetExif := TThreadReadExif.Create(dm.leseDBPfad);
    ThreadGetExif.OnResponse := ThreadResponseGetExif;
end;

procedure Tdm.terminateThreadGetFileIDs;
begin
    if assigned(ThreadGetFileIDs) then
    begin
        ThreadGetFileIDs.Terminate;
        sleep(50);

        if assigned(ThreadGetFileIDs) then
        begin
            FreeAndNil(ThreadGetFileIDs);
        end;
    end;
end;

procedure Tdm.terminateThreadGetFolderIDs;
Begin
    if assigned(ThreadGetFolderIDs) then
    begin
        ThreadGetFolderIDs.Terminate;
        sleep(50);

        if assigned(ThreadGetFolderIDs) then
        begin
            FreeAndNil(ThreadGetFolderIDs);
        end;
    end;
End;

procedure Tdm.terminateThreadHashFiles;
begin
    if assigned(ThreadGetHash) then
    begin
        ThreadGetHash.Terminate;
        sleep(250);

        if assigned(ThreadGetHash) then
        begin
            FreeAndNil(ThreadGetHash);
        end;
    end;
end;

procedure Tdm.terminateThreadGetDoubletten;
begin
    if assigned(ThreadGetDoubletten) then
    begin
        ThreadGetDoubletten.Terminate;
        sleep(50);

        if assigned(ThreadGetDoubletten) then
        begin
            FreeAndNil(ThreadGetDoubletten);
        end;
    end;
end;

procedure Tdm.terminateThreadGetExif;
begin
    if assigned(ThreadGetExif) then
    begin
        ThreadGetExif.Terminate;
        sleep(50);

        if assigned(ThreadGetExif) then
        begin
            FreeAndNil(ThreadGetExif);
        end;
    end;
end;

procedure Tdm.ThreadAddTFile(f: TDatei);
begin
    if dm.cfg.isAddFile(f) then
    begin
        ThreadGetFileIDs.add(f);
    end;
end;

procedure Tdm.ThreadAddTFolder(f: TThreadFolder);
begin
    if dm.cfg.isAddFolder(f.folder) then
    begin
        ThreadGetFolderIDs.add(f);
    end;
end;

procedure Tdm.ThreadAddTFileToHash(f: TDatei);
begin
    if dm.cfg.isAddFileToHash(f) then
    begin
        ThreadGetHash.add(f);
    end;
end;

procedure Tdm.ThreadAddFindDoubletten(f: TDatei);
begin
    if dm.cfg.isAddFindDoubletten(f) then
    begin
        ThreadGetDoubletten.add(f);
    end;
end;

procedure Tdm.ThreadAddReadExif(f: TDatei);
begin
    if dm.cfg.isAddReadExif(f) then
    begin
        ThreadGetExif.add(f);
    end;
end;

procedure Tdm.ThreadResponseGetFolderID(const Response: TThreadFolder);
begin
    // GetVSTItem(form1.VFolder, Response.Nodes.VFolder).setFolderID(Response.dbID.FolderID);
    // GetVSTItem(form1.VFiles, Response.Nodes.VFiles).setFolderID(Response.dbID.FolderID);

    // VSTHandling.setFolderID(form1.VFolder, Response.Nodes.VFolder, Response.dbID.FolderID);
    // VSTHandling.setFolderID(form1.VFiles, Response.Nodes.VFiles, Response.dbID.FolderID);
end;

/// //////////////////////////////////////////////////////////////////////
/// / Receiving the TFile Response from the Thread
/// / Changed is the FileID
/// / Finding the Node and add the ID to the Dataset
/// //////////////////////////////////////////////////////////////////////
procedure Tdm.ThreadResponseGetFileID(const Response: TDatei);
begin
    // form1.GaugeFiles.Progress := form1.GaugeFiles.Progress + 1;
    // form1.Label6.caption := 'Indexing: ' + form1.GaugeFiles.Progress.ToString + ' / ' +
    // form1.GaugeFiles.MaxValue.ToString;
    //
    // GetVSTItem(form1.VFiles, Response.Nodes.VFiles).setHash(Response.contentHash);
    //
    // // TODO second check if node in VFiles exists
    // if (Response.Nodes.VFiles <> nil) then
    // begin
    // VSTHandling.setDBLookupResponse(form1.VFiles, Response.Nodes.VFiles, Response);
    //
    // if Response.contentHash = '' then
    // begin
    // ThreadAddTFileToHash(Response);
    // end else begin
    // ThreadAddFindDoubletten(Response);
    // end;
    //
    // ThreadAddReadExif(Response);
    // end;
end;

/// //////////////////////////////////////////////////////////////////////////
/// / Receiving the Hash Response from the Thread
/// / Changed is the contenthash
/// / Finding the Node and add the hash to the Dataset
/// //////////////////////////////////////////////////////////////////////////
Procedure Tdm.ThreadResponseGetHash(const Response: TDatei);
begin
    // form1.Gauge1.Progress := form1.Gauge1.Progress + 1;
    // form1.Label7.caption := 'Hashing: ' + form1.Gauge1.Progress.ToString + ' / ' + form1.Gauge1.MaxValue.ToString;

    // if GetVSTItem(form1.VFiles, Response.Nodes.VFiles).setHash(Response.contentHash) then
    // // if VSTHandling.setHash(form1.VFiles, Response.Nodes.VFiles, Response.contentHash) then
    // begin
    // ThreadAddFindDoubletten(Response);
    // end;
end;

procedure Tdm.ThreadResponseGetDoubletten(const Response: TDateien);
var
    i: integer;
begin
    // for i := Low(Response) to High(Response) do
    // begin
    // if (Response[i].Nodes.VFiles <> nil) then
    // begin
    // dm.VSTHandling.addFile(form1.VFiles, Response[i].Nodes.VFiles, Response[i]);
    // end;
    // end;
end;

procedure Tdm.ThreadResponseGetExif(const Response: TDatei);
begin
    // if (Response.Nodes.VFiles <> nil) then
    // begin
    // VSTHandling.setMeta(form1.VFiles, Response.Nodes.VFiles, Response.Meta)
    // end;
end;

function Tdm.isVSTFillUp: boolean;
begin
    Result := (assigned(ThreadGetFileIDs) and (ThreadGetFileIDs.openTask > 0));
end;

{ *************************************************
  *
  * MAIN ACTIONS
  *
  *
  *
  ************************************************* }

function Tdm.loadComputer(addComputerCallback: TCBAddComputer): integer;
/// ////////////////////////////////////////////////////////////////////////////
/// LOADS THE COMPUTERS
/// ////////////////////////////////////////////////////////////////////////////
var
    c: TclassComputerRead;
begin
    c := TclassComputerRead.Create(leseDBPfad, addComputerCallback);
    c.readLocalComputer(true);
    c.addDBComputer(true);

    form8.addQuery('Load Computer', c.getLastQueries);

    c.Destroy;
end;

function Tdm.loadDrives(const ParentNode: PVirtualNode; addDriveCB: TCBaddDrive): integer;
/// ////////////////////////////////////////////////////////////////////////////
/// LOADS THE DRIVES
/// ////////////////////////////////////////////////////////////////////////////
var
    d: TclassDriveRead;
begin
    // Info Block
    form1.showActivity('Lese Laufwerke');
    form1.showFolderInfo('');

    // Init
    d := TclassDriveRead.Create(leseDBPfad, GetVSTItem(form1.VFolder, ParentNode).getParentSet, ParentNode);

    // Callback
    d.setAddDriveCallback(addDriveCB);
    d.useCallback(cfg.options.UseCallBacks.Drive);

    // Preload
    d.useDBPreload(cfg.options.Preload.Drive);

    // ACTION
    d.readLocalDrives;
    d.addDBDrives;

    // Info Block
    form8.addQuery('Load Drive', d.getLastQueries);

    d.Destroy;
end;

function Tdm.loadFolders(const ParentNode: PVirtualNode; Callback_AddFolder: TCBaddFolder;
    Callback_addFile: TCBaddDatei): integer; stdcall;
/// ////////////////////////////////////////////////////////////////////////////
/// LOADS THE FOLDERS AND FILES
/// ////////////////////////////////////////////////////////////////////////////
var
    f: TClassFolderRead;
    t1, t2: cardinal;
    item: TclassDBSet;
begin
    item := GetVSTItem(form1.VFolder, ParentNode);

    // Info Block
    t1 := GetTickCount;
    form1.StatusBar1.Panels[3].Text := item.FullPath;
    form1.StatusBar1.Panels[2].Text := 'Ordner: -, Files: -, Zeit: -ms';
    application.ProcessMessages;

    // Init
    f := TClassFolderRead.Create(leseDBPfad, item.getParentSet, ParentNode);

    // Callback
    f.setCallbacks(Callback_AddFolder, Callback_addFile);

    // Options
    f.setPreloadOptions(cfg.options.Preload);
    f.setCallbackOptions(cfg.options.UseCallBacks);
    f.setImportOptions(cfg.options.LiveImport);
    
    // ACTION
    f.readLocalFolder;

    form8.addQuery('Load Folder', f.getLastQueries);
    t2 := GetTickCount;

    form1.StatusBar1.Panels[2].Text := 'Ordner: ' + inttostr(f.FolderCount) + ', Files: ' + inttostr(f.FileCount) +
        ', Zeit: ' + inttostr(t2 - t1) + ' ms';

    f.Destroy;
end;

function Tdm.loadFolderRecursive(DestVST: TVirtualStringTree; const folder: TFolder): integer;
var
    tsr: System.SysUtils.TSearchRec;
    subfolder: TFolder;
    f, f2: TFile;
    maxFileSize: int64;
begin
    Result := 0;

end;

function String2Hex(const Buffer: AnsiString): string;
begin
    setlength(Result, length(Buffer) * 2);
    BinToHex(PAnsiChar(Buffer), PChar(Result), length(Buffer));
end;

procedure Tdm.setRunning(const b: boolean);
begin
    running := b;
end;

function Tdm.isRunning: boolean;
begin
    Result := running;
end;

function Tdm.DBCountFolders: string;
begin
    Result := '0';

    if DB.Connected then
    begin
        q.close;
        q.SQL.Text := 'SELECT count(1) FROM FOLDERS';

        try
            try
                q.Open;
                if not q.Eof then
                    Result := formatfloat('#,0', q.Fields[0].AsInteger);
            except
                on E: Exception do
                begin
                    showmessage(E.Message);
                end;
            end;
        finally
            q.close;
            q.SQL.Clear;
        end;
    end;
end;

function Tdm.DBCountFiles: string;
begin
    Result := '0';

    if DB.Connected then
    begin
        q.close;
        q.SQL.Text := 'SELECT count(1) FROM FILES';

        try
            try
                q.Open;
                if not q.Eof then
                    Result := formatfloat('#,0', q.Fields[0].AsInteger);
            except
                on E: Exception do
                begin
                    showmessage(E.Message);
                end;
            end;
        finally
            q.close;
            q.SQL.Clear;
        end;
    end;
end;

function Tdm.DBCountHahses: string;
begin
    Result := '0';
    if DB.Connected then
    begin
        q.close;
        q.SQL.Text := 'SELECT count(1) FROM FILES WHERE CONTENTHASH is not null;';

        try
            try
                q.Open;
                if not q.Eof then
                    Result := formatfloat('#,0', q.Fields[0].AsInteger);
            except
                on E: Exception do
                begin
                    showmessage(E.Message);
                end;
            end;
        finally
            q.close;
            q.SQL.Clear;
        end;
    end;
end;

function Tdm.DBCountFileSize: string;
begin
    Result := '0';
    if DB.Connected then
    begin
        q.close;
        q.SQL.Text := 'SELECT SUM(FSIZE) FROM FILES';

        try
            try
                q.Open;
                if not q.Eof then
                    Result := formatfloat('#,0', q.Fields[0].AsLargeInt);
            except
                on E: Exception do
                begin
                    showmessage(E.Message);
                end;
            end;
        finally
            q.close;
            q.SQL.Clear;
        end;
    end;
end;

function Tdm.GetVSTItem(DestVST: TBaseVirtualTree; Node: PVirtualNode): TclassDBSet;
begin
    Result := VSTHandling.getItem(DestVST, Node);
end;

end.
