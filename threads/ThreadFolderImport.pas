unit ThreadFolderImport;

interface

uses
    System.Generics.Collections,
    System.SyncObjs,
    System.Classes,
    u_DM_type_def,
    System.SysUtils;

type

    TResponseNotify = procedure(const f: TFolder) of object;

    TThreadFolderImport = class(TThread)
    private
        FCS: TCriticalSection;
        FEvent: TEvent;
        FOnResponse: TResponseNotify;
        FFolder: string;
//        FDBID: TIDSet;
        procedure SetOnResponse(const Value: TResponseNotify);
        function GetOnResponse: TResponseNotify;
        procedure DoResponseNotify(const f: TFolder);

    protected
        procedure Execute; override;

    public
        constructor Create;
        destructor Destroy; override;

        procedure setFolder(const folder: string);//; const DBID: TIDSet);
        property OnResponse: TResponseNotify read GetOnResponse write SetOnResponse;
    end;

implementation

constructor TThreadFolderImport.Create;
begin
    inherited Create(true);
    FCS := TCriticalSection.Create;
    FEvent := TEvent.Create(nil, false, false, 'jojo');
    FFolder := '';
end;

destructor TThreadFolderImport.Destroy;
begin

    FreeAndNil(FEvent);
    FreeAndNil(FCS);

    FFolder := '';

    inherited Destroy;
end;

procedure TThreadFolderImport.setFolder(const folder: string);// const DBID: TIDSet);
begin
    FCS.Enter;
    try
        FFolder := IncludeTrailingPathDelimiter(folder);
//        FDBID := DBID;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadFolderImport.DoResponseNotify(const f: TFolder);
begin
    FCS.Enter;

    try
        if MainThreadID = CurrentThread.ThreadID then
        begin
            if assigned(OnResponse) then
                OnResponse(f);
        end
    finally
        FCS.Leave;
    end;

end;

procedure TThreadFolderImport.Execute;
var
    f: TFolder;
    tsr: System.SysUtils.TSearchRec;

begin
    try
        if (System.SysUtils.directoryexists(FFolder)) then
        begin
            if FindFirst(FFolder + '*.*', faAnyFile or faDirectory, tsr) = 0 then
            begin
                repeat
                    if (tsr.Attr and faDirectory) <> 0 then
                    begin
//                        f.DBID.ComputerID := FDBID.ComputerID;
//                        f.DBID.DriveID := FDBID.DriveID;
//                        f.DBID.FolderID := 0;
//                        f.DBID.FileID := 0;
//
//                        f.DBID.kind := kFolder;
//
//                        f.pfad := FFolder + tsr.name;
//                        f.name := tsr.name;
//
//                        f.parentFolderID := FDBID.FolderID;

                        (DoResponseNotify(f));
                    end;
                until (FindNext(tsr) <> 0) or (Terminated);

                System.SysUtils.FindClose(tsr);
            end;
        end;
        FFolder := '';

    except
        on E: Exception do
        begin

        end;
    end;
end;

function TThreadFolderImport.GetOnResponse: TResponseNotify;
begin
    FCS.Enter;
    try
        Result := FOnResponse;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadFolderImport.SetOnResponse(const Value: TResponseNotify);
begin
    FCS.Enter;
    try
        FOnResponse := Value;
    finally
        FCS.Leave;
    end;
end;

end.
