﻿unit ThreadGetFileIDs;

interface

uses
    System.Generics.Collections,
    System.SyncObjs,
    System.Classes,
    u_DM_type_def,
    ClassVirtuelDBFile,
    System.SysUtils;

type

    TResponseNotify = procedure(const f: TDatei) of object;

    TThreadGetFileIDs = class(TThread)
    private
        FCS: TCriticalSection;
        FEvent: TEvent;
        FQueue: TQueue<TDatei>;
        FOnResponse: TResponseNotify;
        FisCancelEvent: Boolean;
        DB: TClassVirtuelDBFile;
        procedure SetOnResponse(const Value: TResponseNotify);
        function GetOnResponse: TResponseNotify;
        function GetQueueItem: TDatei;
        procedure ProcessQueueItem;
        procedure DoResponseNotify(const f: TDatei);
        procedure TruncateQueue;
        function getOpenTasks: integer;

    protected
        procedure Execute; override;
        procedure TerminatedSet; override;

    public
        constructor Create(const dbName: string);
        destructor Destroy; override;

        procedure Add(const f: TDatei);
        procedure Clear;

        property OnResponse: TResponseNotify read GetOnResponse write SetOnResponse;
        property openTask: integer read getOpenTasks;
    end;

implementation

procedure TThreadGetFileIDs.Add(const f: TDatei);
begin
    FCS.Enter;
    try
        FQueue.Enqueue(f);
        FisCancelEvent := false;
        FEvent.SetEvent;
    finally
        FCS.Leave;
    end;
end;

function TThreadGetFileIDs.GetQueueItem: TDatei;
begin
    FCS.Enter;
    try
        result := FQueue.Dequeue;
        if FQueue.Count > 0 then
            FEvent.SetEvent;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadGetFileIDs.TruncateQueue;
begin
    FCS.Enter;
    try
        FQueue.Clear;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadGetFileIDs.Clear;
begin
    FCS.Enter;
    try
        FisCancelEvent := true;
        FEvent.SetEvent;
    finally
        FCS.Leave;
    end;
end;

constructor TThreadGetFileIDs.Create(const dbName: string);
begin
    inherited Create(false);
    FCS := TCriticalSection.Create;
    FEvent := TEvent.Create(nil, false, false, '');
    FQueue := TQueue<TDatei>.Create;
    DB := TClassVirtuelDBFile.Create(dbName);
end;

destructor TThreadGetFileIDs.Destroy;
begin

    FreeAndNil(FQueue);
    FreeAndNil(FEvent);
    FreeAndNil(FCS);

    DB.Free;

    inherited Destroy;
end;

procedure TThreadGetFileIDs.TerminatedSet;
begin
    inherited;
    FEvent.SetEvent;
end;

procedure TThreadGetFileIDs.DoResponseNotify(const f: TDatei);
begin
    if MainThreadID = CurrentThread.ThreadID then
    begin
        if assigned(OnResponse) then
            OnResponse(f);
    end
    else
        Queue(
                procedure
            begin
                DoResponseNotify(f);
            end);
end;

procedure TThreadGetFileIDs.Execute;
begin
    inherited;
    while not Terminated do
    begin
        FEvent.WaitFor;
        if not Terminated then
            ProcessQueueItem;
    end;
end;

procedure TThreadGetFileIDs.ProcessQueueItem;
var
    RequestFile: TDatei;
    RespondFile: TDatei;
begin
    try
        if FisCancelEvent then
        begin
            TruncateQueue;
        end else begin
            RequestFile := GetQueueItem;
            RespondFile := RequestFile;

//            RespondFile := DB.getFileDBContent(RequestFile, true);
//TODO: respondfile einbauen

            DoResponseNotify(RespondFile);

            RequestFile.Database.Filename := '';
            RespondFile.Database.Filename := '';
        end;
    finally
    end;
end;

function TThreadGetFileIDs.GetOnResponse: TResponseNotify;
begin
    FCS.Enter;
    try
        result := FOnResponse;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadGetFileIDs.SetOnResponse(const Value: TResponseNotify);
begin
    FCS.Enter;
    try
        FOnResponse := Value;
    finally
        FCS.Leave;
    end;
end;

function TThreadGetFileIDs.getOpenTasks: integer;
begin
    FCS.Enter;
    try
        result := FQueue.Count;
    finally
        FCS.Leave;
    end;

end;

end.
