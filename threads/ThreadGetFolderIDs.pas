unit ThreadGetFolderIDs;

interface

uses
    System.Generics.Collections,
    System.SyncObjs,
    System.Classes,
    u_DM_type_def,
    classFolderReadDB,
    System.SysUtils;

type

    TResponseNotify = procedure(const Response: TThreadFolder) of object;

    TThreadGetFolderIDs = class(TThread)
    private
        FCS: TCriticalSection;
        FEvent: TEvent;
        FQueue: TQueue<TThreadFolder>;
        FOnResponse: TResponseNotify;
        FisCancelEvent: Boolean;
        DB: TclassFolderReadDB;
        procedure SetOnResponse(const Value: TResponseNotify);
        function GetOnResponse: TResponseNotify;
        function GetQueueItem: TThreadFolder;
        procedure ProcessQueueItem;
        procedure DoResponseNotify(const AResponse: TThreadFolder);
        procedure TruncateQueue;
        function getOpenTasks: integer;

    protected
        procedure Execute; override;
        procedure TerminatedSet; override;

    public
        constructor Create(const dbName: string);
        destructor Destroy; override;

        procedure Add(const ARequest: TThreadFolder);
        procedure preloadFolder(aDataset: TDataSet);
        procedure Clear;

        property OnResponse: TResponseNotify read GetOnResponse write SetOnResponse;
        property openTask: integer read getOpenTasks;
    end;

implementation

procedure TThreadGetFolderIDs.Add(const ARequest: TThreadFolder);
begin
    FCS.Enter;
    try
        FQueue.Enqueue(ARequest);
        FisCancelEvent := false;
        FEvent.SetEvent;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadGetFolderIDs.Clear;
begin
    FCS.Enter;
    try
        FisCancelEvent := true;
        FEvent.SetEvent;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadGetFolderIDs.preloadFolder(aDataset: TDataSet);
begin
    DB.DataSet := aDataset;
    DB.loadDBFolders;
end;

constructor TThreadGetFolderIDs.Create(const dbName: string);
var DataSet:TDataSet;
begin
    inherited Create(false);
    FCS := TCriticalSection.Create;
    FEvent := TEvent.Create(nil, false, false, '');
    FQueue := TQueue<TThreadFolder>.Create;
    DB := TclassFolderReadDB.Create(dbName, DataSet);
end;

destructor TThreadGetFolderIDs.Destroy;
begin

    FreeAndNil(FQueue);
    FreeAndNil(FEvent);
    FreeAndNil(FCS);

    DB.Free;

    inherited Destroy;
end;

procedure TThreadGetFolderIDs.TerminatedSet;
begin
    inherited;
    FEvent.SetEvent;
end;

procedure TThreadGetFolderIDs.DoResponseNotify(const AResponse: TThreadFolder);
begin
    if MainThreadID = CurrentThread.ThreadID then
    begin
        if assigned(OnResponse) then
            OnResponse(AResponse);
    end
    else
        Queue(
                procedure
            begin
                DoResponseNotify(AResponse);
            end);
end;

procedure TThreadGetFolderIDs.Execute;
begin
    inherited;
    while not Terminated do
    begin
        FEvent.WaitFor;
        if not Terminated then
            ProcessQueueItem;
    end;
end;

function TThreadGetFolderIDs.GetQueueItem: TThreadFolder;
begin
    FCS.Enter;
    try
        Result := FQueue.Dequeue;
        if FQueue.Count > 0 then
            FEvent.SetEvent;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadGetFolderIDs.TruncateQueue;
begin
    FCS.Enter;
    try
        FQueue.Clear;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadGetFolderIDs.ProcessQueueItem;
var
    LRequest: TThreadFolder;
    LResponse: TThreadFolder;
begin
    try
        LRequest := GetQueueItem;
        LResponse := LRequest;

        LResponse.folder.Database.ID := DB.getFolderID(LRequest.folder.Database, true, true,true);

        DoResponseNotify(LResponse);
        LRequest.folder.Database.Path := '';
        LResponse.folder.Database.Path := '';
    finally
    end;
end;

function TThreadGetFolderIDs.GetOnResponse: TResponseNotify;
begin
    FCS.Enter;
    try
        Result := FOnResponse;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadGetFolderIDs.SetOnResponse(const Value: TResponseNotify);
begin
    FCS.Enter;
    try
        FOnResponse := Value;
    finally
        FCS.Leave;
    end;
end;

function TThreadGetFolderIDs.getOpenTasks: integer;
begin
    FCS.Enter;
    try
        Result := FQueue.Count;
    finally
        FCS.Leave;
    end;

end;

end.
