unit ThreadFindDoublette;

interface

uses
    System.Generics.Collections,
    System.SyncObjs,
    System.Classes,
    u_DM_type_def,
    classVirtuelDBFile,
    System.SysUtils;

type

    TResponseNotify = procedure(const Response: TDateien) of object;

    TThreadFindDoublette = class(TThread)
    private
        FCS: TCriticalSection;
        FEvent: TEvent;
        FQueue: TQueue<TDatei>;
        FOnResponse: TResponseNotify;
        FisCancelEvent: Boolean;
        DB: TClassVirtuelDBFile;
        procedure SetOnResponse(const Value: TResponseNotify);
        function GetOnResponse: TResponseNotify;
        procedure DoResponseNotify(const AResponse: TDateien);

        function GetQueueItem: TDatei;
        procedure ProcessQueueItem;
        procedure TruncateQueue;
        function getOpenTasks: integer;

    protected
        procedure Execute; override;
        procedure TerminatedSet; override;

    public
        constructor Create(const dbName: string);
        destructor Destroy; override;

        procedure Add(const ARequest: TDatei);
        procedure Clear;

        property OnResponse: TResponseNotify read GetOnResponse write SetOnResponse;
        property openTask: integer read getOpenTasks;
    end;

implementation

procedure TThreadFindDoublette.Add(const ARequest: TDatei);
begin
    FCS.Enter;
    try
        FQueue.Enqueue(ARequest);
        FisCancelEvent := false;
        FEvent.SetEvent;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadFindDoublette.Clear;
begin
    FCS.Enter;
    try
        FisCancelEvent := true;
        FEvent.SetEvent;
    finally
        FCS.Leave;
    end;
end;

constructor TThreadFindDoublette.Create(const dbName: string);
begin
    inherited Create(false);
    FCS := TCriticalSection.Create;
    FEvent := TEvent.Create(nil, false, false, '');
    FQueue := TQueue<TDatei>.Create;
    DB := TClassVirtuelDBFile.Create(dbName);
end;

destructor TThreadFindDoublette.Destroy;
begin

    FreeAndNil(FQueue);
    FreeAndNil(FEvent);
    FreeAndNil(FCS);

    DB.Free;

    inherited Destroy;
end;

procedure TThreadFindDoublette.TerminatedSet;
begin
    inherited;
    FEvent.SetEvent;
end;

procedure TThreadFindDoublette.DoResponseNotify(const AResponse: TDateien);
begin
    if MainThreadID = CurrentThread.ThreadID then
    begin
        if assigned(OnResponse) then
            OnResponse(AResponse);
    end
    else
        Queue(
                procedure
            begin
                DoResponseNotify(AResponse);
            end);
end;

procedure TThreadFindDoublette.Execute;
begin
    inherited;
    while not Terminated do
    begin
        FEvent.WaitFor;
        if not Terminated then
            ProcessQueueItem;
    end;
end;

function TThreadFindDoublette.GetQueueItem: TDatei;
begin
    FCS.Enter;
    try
        Result := FQueue.Dequeue;
        if FQueue.Count > 0 then
            FEvent.SetEvent;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadFindDoublette.TruncateQueue;
begin
    FCS.Enter;
    try
        FQueue.Clear;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadFindDoublette.ProcessQueueItem;
begin
    try
        DoResponseNotify(DB.getDoubletten(GetQueueItem));
    finally
    end;
end;

function TThreadFindDoublette.GetOnResponse: TResponseNotify;
begin
    FCS.Enter;
    try
        Result := FOnResponse;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadFindDoublette.SetOnResponse(const Value: TResponseNotify);
begin
    FCS.Enter;
    try
        FOnResponse := Value;
    finally
        FCS.Leave;
    end;
end;

function TThreadFindDoublette.getOpenTasks: integer;
begin
    FCS.Enter;
    try
        Result := FQueue.Count;
    finally
        FCS.Leave;
    end;

end;

end.
