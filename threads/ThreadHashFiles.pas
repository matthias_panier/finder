unit ThreadHashFiles;

interface

uses
    System.Generics.Collections,
    System.SyncObjs,
    System.Classes,
    u_DM_type_def,
    classVirtuelDBHashFile,
    System.SysUtils;

type

    TResponseNotify = procedure(const Response: TDatei) of object;

    TThreadHashFiles = class(TThread)
    private
        FCS: TCriticalSection;
        FEvent: TEvent;
        FQueue: TQueue<TDatei>;
        FOnResponse: TResponseNotify;
        FisCancelEvent: Boolean;
        DB: TclassVirtuelDBHashFile;
        procedure SetOnResponse(const Value: TResponseNotify);
        function GetOnResponse: TResponseNotify;
        procedure DoResponseNotify(const AResponse: TDatei);

        function GetQueueItem: TDatei;
        procedure ProcessQueueItem;
        procedure TruncateQueue;
        function getOpenTasks: integer;

    protected
        procedure Execute; override;
        procedure TerminatedSet; override;

    public
        constructor Create(const dbName: string);
        destructor Destroy; override;

        procedure Add(const ARequest: TDatei);
        procedure Clear;

        property OnResponse: TResponseNotify read GetOnResponse write SetOnResponse;
        property openTask: integer read getOpenTasks;
    end;

implementation

procedure TThreadHashFiles.Add(const ARequest: TDatei);
begin
    FCS.Enter;
    try
        FQueue.Enqueue(ARequest);
        FisCancelEvent := false;
        FEvent.SetEvent;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadHashFiles.Clear;
begin
    FCS.Enter;
    try
        FisCancelEvent := true;
        FEvent.SetEvent;
    finally
        FCS.Leave;
    end;
end;

constructor TThreadHashFiles.Create(const dbName: string);
begin
    inherited Create(false);
    FCS := TCriticalSection.Create;
    FEvent := TEvent.Create(nil, false, false, '');
    FQueue := TQueue<TDatei>.Create;
    DB := TclassVirtuelDBHashFile.Create(dbName);
end;

destructor TThreadHashFiles.Destroy;
begin

    FreeAndNil(FQueue);
    FreeAndNil(FEvent);
    FreeAndNil(FCS);

    DB.Free;

    inherited Destroy;
end;

procedure TThreadHashFiles.TerminatedSet;
begin
    inherited;
    FEvent.SetEvent;
end;

procedure TThreadHashFiles.DoResponseNotify(const AResponse: TDatei);
begin
    if MainThreadID = CurrentThread.ThreadID then
    begin
        if assigned(OnResponse) then
            OnResponse(AResponse);
    end
    else
        Queue(
                procedure
            begin
                DoResponseNotify(AResponse);
            end);
end;

procedure TThreadHashFiles.Execute;
begin
    inherited;
    while not Terminated do
    begin
        FEvent.WaitFor;
        if not Terminated then
            ProcessQueueItem;
    end;
end;

function TThreadHashFiles.GetQueueItem: TDatei;
begin
    FCS.Enter;
    try
        Result := FQueue.Dequeue;
        if FQueue.Count > 0 then
            FEvent.SetEvent;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadHashFiles.TruncateQueue;
begin
    FCS.Enter;
    try
        FQueue.Clear;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadHashFiles.ProcessQueueItem;
var
    f: TDatei;
begin
    try
        f := GetQueueItem;
        f.Database.Contenthash := DB.hashFile(f, 10000000);
        DoResponseNotify(f);
    finally
    end;
end;

function TThreadHashFiles.GetOnResponse: TResponseNotify;
begin
    FCS.Enter;
    try
        Result := FOnResponse;
    finally
        FCS.Leave;
    end;
end;

procedure TThreadHashFiles.SetOnResponse(const Value: TResponseNotify);
begin
    FCS.Enter;
    try
        FOnResponse := Value;
    finally
        FCS.Leave;
    end;
end;

function TThreadHashFiles.getOpenTasks: integer;
begin
    FCS.Enter;
    try
        Result := FQueue.Count;
    finally
        FCS.Leave;
    end;

end;

end.
