object Form4: TForm4
  Left = 0
  Top = 0
  Caption = 'Einstellungen'
  ClientHeight = 653
  ClientWidth = 717
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 330
    Height = 95
    Caption = 'Anzeige'
    TabOrder = 0
    object CheckBox1: TCheckBox
      Left = 24
      Top = 44
      Width = 225
      Height = 24
      Caption = 'Zeige Ordner in Dateiliste'
      TabOrder = 1
    end
    object CheckBox2: TCheckBox
      Left = 24
      Top = 24
      Width = 225
      Height = 24
      Caption = 'Auto Expand Ordner in Ordnerliste'
      TabOrder = 0
    end
    object CheckBox7: TCheckBox
      Left = 24
      Top = 64
      Width = 225
      Height = 24
      Caption = 'Zeige Dupletten in Dateiliste'
      TabOrder = 2
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 102
    Width = 330
    Height = 204
    Caption = 'Live Indexing'
    TabOrder = 1
    object CheckBox3: TCheckBox
      Left = 24
      Top = 24
      Width = 201
      Height = 17
      Caption = 'Auto Index Ordner'
      TabOrder = 0
    end
    object CheckBox4: TCheckBox
      Left = 24
      Top = 44
      Width = 201
      Height = 17
      Caption = 'Auto Index Dateien'
      TabOrder = 1
    end
    object CheckBox5: TCheckBox
      Left = 48
      Top = 88
      Width = 201
      Height = 17
      Caption = 'Auto Hashing von Dateien'
      TabOrder = 3
    end
    object CheckBox6: TCheckBox
      Left = 24
      Top = 154
      Width = 201
      Height = 17
      Caption = 'Auto Lesen der Exif Daten'
      TabOrder = 7
    end
    object CheckBox11: TCheckBox
      Left = 80
      Top = 132
      Width = 97
      Height = 17
      Caption = 'Max File Size'
      TabOrder = 5
    end
    object Edit1: TEdit
      Left = 192
      Top = 130
      Width = 121
      Height = 21
      TabOrder = 6
      Text = 'Edit1'
    end
    object CheckBox22: TCheckBox
      Left = 48
      Top = 66
      Width = 97
      Height = 17
      Caption = 'Use Thread'
      TabOrder = 2
    end
    object CheckBox23: TCheckBox
      Left = 80
      Top = 110
      Width = 97
      Height = 17
      Caption = 'Use Thread'
      TabOrder = 4
    end
    object CheckBox24: TCheckBox
      Left = 48
      Top = 176
      Width = 97
      Height = 17
      Caption = 'Use Thread'
      TabOrder = 8
    end
  end
  object Button1: TButton
    Left = 597
    Top = 528
    Width = 75
    Height = 25
    Caption = 'speichern'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 453
    Top = 528
    Width = 75
    Height = 25
    Caption = 'laden'
    TabOrder = 3
    OnClick = Button2Click
  end
  object GroupBox3: TGroupBox
    Left = 350
    Top = 174
    Width = 330
    Height = 160
    Caption = 'File Whitelist'
    TabOrder = 4
    object SpeedButton4: TSpeedButton
      Left = 248
      Top = 28
      Width = 23
      Height = 22
      Caption = '+'
      OnClick = SpeedButton4Click
    end
    object SpeedButton5: TSpeedButton
      Left = 272
      Top = 28
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = SpeedButton5Click
    end
    object SpeedButton6: TSpeedButton
      Left = 296
      Top = 28
      Width = 23
      Height = 22
      Caption = '-'
      OnClick = SpeedButton6Click
    end
    object CheckBox9: TCheckBox
      Left = 24
      Top = 24
      Width = 160
      Height = 24
      Caption = 'Whitelist anwenden'
      TabOrder = 0
    end
    object ListView1: TListView
      Left = 40
      Top = 50
      Width = 280
      Height = 100
      Columns = <
        item
          Caption = 'Dateimaske'
          Width = 80
        end
        item
          Caption = 'Min File Size'
          Width = 80
        end
        item
          Caption = 'Max File Size'
          Width = 80
        end>
      FlatScrollBars = True
      ReadOnly = True
      RowSelect = True
      TabOrder = 1
      ViewStyle = vsReport
    end
  end
  object GroupBox4: TGroupBox
    Left = 350
    Top = 343
    Width = 330
    Height = 160
    Caption = 'File Blacklist'
    TabOrder = 5
    object SpeedButton7: TSpeedButton
      Left = 248
      Top = 28
      Width = 23
      Height = 22
      Caption = '+'
      OnClick = SpeedButton7Click
    end
    object SpeedButton8: TSpeedButton
      Left = 272
      Top = 28
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = SpeedButton8Click
    end
    object SpeedButton9: TSpeedButton
      Left = 296
      Top = 28
      Width = 23
      Height = 22
      Caption = '-'
      OnClick = SpeedButton9Click
    end
    object CheckBox10: TCheckBox
      Left = 24
      Top = 24
      Width = 160
      Height = 24
      Caption = 'Blacklist anwenden'
      TabOrder = 0
    end
    object ListView2: TListView
      Left = 40
      Top = 50
      Width = 280
      Height = 100
      Columns = <
        item
          Caption = 'Dateimaske'
          Width = 80
        end
        item
          Caption = 'Min File Size'
          Width = 80
        end
        item
          Caption = 'Max File Size'
          Width = 80
        end>
      FlatScrollBars = True
      ReadOnly = True
      RowSelect = True
      TabOrder = 1
      ViewStyle = vsReport
    end
  end
  object GroupBox5: TGroupBox
    Left = 8
    Top = 320
    Width = 330
    Height = 174
    Caption = 'Rekursives Einlesen'
    TabOrder = 6
    object CheckBox12: TCheckBox
      Left = 16
      Top = 24
      Width = 225
      Height = 17
      Caption = 'Benutze Threads'
      TabOrder = 0
    end
    object CheckBox13: TCheckBox
      Left = 16
      Top = 44
      Width = 225
      Height = 17
      Caption = 'Dateien in Ordner darstellen'
      TabOrder = 1
    end
    object CheckBox14: TCheckBox
      Left = 32
      Top = 64
      Width = 257
      Height = 17
      Caption = 'Ordner Ausklappen'
      TabOrder = 2
    end
    object CheckBox15: TCheckBox
      Left = 16
      Top = 84
      Width = 217
      Height = 17
      Caption = 'Ans Ende Scollen'
      TabOrder = 3
    end
    object CheckBox16: TCheckBox
      Left = 16
      Top = 104
      Width = 97
      Height = 17
      Caption = 'Max File Size'
      TabOrder = 4
    end
    object Edit2: TEdit
      Left = 136
      Top = 101
      Width = 121
      Height = 21
      TabOrder = 5
      Text = 'Edit2'
    end
    object CheckBox17: TCheckBox
      Left = 16
      Top = 124
      Width = 145
      Height = 17
      Caption = 'Dateien hashen'
      TabOrder = 6
    end
    object CheckBox18: TCheckBox
      Left = 16
      Top = 144
      Width = 161
      Height = 17
      Caption = 'Exif Daten auslesen'
      TabOrder = 7
    end
  end
  object GroupBox6: TGroupBox
    Left = 350
    Top = 8
    Width = 330
    Height = 160
    Caption = 'Ordner Exclude List'
    TabOrder = 7
    object SpeedButton1: TSpeedButton
      Left = 248
      Top = 28
      Width = 23
      Height = 22
      Caption = '+'
      OnClick = SpeedButton1Click
    end
    object SpeedButton2: TSpeedButton
      Left = 272
      Top = 28
      Width = 23
      Height = 22
      Caption = '...'
      OnClick = SpeedButton2Click
    end
    object SpeedButton3: TSpeedButton
      Left = 296
      Top = 28
      Width = 23
      Height = 22
      Caption = '-'
      OnClick = SpeedButton3Click
    end
    object CheckBox8: TCheckBox
      Left = 24
      Top = 24
      Width = 160
      Height = 17
      Caption = 'Exlude List anwenden'
      TabOrder = 0
    end
    object ListView3: TListView
      Left = 40
      Top = 50
      Width = 280
      Height = 100
      Columns = <
        item
          Caption = 'Ordner'
          Width = 240
        end>
      FlatScrollBars = True
      ReadOnly = True
      RowSelect = True
      TabOrder = 1
      ViewStyle = vsReport
    end
  end
  object GroupBox7: TGroupBox
    Left = 8
    Top = 493
    Width = 330
    Height = 96
    Caption = 'Preload'
    TabOrder = 8
    object CheckBox19: TCheckBox
      Left = 16
      Top = 24
      Width = 121
      Height = 17
      Caption = 'Preload Laufwerke'
      TabOrder = 0
    end
    object CheckBox20: TCheckBox
      Left = 16
      Top = 44
      Width = 121
      Height = 17
      Caption = 'Preload Ordner'
      TabOrder = 1
    end
    object CheckBox21: TCheckBox
      Left = 16
      Top = 64
      Width = 121
      Height = 17
      Caption = 'Preload Dateien'
      TabOrder = 2
    end
  end
end
