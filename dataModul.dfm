object dm: Tdm
  OldCreateOrder = False
  OnCreate = DataModuleCreate
  OnDestroy = DataModuleDestroy
  Height = 357
  Width = 466
  object db: TIBDatabase
    Params.Strings = (
      'user_name=SYSDBA'
      'password=masterkey'
      'lc_ctype=UTF8')
    LoginPrompt = False
    ServerType = 'IBServer'
    Left = 32
    Top = 128
  end
  object t: TIBTransaction
    DefaultDatabase = db
    AutoStopAction = saCommit
    Left = 160
    Top = 128
  end
  object q: TIBQuery
    Database = db
    Transaction = t
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    Left = 224
    Top = 128
  end
  object q2: TIBQuery
    Database = db
    Transaction = t
    BufferChunks = 1000
    CachedUpdates = False
    ParamCheck = True
    Left = 288
    Top = 128
  end
end
