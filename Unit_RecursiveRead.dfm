object Form7: TForm7
  Left = 0
  Top = 0
  Caption = 'Form7'
  ClientHeight = 579
  ClientWidth = 1077
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  DesignSize = (
    1077
    579)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 304
    Top = 16
    Width = 31
    Height = 13
    Caption = 'Label1'
  end
  object Label2: TLabel
    Left = 400
    Top = 16
    Width = 31
    Height = 13
    Caption = 'Label2'
  end
  object VRecusive: TVirtualStringTree
    Left = 24
    Top = 48
    Width = 1033
    Height = 505
    Anchors = [akLeft, akTop, akRight, akBottom]
    Header.AutoSizeIndex = 0
    Header.Height = 22
    Header.Options = [hoColumnResize, hoDblClickResize, hoDrag, hoOwnerDraw, hoShowImages, hoShowSortGlyphs, hoVisible]
    Images = Form1.ImageList1
    Margin = 1
    TabOrder = 0
    OnFreeNode = VRecusiveFreeNode
    OnGetText = VRecusiveGetText
    OnGetImageIndex = VRecusiveGetImageIndex
    OnInitChildren = VRecusiveInitChildren
    OnInitNode = VRecusiveInitNode
    Columns = <
      item
        Position = 0
        Text = 'Name'
        Width = 208
      end
      item
        Position = 1
        Text = 'Gr'#246#223'e'
        Width = 116
      end
      item
        Position = 3
        Text = 'Datum'
        Width = 74
      end
      item
        Position = 2
        Text = 'Pfad'
        Width = 54
      end
      item
        Position = 4
        Text = 'Hash'
        Width = 78
      end
      item
        Position = 5
        Text = 'Aufl'#246'sung'
        Width = 90
      end
      item
        Position = 6
        Text = 'Kamera'
        Width = 92
      end
      item
        Position = 7
        Text = 'Koordinaten'
        Width = 118
      end
      item
        Position = 8
        Text = 'ExifDatum'
        Width = 106
      end
      item
        Position = 9
        Text = 'Image'
        Width = 100
      end>
  end
  object Button1: TButton
    Left = 982
    Top = 8
    Width = 75
    Height = 25
    Caption = 'Abbrechen'
    TabOrder = 1
    OnClick = Button1Click
  end
end
