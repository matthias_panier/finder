object Form8: TForm8
  Left = 0
  Top = 0
  Caption = 'Log'
  ClientHeight = 434
  ClientWidth = 751
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  DesignSize = (
    751
    434)
  PixelsPerInch = 96
  TextHeight = 13
  object ListView1: TListView
    Left = 8
    Top = 8
    Width = 735
    Height = 418
    Anchors = [akLeft, akTop, akRight, akBottom]
    Columns = <
      item
        Caption = 'Prozess'
        Width = 150
      end
      item
        Caption = 'Funktion'
        Width = 120
      end
      item
        Caption = 'Startzeit'
        Width = 80
      end
      item
        Caption = 'Endzeit'
        Width = 80
      end
      item
        Caption = 'Delta'
        Width = 60
      end
      item
        AutoSize = True
        Caption = 'Message'
      end>
    FlatScrollBars = True
    GridLines = True
    TabOrder = 0
    ViewStyle = vsReport
  end
end
