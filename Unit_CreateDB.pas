unit Unit_CreateDB;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
    System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
    create_db_structure;

type
    TForm3 = class(TForm)
        GroupBox1: TGroupBox;
        Label1: TLabel;
        Image1: TImage;
        Edit1: TEdit;
        Button1: TButton;
        procedure Button1Click(Sender: TObject);
        procedure FormShow(Sender: TObject);
    private
        Fapproved: Boolean;
        procedure Setapproved(const Value: Boolean);
    public
        property approved: Boolean read Fapproved write Setapproved;
    end;

var
    Form3: TForm3;

implementation

{$R *.dfm}

uses dataModul;

procedure TForm3.Button1Click(Sender: TObject);
var
    edb: TClassCreateDBStructure;
begin
    approved := false;
    try
        edb := TClassCreateDBStructure.create;

        try
            if edb.createDB(Edit1.Text) then
            begin
                approved := true;
                dm.speichereDBPfad(Edit1.Text);
                showmessage('Datenbank erfolgreich angelegt');
            end else begin
                showmessage(edb.lastErrorMsg);
            end;
        finally
            edb.Free;
        end;
    except
        on E: exception do
        begin
            showmessage(E.ClassName + ': ' + E.Message);
        end;
    end;

    if approved then
    begin
        close;
    end;
end;

procedure TForm3.FormShow(Sender: TObject);
begin
    Edit1.Text := dm.leseDBPfad;
    approved := false;
end;

procedure TForm3.Setapproved(const Value: Boolean);
begin
    Fapproved := Value;
end;

end.
