program finder;

uses
  FastMM4 in 'library\fmm4\FastMM4.pas',
  FastMM4Messages in 'library\fmm4\FastMM4Messages.pas',
  Vcl.Forms,
  main in 'main.pas' {Form1},
  dataModul in 'dataModul.pas' {dm: TDataModule},
  Unit_ChooseDB in 'Unit_ChooseDB.pas' {Form2},
  Unit_CreateDB in 'Unit_CreateDB.pas' {Form3},
  Unit_Einstellungen in 'Unit_Einstellungen.pas' {Form4},
  create_db_structure in 'class\create_db_structure.pas',
  create_empty_db in 'class\create_empty_db.pas',
  registry_connection in 'class\registry_connection.pas',
  try_db_connection in 'class\try_db_connection.pas',
  classFilesystem in 'class\classFilesystem.pas',
  u_DM_type_def in 'class\u_DM_type_def.pas',
  CCR.Exif.BaseUtils in 'library\ccr\CCR.Exif.BaseUtils.pas',
  CCR.Exif.Consts in 'library\ccr\CCR.Exif.Consts.pas',
  CCR.Exif.IPTC in 'library\ccr\CCR.Exif.IPTC.pas',
  CCR.Exif.JpegUtils in 'library\ccr\CCR.Exif.JpegUtils.pas',
  CCR.Exif in 'library\ccr\CCR.Exif.pas',
  CCR.Exif.StreamHelper in 'library\ccr\CCR.Exif.StreamHelper.pas',
  CCR.Exif.TagIDs in 'library\ccr\CCR.Exif.TagIDs.pas',
  CCR.Exif.TiffUtils in 'library\ccr\CCR.Exif.TiffUtils.pas',
  CCR.Exif.XMPUtils in 'library\ccr\CCR.Exif.XMPUtils.pas',
  classDBBase in 'class\classDBBase.pas',
  FastMMUsageTracker in 'library\FastMMUsageTracker.pas' {fFastMMUsageTracker},
  ThreadGetFileIDs in 'threads\ThreadGetFileIDs.pas',
  ThreadGetFolderIDs in 'threads\ThreadGetFolderIDs.pas',
  classVirtuelDBFile in 'class\classVirtuelDBFile.pas',
  classVirtuelDBFolder in 'class\classVirtuelDBFolder.pas',
  classVirtuelDBDrive in 'class\classVirtuelDBDrive.pas',
  classVirtuelDBHashFile in 'class\classVirtuelDBHashFile.pas',
  ThreadHashFiles in 'threads\ThreadHashFiles.pas',
  ThreadFindDoublette in 'threads\ThreadFindDoublette.pas',
  classExif in 'class\classExif.pas',
  ThreadReadExif in 'threads\ThreadReadExif.pas',
  classVirtuelDBImages in 'class\classVirtuelDBImages.pas',
  ThreadFolderImport in 'threads\ThreadFolderImport.pas',
  classFilter in 'class\classFilter.pas',
  classLogging in 'class\classLogging.pas',
  Unit_Whitelist in 'Unit_Whitelist.pas' {Form5},
  Unit_Dialog in 'Unit_Dialog.pas' {Form6},
  classOptions in 'class\classOptions.pas',
  classFilesList in 'class\classFilesList.pas',
  classSysParams in 'class\classSysParams.pas',
  classVSTHandling in 'class\classVSTHandling.pas',
  Unit_RecursiveRead in 'Unit_RecursiveRead.pas' {Form7},
  classDBSet in 'class\classDBSet.pas',
  classComputerRead in 'class\classComputerRead.pas',
  classDriveRead in 'class\classDriveRead.pas',
  classFolderRead in 'class\classFolderRead.pas',
  classComputerReadDB in 'class\classComputerReadDB.pas',
  classDriveReadDB in 'class\classDriveReadDB.pas',
  Unit_logging in 'Unit_logging.pas' {Form8},
  classFolderReadDB in 'class\classFolderReadDB.pas';

{$R *.res}

begin
    Application.Initialize;
    Application.MainFormOnTaskbar := True;
    Application.CreateForm(Tdm, dm);
  Application.CreateForm(TForm1, Form1);
  Application.CreateForm(TForm2, Form2);
  Application.CreateForm(TForm3, Form3);
  Application.CreateForm(TForm4, Form4);
  Application.CreateForm(TForm5, Form5);
  Application.CreateForm(TForm6, Form6);
  Application.CreateForm(TForm7, Form7);
  Application.CreateForm(TForm8, Form8);
  Application.Run;

end.
