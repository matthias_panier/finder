unit Unit_ChooseDB;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
    System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls,
    IBX.IBDatabase, try_db_connection;

type
    TForm2 = class(TForm)
        Button1: TButton;
        Button2: TButton;
        Button3: TButton;
        Edit1: TEdit;
        GroupBox1: TGroupBox;
        Image1: TImage;
        Label1: TLabel;
        procedure Button1Click(Sender: TObject);
        procedure Button2Click(Sender: TObject);
        procedure Button3Click(Sender: TObject);
        procedure FormShow(Sender: TObject);
    private
        procedure loadDBPfad;
    end;

var
    Form2: TForm2;

implementation

{$R *.dfm}

uses dataModul, Unit_CreateDB, main;

resourcestring
    SMsgConnectionSuccess = 'Verbindung erfolgreich';

procedure TForm2.Button1Click(Sender: TObject);
var
    tcDB: TTryConnectDB;
begin
    try
        tcDB := TTryConnectDB.Create;
        try
        try
            tcDB.password := 'masterkey';
            tcDB.username := 'SYSDBA';
            if tcDB.tryConnectToDBByPath(Edit1.Text) then
            begin
                Showmessage(SMsgConnectionSuccess);
            end;
        except
          on E: Exception do
          begin
            showmessage(e.Message);
          end;
        end;
        finally
            tcDB.free;
        end;
    except
        on E: exception do
        begin
            Showmessage(E.ClassName + ': ' + E.Message);
        end;
    end;
end;

procedure TForm2.Button2Click(Sender: TObject);
begin
    dm.terminateAllThreads;

    form3.showmodal;
    if form3.approved then
    begin
        loadDBPfad;
    end;
end;

procedure TForm2.Button3Click(Sender: TObject);
begin
    dm.speichereDBPfad(Edit1.Text);
    close;
end;

procedure TForm2.FormShow(Sender: TObject);
begin
    loadDBPfad;
end;

procedure TForm2.loadDBPfad;
begin
    Edit1.Text := dm.leseDBPfad;
end;

end.
