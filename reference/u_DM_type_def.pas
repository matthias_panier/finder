unit u_DM_type_def;

// http://www.alberton.info/firebird_sql_meta_info.html
interface

uses Controls, Graphics, ExtCtrls, Forms, classes, Contnrs, SysUtils, Messages, VirtualTrees, CCR.Exif, Windows,
    CCR.Exif.TiffUtils;

const
    WM_AFTER_SHOW = WM_USER + 300; // custom message
    WM_AFTER_CREATE = WM_USER + 301; // custom message

type
    TFilesListEntry = record
        mask: string;
        minSize: integer;
        maxSize: integer;
    end;

type
    TLogSources = (lsNone, lsMain, lsDm, lsDb, lsSystem, lsThread);
    TLogLevel = (llNone, llInfo, llWarning, llError, llException);

    TMySource = (sNone, sFileSystem, sDataBase);
    TMyKind = (kNone, kRoot, kDrive, kFolder, kFile, kFilter);
    TMetaKind = (MyKnone, MyKaudio, MyKvideo, MyKimage);

    TMyNodeLinks = record
        Vfolder: PVirtualNode;
        Vfiles: PVirtualNode;
        VParent: PVirtualNode;
    end;

type
    TDBSet = record
        ComputerID: integer; // DB.computers.ID

        DriveID: integer; // DB.drives.ID
        FolderID: integer; // DB.folders.ID
        FileID: integer; // DB.files.ID
        ExtensionID: integer;
        FilterID: integer;
        SubFilterID: integer;
        DetailFilterID: integer;

        source: TMySource; // manually set source
        kind: TMyKind; // manually set type
    end;

    /// /////////////////////////////
    /// / Database corresponding ////
    /// /////////////////////////////
    TDBCAMMAN = record
        ID: integer;
        Manufactor: string; // 64 Zeichen
    end;

    TDBCamMod = record
        ID: integer;
        Modell: string; // 64 Zeichen
    end;

    TDBComputer = record
        ID: integer;
        Name: string; // 32

        add_date: int64; // when was the dataset inserted
    end;

    TDBDrive = record
        ID: integer;
        Computer_ID: integer;
        Folder_ID: integer;

        DSize: int64;
        SerialNR: string; // 32
        Caption: string; // 64

        add_date: int64;
    end;

    TDBFolder = record
        ID: integer;
        Parent_ID: integer;
        Computer_ID: integer;
        Drive_ID: integer;

        Path: string; // full path    // 255
        create_date: int64; // create date of folder

        Caption: string; // name  //128
        Deleted: integer;
        add_date: int64;
    end;

    TDBFile = record
        ID: integer;
        Computer_ID: integer;
        Drive_ID: integer;
        Folder_ID: integer;
        Icon_ID: integer;
        Ext_ID: integer;

        Filename: string; // 150
        Caption: string; // 150

        Contenthash: string; // 64

        FSize: int64;

        isOriginal: integer;
        isDublette: integer;
        isBackup: integer;
        isTagged: integer;
        Deleted: integer;

        create_date: int64; // file create date
        hash_date: int64; // date when file was hashed

        add_date: int64; // create Date in Database
    end;

    TDBExt = record
        ID: integer;
        ext: string; // 32
    end;

    TDBImgData = record
        ID: integer; // same as file_ID
        add_date: int64; // when added to DB
        change_date: int64; // when changed in DB

        exif_date: int64;
        exif_camman_id: integer;
        exif_cammod_id: integer;
        exif_desc: string; // 250
        exif_lat: real;
        exif_lon: real;
        exif_alt: integer;
        exif_x: integer;
        exif_y: integer;
        file_x: integer;
        file_y: integer;

        date: int64;
        camman_id: integer; // camman_ID
        cammod_id: integer; // cammod_ID
        desc: string; // 250

        x: integer;
        y: integer;
        alt: integer;
        lat: real;
        lon: real;
    end;



    TFlagSet = record
        isOriginal: boolean;
        isDublette: boolean;
        isBackup: boolean;
        isTagged: boolean;
        isReadFromDB: boolean;
        isReadFromFileSystem: boolean;
        Deleted: boolean; // only valid for files and folders
        isAddedToDB: boolean;
        isImage: boolean;
    end;

    TGeoCoordinates = record
        longitude, latitude: Extended;
        altitude: integer;
    end;

    TMetaDisplay = record
        Description: string;
        date: int64;
        Camera: string;
        Make: string;
        Coordinates: TGeoCoordinates;
    end;

    TMeta = record
        kind: TMetaKind;
        dimX: integer;
        dimY: integer;
        DPIX: integer;
        DPIY: integer;
        Description: string;
        Camera: string;
        Make: string;
        date: int64;
        Coordinates: TGeoCoordinates;

        display: TMetaDisplay;
    end;

    TComputer = record
        DB: TDBComputer;
        Nodes: TMyNodeLinks;

        imageIndex: integer;
        Name: string;
    end;

    TDrive = record
        DB: TDBDrive;
        Nodes: TMyNodeLinks;

        imageIndex: integer;
        Name: string;
    end;

    TFolder = record
        DB: TDBFolder;

        Nodes: TMyNodeLinks;

        imageIndex: integer;
        Name: string;
    end;

    TFile = record
        DB: record
            Datei: TDBFile;
            Image: TDBImgData;
        end;

        Nodes: TMyNodeLinks;

        imageIndex: integer;
        Name: string;
    end;


    // //     DBID: TDBSet;


    // //     imageIndex: integer; // windows image index
    // //     pfad: string; // DB.drives.??? // TODO Define the DB equivalent
    // //     serialNR: string; // DB.drives.SERIALNR
    // //     name: string; // DB.drives.NAME
    // //     caption: string; // DB.drives.CAPTION

    // //     size: int64; // TODO Where to gather and store?
    // //     used: int64; // TODO Where to gather and store?

    // //     Nodes: TMyNodeLinks;
    // // end;




    // //     DBID: TDBSet;
    // //     Flags: TFlagSet;


    // //     DBData: TDBFolder;

    // //     imageIndex: integer; // windows image index
    // //     pfad: string; // DB.folders.PATH
    // //     name: string; // DB.folders.FOLDER
    // //     parentFolderID: integer; // DB.folders.FOLDERS_ID
    // //     folder_created: int64;

    // //     Nodes: TMyNodeLinks;
    // // end;

    // TFileDisplay = record
    // Filename: string;
    // end;




    // DBID: TDBSet;
    // Flags: TFlagSet;


    // imageIndex: integer; // windows image index
    // pfad: string; // file path/ DB.folders.path
    // pfad_extended: string;
    // Filename: string; // DB.files.FILENAME
    // extension: string; // DB.files.EXT
    // extension_ID: integer; // DB.files.EXT_ID

    // file_created: int64; // file date / DB.files.CREATE_DATE

    // size: int64; // DB.files.FSIZE
    // Contenthash: string; // DB.files.CONTENTHASH

    // Nodes: TMyNodeLinks;
    // Meta: TMeta;
    // display: TFileDisplay;
    // end;



    TVNodeData= record
     DBID: TDBSet;
     Flags: TFlagSet;
//
//
     imageIndex: integer; // windows image index
//
     Image: TBitmap;
//
     parentFolderID: integer;
     caption: string;
     Filename: string;
     extension: string;
     hash: string;
     pfad: string;
//
     filter_where: string;
//
     size: int64;
     datum: int64;
     Meta: TMeta;
     Nodes: TMyNodeLinks;
     end;

    TComputers = array of TComputer;
    TDrives = array of TDrive;
    TFolders = array of TFolder;
    TFiles = array of TFile;
    PVNodeData = ^TVNodeData;

    { LOG }
    TLogEntry = record
        msg: string;
        timestamp: double;
        source: TLogSources;
        level: TLogLevel;
    end;

    TLog = record
        position: integer;
        laenge: integer;
        entry: array of TLogEntry;
    end;

    TAutoHashFilesExtensions = record
        extension: string;
        active: boolean;
        minSize: int64;
        maxSize: int64;
    end;

    TAutoHashFiles = record
        active: boolean;
        minSize: int64;
        maxSize: int64;

        useFilter: boolean;
        extensions: array of TAutoHashFilesExtensions;
    end;

    TOptionsDisplay = record
        ShowFolders: boolean;
        Filter: boolean;
        FilterMask: string;
        ExpandDoubletten: boolean;
        ExpandFolders: boolean;
        ShowDoubletten: boolean;
    end;

    TOptionsLiveImport = record
        Folders: boolean;
        Files: boolean;
        hash: boolean;
        Exif: boolean;
        UseMaxFileSize: boolean;
        MaxFileSize: int64;
    end;

    TOptionsRecursiveRead = record

        UseThread: boolean;
        hashFiles: boolean;
        exifFiles: boolean;
        CombineInFolders: boolean;
        ExpandCombinedFolders: boolean;
        ScollToEnd: boolean;
        UseMaxFileSize: boolean;
        MaxFileSize: int64;
    end;

    TOptionsFolderList = record
        active: boolean;
        List: TStringlist;
    end;

    TExplodeArray = array of String;
function Explode(const cSeparator, vString: String): TExplodeArray;
function Implode(const cSeparator: String; const cArray: TExplodeArray): String;
function OccurrencesOfChar(const S: string; const C: char): integer;

function BoolToString(b: boolean): string;
function StringToBool(S: string): boolean;

function getEmptyTFolder: TFolder;

function getEmptyTIDSet: TDBSet;
function getEmptyTMeta: TMeta;

function getEmptyTFlags: TFlagSet;

function retrieveVolumeName(DriveLetter: char): string;
function retrieveVolumeSerialNumber(const Folder: string): string;
function IsNodeVisibleInClientRect(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex = NoColumn)
    : boolean;

function GPSLatToReal(const l: TGPSLatitude): real;
function GPSLonToReal(const l: TGPSLongitude): real;
function GPSAltToInt(const l: TExifFraction): integer;

implementation

function BoolToString(b: boolean): string;
begin
    if b then
        result := 'True'
    else
        result := 'False';
end;

function StringToBool(S: string): boolean;
begin
    result := S = 'True';
end;

function GPSLatToReal(const l: TGPSLatitude): real;
var
    ref: integer;
begin
    // d:= ExifData.GPSLatitude.Degrees.Quotient; {Degrees}
    // m:= ExifData.GPSLatitude.Minutes.Quotient; {Minutes}
    // s:= ExifData.GPSLatitude.Seconds.Quotient; {Seconds}

    result := 0;

    { Now determine the sign }
    if l.Direction = ltNorth then
        ref := 1
    else
        ref := -1;

    { Convert to decimal }
    result := ref * (l.degrees.Quotient + (l.Minutes.Quotient / 60) + (l.Seconds.Quotient / 60 / 60));

end;

function GPSLonToReal(const l: TGPSLongitude): real;
var
    ref: integer;
begin
    result := 0;

    { Now determine the sign }
    if l.Direction = lnEast then
        ref := 1
    else
        ref := -1;

    { Convert to decimal }
    result := ref * (l.degrees.Quotient + (l.Minutes.Quotient / 60) + (l.Seconds.Quotient / 60 / 60));

end;

function GPSAltToInt(const l: TExifFraction): integer;
begin
    result := 0;
    if (l.Denominator <> 0) then
        result := trunc(l.Numerator / l.Denominator);
end;

function convertLLA: real;
begin

end;

function getEmptyTIDSet: TDBSet;
begin

    result.FileID := 0;
    result.FolderID := 0;
    result.DriveID := 0;
    result.ComputerID := 0;
    result.ExtensionID := 0;
    result.source := sNone;
    result.kind := kNone;
end;

function getEmptyTFolder: TFolder;
begin
//    result.DBID := getEmptyTIDSet;
//    result.Flags := getEmptyTFlags;

    result.imageIndex := 0;
//    result.pfad := '';
    result.Name := '';
//    result.parentFolderID := 0;
//    result.folder_created := 0;
    result.Nodes.Vfolder := NIL;
    result.Nodes.Vfiles := NIL;
end;

function getEmptyTFlags: TFlagSet;
begin
    result.isOriginal := false;
    result.isDublette := false;
    result.isBackup := false;
    result.isTagged := false;
    result.isReadFromDB := false;
    result.isAddedToDB := false;
    result.isImage := false;
end;

function getEmptyTFile: TFile;
begin
//    result.DBID := getEmptyTIDSet;
//    result.Meta := getEmptyTMeta;
//    result.Flags := getEmptyTFlags;

    result.imageIndex := 0;
//    result.pfad := '';
//    result.pfad_extended := '';
//    result.Filename := '';
//    result.extension := '';
//    result.file_created := 0;
//    result.size := 0;
//    result.Contenthash := '';
    result.Nodes.Vfolder := NIL;
    result.Nodes.Vfiles := NIL;
end;

function getEmptyTMeta: TMeta;
begin
    result.kind := MyKnone;
    result.dimX := 0;
    result.dimY := 0;
    result.DPIX := 0;
    result.DPIY := 0;
    result.Description := '';
    result.Camera := '';
    result.Make := '';
    result.date := 0;
    result.Coordinates.longitude := 0;
    result.Coordinates.latitude := 0;
    result.Coordinates.altitude := 0;
end;

function retrieveVolumeName(DriveLetter: char): string;
var
    dummy: DWORD;
    buffer: array [0 .. MAX_PATH] of char;
    oldmode: LongInt;
begin
    oldmode := SetErrorMode(SEM_FAILCRITICALERRORS);
    try
        GetVolumeInformation(PChar(DriveLetter + ':\'), buffer, SizeOf(buffer), nil, dummy, dummy, nil, 0);
        result := StrPas(buffer);
    finally
        SetErrorMode(oldmode);
    end;
end;

function retrieveVolumeSerialNumber(const Folder: string): string;
var
    drive: string;
    FileSysName, VolName: array [0 .. 255] of char;
    SerialNum, MaxCLength, FileSysFlag: DWORD;
begin
    result := '';
    drive := IncludeTrailingPathDelimiter(extractFileDrive(Folder));
    GetVolumeInformation(PChar(drive), VolName, 255, @SerialNum, MaxCLength, FileSysFlag, FileSysName, 255);
    result := IntToHex(SerialNum, 8);
end;

function IsNodeVisibleInClientRect(Tree: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex = NoColumn)
    : boolean;
begin
    result := Tree.IsVisible[Node] and Tree.GetDisplayRect(Node, Column, false).IntersectsWith(Tree.ClientRect);
end;

function Implode(const cSeparator: String; const cArray: TExplodeArray): String;
var
    i: integer;
begin
    result := '';
    for i := 0 to length(cArray) - 1 do
    begin
        result := result + cSeparator + cArray[i];
    end;
    System.Delete(result, 1, length(cSeparator));
end;

function Explode(const cSeparator, vString: String): TExplodeArray;
var
    i: integer;
    S: String;
begin
    S := vString;
    setLength(result, 0);
    i := 0;
    while Pos(cSeparator, S) > 0 do
    begin
        setLength(result, length(result) + 1);
        result[i] := Copy(S, 1, Pos(cSeparator, S) - 1);
        Inc(i);
        S := Copy(S, Pos(cSeparator, S) + length(cSeparator), length(S));
    end;
    setLength(result, length(result) + 1);
    result[i] := Copy(S, 1, length(S));
    S := '';
end;

function OccurrencesOfChar(const S: string; const C: char): integer;
var
    i: integer;
begin
    result := 0;
    for i := 1 to length(S) do
        if S[i] = C then
            Inc(result);
end;

end.

/// ///////////////////////////
// Callback Function declaration
/// ///////////////////////////

// type
// TInsert = function(f: TMyFile): boolean of object; stdcall;
//
// type
// TInsertHash = function(f: TMyFile): boolean of object; stdcall;
//
// type
// TGetHashTS = function(f: TMyFile): TMyFile of object; stdcall;
//
// type
// TUpdateProgress = function(position: integer; msg: string; index: integer): boolean of object; stdCall;





// function inttobool(i: integer): boolean;
// function booltoint(b: boolean): integer;


// function iff(b: boolean; resultTrue, resultFalse: variant): variant;
//
// var
// alphaBlend: boolean;
// alphaBlendValue: Byte;

// function booltoint(b: boolean): integer;
// begin
// if b then
// result := 1
// else
// result := 0;
// end;

// function inttobool(i: integer): boolean;
// begin
// result := i = 1;
// end;

// function iff(b: boolean; resultTrue, resultFalse: variant): variant;
// begin
// if b then
// result := resultTrue
// else
// result := resultFalse;
// end;

/// https://stackoverflow.com/questions/829843/how-to-get-icon-and-description-from-file-extension-using-delphi
// function GetGenericFileType(AExtension: string): string;
{ Get file type for an extension }
// var
// AInfo: TSHFileInfo;
// begin
// SHGetFileInfo( PChar( AExtension ), FILE_ATTRIBUTE_NORMAL, AInfo, SizeOf( AInfo ),
// SHGFI_TYPENAME or SHGFI_USEFILEATTRIBUTES );
// Result := AInfo.szTypeName;
// end;

// function GetGenericIconIndex(AExtension: string): integer;
{ Get icon index for an extension type }
// var
// AInfo: TSHFileInfo;
// begin
// if SHGetFileInfo( PChar( AExtension ), FILE_ATTRIBUTE_NORMAL, AInfo, SizeOf( AInfo ),
// SHGFI_SYSICONINDEX or SHGFI_SMALLICON or SHGFI_USEFILEATTRIBUTES ) <> 0 then
// Result := AInfo.iIcon
// else
// result := -1;
// end;

// function GetGenericFileIcon(AExtension: string): TIcon;
{ Get icon for an extension }
// var
// AInfo: TSHFileInfo;
// AIcon: TIcon;
// begin
// if SHGetFileInfo( PChar( AExtension ), FILE_ATTRIBUTE_NORMAL, AInfo, SizeOf( AInfo ),
// SHGFI_SYSICONINDEX or SHGFI_SMALLICON or SHGFI_USEFILEATTRIBUTES ) <> 0 then
// begin
// AIcon := TIcon.Create;
// try
// AIcon.Handle := AInfo.hIcon;
// Result := AIcon;
// except
// AIcon.Free;
// raise;
// end;
// end
// else
// result := nil;
// end;

// end.
