unit classDBSet;

interface

uses u_DM_type_def;

type
    TClassDBSet = class
    
    private
        Computer : TDBComputer ;
        Drive: TDBDrive;
        Folder: TDBFolder;
        Datei: TDBFile;
        Image: TDBImgData;

    public
        constructor create;
        destructor destroy; override;
    end;

implementation

/// ///////////////////////////
// on creation
/// ///////////////////////////
constructor TClassDBSet.create;
begin
    inherited;

    Finalize(Computer);
    Finalize(Drive);
    Finalize(Folder);
    Finalize(Datei);

//    setLength(dateien, 0);
end;

/// ///////////////////////////
// on destroy
/// ///////////////////////////
destructor TClassDBSet.destroy;
begin
//    setLength(dateien, 0);

    inherited;
end;

end.
