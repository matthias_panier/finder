unit create_db_structure;

interface

uses IBX.IBDatabase, IBX.IBQuery, create_empty_db;

type
    TClassCreateDBStructure = class(TClassCreateEmptyDatabase)
    private
        function addDBIndex(iq: tibquery; const table, field: string): boolean;
        function createTable_Computer(iq: tibquery): boolean;
        function createTable_Drive(iq: tibquery): boolean;
        function createTable_Folder(iq: tibquery): boolean;
        function createTable_Extensions(iq: tibquery): boolean;
        function createTable_File(iq: tibquery): boolean;
        function createTable_FilesHistory(iq: tibquery): boolean;
        function createTable_Icons(iq: tibquery): boolean;
        function createTable_Tags(iq: tibquery): boolean;
        function createTable_TagValues(iq: tibquery): boolean;
        function createTable_imgdata(iq: tibquery): boolean;
        function createTable_CamManufactor(iq: tibquery): boolean;
        function createTable_CamModell(iq: tibquery): boolean;
        
    public
        function createDB(const pfad: string): boolean;
    end;

implementation

function TClassCreateDBStructure.createDB(const pfad: string): boolean;
var
    db: TIBDatabase;
    t: TIBTransaction;
    q: tibquery;
begin
    result := false;

    if createEmptyDB(pfad) then
    begin
        db := TIBDatabase.create(nil);
        try
            t := TIBTransaction.create(nil);
            try
                q := tibquery.create(nil);
                try
                    db.Params.Clear;
                    db.Params.Add('user_name=sysdba');
                    db.Params.Add('password=masterkey');
                    db.Params.Add('lc_ctype=UTF8');

                    db.DatabaseName := pfad;
                    db.LoginPrompt := false;
                    db.DefaultTransaction := t;
                    db.IdleTimer := 0;
                    db.SQLDialect := 3;
                    db.TraceFlags := [];

                    t.Active := false;
                    t.DefaultDatabase := db;
                    t.AutoStopAction := saCommit;

                    q.Database := db;
                    q.Transaction := t;
                    q.CachedUpdates := false;
                    q.BufferChunks := 1000;

                    db.Open;

                    // createTable_Loader(q);
                    createTable_Computer(q);
                    createTable_Drive(q);
                    createTable_Folder(q);
                    createTable_Extensions(q);
                    createTable_Files(q);
                    createTable_FilesHistory(q);
                    createTable_Icons(q);
                    createTable_Tags(q);
                    createTable_TagValues(q);

                    createTable_imgdata(q);
                    createTable_CamManufactor(q);
                    createTable_CamModell(q);

                    result := db.Connected;

                finally
                    q.SQL.Clear;
                    q.Params.Clear;
                    q.Free;
                end;
            finally
                t.Free;
            end;
        finally
            db.Free;
        end;
    end;
end;

function TClassCreateDBStructure.addDBIndex(iq: tibquery; const table, field: string): boolean;
begin
    result := false;

    iq.SQL.Text := 'CREATE INDEX ' + table + '_' + field + ' ON ' + table + '(' + field + ');';

    try
        iq.ExecSQL;
        result := true;
        iq.Close;
        iq.SQL.Clear;
    except

    end;
end;

function TClassCreateDBStructure.createTable_Computer(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "COMPUTER" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"NAME" VARCHAR(32) CHARACTER SET "UTF8",');

    iq.SQL.Add('"ADD_DATE" BIGINT');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        iq.SQL.Clear;

        addDBIndex(iq, 'COMPUTER', 'ID');
        addDBIndex(iq, 'COMPUTER', 'NAME');

        result := true;
    except

    end;

end;

function TClassCreateDBStructure.createTable_Drive(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "DRIVE" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"COMPUTER_ID" INTEGER,');
    iq.SQL.Add('"FOLDER_ID" INTEGER,');

    iq.SQL.Add('"DSIZE" BIGINT,');
    iq.SQL.Add('"SERIALNR" VARCHAR(32) CHARACTER SET "UTF8",');
    iq.SQL.Add('"CAPTION" VARCHAR(64) CHARACTER SET "UTF8",');

    iq.SQL.Add('"ADD_DATE" BIGINT');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        iq.SQL.Clear;

        addDBIndex(iq, 'DRIVE', 'ID');
        addDBIndex(iq, 'DRIVE', 'SERIALNR');
        
        result := true;
    except

    end;
end;

function TClassCreateDBStructure.createTable_Folder(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "FOLDER" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"PARENT_ID" INTEGER,');
    iq.SQL.Add('"COMPUTER_ID" INTEGER,');
    iq.SQL.Add('"DRIVE_ID" INTEGER,');

    iq.SQL.Add('"PATH" VARCHAR(255) CHARACTER SET "UTF8",');

    iq.SQL.Add('"CAPTION" VARCHAR(128) CHARACTER SET "UTF8",');
    iq.SQL.Add('"DELETED" INTEGER,');   // marks this Folder as Deleted

    iq.SQL.Add('"CREATE_DATE" BIGINT ,');
    iq.SQL.Add('"ADD_DATE" BIGINT');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        iq.SQL.Clear;

        addDBIndex(iq, 'FOLDER', 'ID');
        addDBIndex(iq, 'FOLDER', 'PATH');
        addDBIndex(iq, 'FOLDER', 'PARENT_ID');
        addDBIndex(iq, 'FOLDER', 'DELETED');

        result := true;
    except

    end;
end;

function TClassCreateDBStructure.createTable_File(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "FILE" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"COMPUTER_ID" INTEGER,');
    iq.SQL.Add('"DRIVE_ID" INTEGER,');
    iq.SQL.Add('"FOLDER_ID" INTEGER,');
    iq.SQL.Add('"ICON_ID" INTEGER,');
    iq.SQL.Add('"EXT_ID" INTEGER,');

    iq.SQL.Add('"FILENAME" VARCHAR(150) CHARACTER SET "UTF8" ,');
    iq.SQL.Add('"FSIZE" BIGINT ,');

    iq.SQL.Add('"CAPTION" VARCHAR(150) CHARACTER SET "UTF8" ,');
    iq.SQL.Add('"CONTENTHASH" VARCHAR(64) CHARACTER SET "UTF8" ,');

    iq.SQL.Add('"ISORIGINAL" INTEGER,');
    iq.SQL.Add('"ISDUBLETTE" INTEGER,');
    iq.SQL.Add('"ISBACKUP" INTEGER,');
    iq.SQL.Add('"ISTAGGED" INTEGER,');
    iq.SQL.Add('"DELETED" INTEGER,');   // marks this Folder as Deleted

    iq.SQL.Add('"CREATE_DATE" BIGINT,');
    iq.SQL.Add('"HASH_DATE" BIGINT,');

    iq.SQL.Add('"ADD_DATE" BIGINT');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        iq.SQL.Clear;

        addDBIndex(iq, 'FILE', 'ID');
        addDBIndex(iq, 'FILE', 'FOLDER_ID');
        addDBIndex(iq, 'FILE', 'DELETED');
        addDBIndex(iq, 'FILE', 'EXT_ID');
        addDBIndex(iq, 'FILE', 'FSIZE');
        addDBIndex(iq, 'FILE', 'CAPTION');

        iq.SQL.Text := 'CREATE INDEX FID_FN ON FILE(FILENAME);';
        try
            iq.ExecSQL;
            iq.Close;
            iq.SQL.Clear;
        except

        end;
        iq.SQL.Text := 'CREATE INDEX FID_CH ON FILE(CONTENTHASH);';
        try
            iq.ExecSQL;
            iq.Close;
            iq.SQL.Clear;
        except

        end;
        iq.SQL.Text := 'CREATE INDEX FILES_HSIZE ON FILE(FSIZE,CONTENTHASH);'; // hash size ID
        try
            iq.ExecSQL;
            iq.Close;
            iq.SQL.Clear;
        except

        end;

        result := true;
    except

    end;
end;

function TClassCreateDBStructure.createTable_Extensions(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "EXT" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"EXT" VARCHAR(32) CHARACTER SET "UTF8"');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        iq.SQL.Clear;

        addDBIndex(iq, 'EXT', 'ID');
        addDBIndex(iq, 'EXT', 'EXT');

        result := true;
    except

    end;
end;

function TClassCreateDBStructure.createTable_FilesHistory(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "FILES_HISTORY" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"FILES_ID" INTEGER,');
    iq.SQL.Add('"FOLDERS_ID_OLD" INTEGER,');
    iq.SQL.Add('"FILENAME_OLD" VARCHAR(255) CHARACTER SET "UTF8" ,');
    iq.SQL.Add('"EXT_OLD" VARCHAR(32) CHARACTER SET "UTF8" ,');
    iq.SQL.Add('"CREATED" BIGINT ');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        iq.SQL.Clear;

        addDBIndex(iq, 'FILES_HISTORY', 'ID');
        addDBIndex(iq, 'FILES_HISTORY', 'FILES_ID');
        addDBIndex(iq, 'FILES_HISTORY', 'FOLDERS_ID_OLD');

    except

    end;
end;

function TClassCreateDBStructure.createTable_Icons(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "ICON" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"EXT" VARCHAR(32) CHARACTER SET "UTF8" ,');
    iq.SQL.Add('"DESCRIPTION" VARCHAR(64) CHARACTER SET "UTF8" ,');
    iq.SQL.Add('"RAW" BLOB');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        iq.SQL.Clear;

        addDBIndex(iq, 'ICON', 'ID');
        addDBIndex(iq, 'ICON', 'EXT');
    except

    end;

end;

function TClassCreateDBStructure.createTable_Tags(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "TAGS" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"FILES_ID" INTEGER');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;

        addDBIndex(iq, 'TAGS', 'ID');
        addDBIndex(iq, 'TAGS', 'FILES_ID');

        result := true;
    except

    end;

end;

function TClassCreateDBStructure.createTable_TagValues(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "TAG_VALUES" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"CAPTION" VARCHAR(16) CHARACTER SET "UTF8"');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;
        addDBIndex(iq, 'TAG_VALUES', 'ID');
        addDBIndex(iq, 'TAG_VALUES', 'CAPTION');

        result := true;
    except

    end;
end;

function TClassCreateDBStructure.createTable_imgdata(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "IMGDATA" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"EXIF_DATE" BIGINT,');
    iq.SQL.Add('"EXIF_CAMMAN_ID" INTEGER,');
    iq.SQL.Add('"EXIF_CAMMOD_ID" INTEGER,');
    iq.SQL.Add('"EXIF_LON" FLOAT,');
    iq.SQL.Add('"EXIF_LAT" FLOAT,');
    iq.SQL.Add('"EXIF_ALT" INTEGER,');
    iq.SQL.Add('"EXIF_X" INTEGER,');
    iq.SQL.Add('"EXIF_Y" INTEGER,');
    iq.SQL.Add('"EXIF_DESC" VARCHAR(250) CHARACTER SET "UTF8" ,');
    iq.SQL.Add('"FILE_X" INTEGER,');
    iq.SQL.Add('"FILE_Y" INTEGER,');

    iq.SQL.Add('"DATE" BIGINT,');
    iq.SQL.Add('"CAMMAN_ID" INTEGER,');
    iq.SQL.Add('"CAMMOD_ID" INTEGER,');
    iq.SQL.Add('"DESC" VARCHAR(250) CHARACTER SET "UTF8" ,');

    iq.SQL.Add('"LON" FLOAT,');
    iq.SQL.Add('"LAT" FLOAT,');
    iq.SQL.Add('"ALT" INTEGER,');
    iq.SQL.Add('"X" INTEGER,');
    iq.SQL.Add('"Y" INTEGER,');

    iq.SQL.Add('"CHANGE_DATE" BIGINT,');
    iq.SQL.Add('"ADD_DATE" BIGINT ');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;

        addDBIndex(iq, 'IMGDATA', 'ID');
        addDBIndex(iq, 'IMGDATA', 'CAMMAN_ID');
        addDBIndex(iq, 'IMGDATA', 'CAMMOD_ID');
        addDBIndex(iq, 'IMGDATA', 'DATE');
        addDBIndex(iq, 'IMGDATA', 'LON');
        addDBIndex(iq, 'IMGDATA', 'LAT');
        addDBIndex(iq, 'IMGDATA', 'ALT');
        addDBIndex(iq, 'IMGDATA', 'X');
        addDBIndex(iq, 'IMGDATA', 'Y');

        addDBIndex(iq, 'IMGDATA', 'DISMAN_ID');
        addDBIndex(iq, 'IMGDATA', 'DISMOD_ID');
        addDBIndex(iq, 'IMGDATA', 'DISPLAY_LON');
        addDBIndex(iq, 'IMGDATA', 'DISPLAY_LAT');
        addDBIndex(iq, 'IMGDATA', 'DISPLAY_ALT');
        addDBIndex(iq, 'IMGDATA', 'DESCRIPTION');
        addDBIndex(iq, 'IMGDATA', 'DISPLAY_DESC');
        addDBIndex(iq, 'IMGDATA', 'DISPLAY_DATE');

        iq.SQL.Text := 'CREATE INDEX GID_LLA ON IMGDATA(LON,LAT,ALT);';
        try
            iq.ExecSQL;
            iq.Close;
        except

        end;

        iq.SQL.Text := 'CREATE INDEX GID_LL ON IMGDATA(LON,LAT);';
        try
            iq.ExecSQL;
            iq.Close;
        except

        end;

        iq.SQL.Text := 'CREATE INDEX GID_XY ON IMGDATA(X,Y);';
        try
            iq.ExecSQL;
            iq.Close;
        except

        end;

        result := true;
    except

    end;
end;

function TClassCreateDBStructure.createTable_CamManufactor(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "CAMMAN" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"MANUFACTOR" VARCHAR(100) CHARACTER SET "UTF8"');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;

        addDBIndex(iq, 'CAMMAN', 'ID');
        addDBIndex(iq, 'CAMMAN', 'MANUFACTOR');
        finally
          
        end;
end;

function TClassCreateDBStructure.createTable_CamModell(iq: tibquery): boolean;
begin
    result := false;

    iq.Close;
    iq.SQL.Clear;
    iq.SQL.Add('CREATE TABLE "CAMMOD" (');

    iq.SQL.Add('"ID" INTEGER,');
    iq.SQL.Add('"MODELL" VARCHAR(100) CHARACTER SET "UTF8"');

    iq.SQL.Add(');');

    try
        iq.ExecSQL;
        iq.Close;

        addDBIndex(iq, 'CAMMOD', 'ID');
        addDBIndex(iq, 'CAMMOD', 'MODELL');
        finally
          
        end;
end;

end.
