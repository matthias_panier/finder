﻿unit main;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
    System.Classes, Vcl.Graphics, Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
    Vcl.ComCtrls, Vcl.Menus, Vcl.StdCtrls, FileCtrl, System.ImageList, Vcl.ImgList, Vcl.Buttons, Vcl.Samples.Gauges,
    VirtualTrees, Vcl.ExtCtrls, System.IOUtils, ClassExif,

    ShellApi, Vcl.ToolWin, u_DM_type_def, VclTee.TeeGDIPlus, VclTee.TeEngine, VclTee.Series, VclTee.TeeProcs,
    VclTee.Chart, Data.DB,
    VclTee.DBChart, DateUtils, IdHashMessageDigest, ClassDBSet;

// function CB_text(const s: string): boolean; stdcall;
procedure CB_AddComputer(computer: TComputer); stdcall;
procedure CB_AddDrive(Drive: TDrive; ParentNode: PVirtualNode); stdcall;
procedure CB_AddFolder(folder: TFolder; ParentNode: PVirtualNode); stdcall;
procedure CB_AddDatei(Datei: TDatei); stdcall;

type
    TForm1 = class(TForm)
        Beenden1: TMenuItem;
        Datei1: TMenuItem;
        Datenbank1: TMenuItem;
        Einstellungen1: TMenuItem;
        Einstellungen2: TMenuItem;
        ImageList1: TImageList;
        ImageList2: TImageList;
        MainMenu1: TMainMenu;

        StatusBar1: TStatusBar;

        Splitter1: TSplitter;
        Splitter2: TSplitter;

        Panel1: TPanel;
        Panel2: TPanel;
        Panel3: TPanel;
        PVFiles1: TPanel;
        PVFolder1: TPanel;

        Button1: TButton;
        Button2: TButton;

        Label1: TLabel;
        Label6: TLabel;
        Label7: TLabel;

        VFolder: TVirtualStringTree;
        VFiles: TVirtualStringTree;

        GaugeFolders: TGauge;
        GaugeFiles: TGauge;
        Gauge1: TGauge;

        Image1: TImage;
        Label2: TLabel;
        Label3: TLabel;
        Label4: TLabel;
        Panel4: TPanel;
        Label5: TLabel;
        Label8: TLabel;
        Label9: TLabel;
        Label10: TLabel;
        SpeedButton7: TSpeedButton;
        Label11: TLabel;
        Label12: TLabel;
        Label13: TLabel;
        Label14: TLabel;
        PopupMenuVFolder: TPopupMenu;
        PopupMenuVFiles: TPopupMenu;
        NeuenOrdnererstellen1: TMenuItem;
        Image2: TImage;
        OrdnerEinlesen1: TMenuItem;
        Logging1: TMenuItem;

        procedure Beenden1Click(Sender: TObject);
        procedure Button1Click(Sender: TObject);
        procedure Button2Click(Sender: TObject);
        procedure Datenbank1Click(Sender: TObject);
        procedure Einstellungen2Click(Sender: TObject);
        procedure FormClose(Sender: TObject; var Action: TCloseAction);
        procedure FormCreate(Sender: TObject);
        procedure FormShow(Sender: TObject);
        procedure Logging1Click(Sender: TObject);
        procedure SpeedButton7Click(Sender: TObject);

        procedure VFolderChange(Sender: TBaseVirtualTree; Node: PVirtualNode);

        procedure VFilesChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
        procedure VFilesDblClick(Sender: TObject);
        procedure VFilesCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode; Column: TColumnIndex;
            var Result: integer);
        procedure VFilesGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
            TextType: TVSTTextType; var CellText: string);
        procedure VFilesHeaderClick(Sender: TVTHeader; HitInfo: TVTHeaderHitInfo);
        procedure VFilesKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
        procedure VFilesPaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode;
            Column: TColumnIndex; TextType: TVSTTextType);
        procedure VSTFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
        procedure VSTGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; kind: TVTImageKind;
            Column: TColumnIndex; var Ghosted: boolean; var ImageIndex: TImageIndex);
        procedure VFolderGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
            TextType: TVSTTextType; var CellText: string);
        procedure VFolderInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode;
            var InitialStates: TVirtualNodeInitStates);
        procedure VFolderPaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode;
            Column: TColumnIndex; TextType: TVSTTextType);
    private
        function UpdatePanel(const text: string; const position: integer): boolean;

        procedure VFolderNodeSelect(SelectedNode: PVirtualNode);

        procedure setLocalMode;

        procedure loadWindowPositionFromReg;

        procedure reloadDB;
        procedure storeWindowPositionInReg;
        procedure WmAfterCreate(var Msg: TMessage); message WM_AFTER_CREATE;
        procedure WmAfterShow(var Msg: TMessage); message WM_AFTER_SHOW;

        procedure showEinstellungen;
    public
        s: String;

        /// ///////////////

        procedure displayProgressReset;

        function showActivity(const text: string): boolean;
        function showFolderInfo(const text: string): boolean;
        function showFileInfo(const text: string): boolean;
        function showConnectionStatus(const text: string): boolean;
        function showDBname(const text: string): boolean;

        procedure BeforeDestruction; override;

        procedure UpdateDisplayRecursiveRead(const f: TDatei);
        procedure updateCounts;
    end;

var
    Form1: TForm1;
    count: integer;

implementation

{$R *.dfm}

uses dataModul, Unit_CHooseDB, Unit_Einstellungen, Unit_RecursiveRead, Unit_logging;

resourcestring

    SLeft = 'left';
    STop = 'top';
    SWidth = 'width';
    SHeight = 'height';
    SVFolder = 'VFolder';
    SVFiles = 'VFiles';
    SInfo = 'PInfo';

procedure CB_AddComputer(computer: TComputer); stdcall;
begin
    dm.VSTHandling.addComputer(Form1.VFolder, computer);
end;

procedure CB_AddDrive(Drive: TDrive; ParentNode: PVirtualNode); stdcall;
begin
    dm.VSTHandling.addDrive(Form1.VFolder, ParentNode, Drive);
end;

procedure CB_AddFolder(folder: TFolder; ParentNode: PVirtualNode); stdcall;
var
    VFolderNode, VFilesNode: PVirtualNode;
begin
    VFolderNode := dm.VSTHandling.addFolder(Form1.VFolder, ParentNode, folder);
    VFilesNode := dm.VSTHandling.addFolder(Form1.VFiles, nil, folder);
end;

procedure CB_AddDatei(Datei: TDatei); stdcall;
begin
    dm.VSTHandling.addFile(Form1.VFiles, nil, Datei);
end;

procedure TForm1.BeforeDestruction;
begin
    inherited;

    dm.terminateThreadGetFileIDs;
    dm.terminateThreadGetFolderIDs;
    dm.terminateThreadHashFiles;
    dm.terminateThreadGetDoubletten;
    dm.terminateThreadGetExif;
end;

/// ///////////////////////////////////////////
/// Reloads the DB
/// ///////////////////////////////////////////
procedure TForm1.reloadDB;
begin
    if (dm.leseDBPfad <> dm.DB.DatabaseName) then
    begin
        showConnectionStatus('Nicht verbunden');
        dm.reloadDatabase;

        if (dm.DB.Connected) then
        begin
            showConnectionStatus('Verbunden');
            showDBname(dm.DB.DatabaseName);

            dm.restartAllThreads;
            setLocalMode;
        end;
    end;
end;

{ *************************************
  *  Starting Ending
  *
  *
  *
  ************************************ }

/// ///////////////////////////////////////////
/// On Create
/// ///////////////////////////////////////////
procedure TForm1.FormCreate(Sender: TObject);
begin
    PostMessage(Self.Handle, WM_AFTER_CREATE, 0, 0);
end;

/// After Create
procedure TForm1.WmAfterCreate(var Msg: TMessage);
var
    hImgSm, hImgBig: Thandle;
    fi: TSHFileInfo;
begin
    loadWindowPositionFromReg;

    // Imageliste mit kleinen Symbolen ermitteln
    // und an die Imageliste auf der Form übergeben
    hImgSm := SHGetFileInfo('', 0, fi, sizeof(fi), SHGFI_SYSICONINDEX or SHGFI_SMALLICON);
    if (hImgSm <> 0) then
    begin
        ImageList1.Handle := hImgSm;
    end;

    // Imageliste mit großen Symbolen ermitteln
    // und an die Imageliste auf der Form übergeben
    hImgBig := SHGetFileInfo('', 0, fi, sizeof(fi), SHGFI_SYSICONINDEX or SHGFI_ICON);
    if (hImgBig <> 0) then
    begin
        ImageList2.Handle := hImgBig;
    end;
end;

procedure TForm1.FormShow(Sender: TObject);
begin
    PostMessage(Self.Handle, WM_AFTER_SHOW, 0, 0);
end;

procedure TForm1.WmAfterShow(var Msg: TMessage);
begin
    reloadDB;
    setLocalMode;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    storeWindowPositionInReg;
end;

procedure TForm1.Beenden1Click(Sender: TObject);
begin
    close;
end;

procedure TForm1.Datenbank1Click(Sender: TObject);
begin
    form2.showmodal;
    reloadDB;
end;

procedure TForm1.Einstellungen2Click(Sender: TObject);
begin
    showEinstellungen;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
    if (VFolder.SelectedCount > 0) and (VFolder.FocusedNode <> nil) then
    begin
        // show rekursive read dialog
        // run recursice import
        form7.showmodal;
    end;
end;

{ *************************************
  * Select Node in left outline
  *
  * Main start
  *
  ************************************ }
procedure TForm1.VFolderChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
    if (VFolder.SelectedCount > 0) and (Node <> nil) then
    begin
        application.ProcessMessages;
        VFolderNodeSelect(Node);
    end;
end;

{ *************************************
  *  MAIN ACTIONS
  *
  *
  *
  *
  ************************************ }

/// //////////////////////////////////
/// / Main Entry Point for local files
/// //////////////////////////////////
procedure TForm1.setLocalMode;
begin
    VFolder.Clear;
    VFolder.NodeDataSize := sizeof(TClassDBSet);
    VFiles.Clear;
    VFiles.NodeDataSize := sizeof(TClassDBSet);

    dm.loadComputer(CB_AddComputer);
end;

/// ///////
/// displays the initial state when starting to list a folder entry
/// ///////
procedure TForm1.displayProgressReset;
begin
    Label6.Caption := '0 / 0';
    Label7.Caption := '0 / 0';
end;

/// //////////////////////////////////////////////
/// Generate Folders Entries in VFolder and VFiles
/// Also Adds files to VFiles
/// //////////////////////////////////////////////
procedure TForm1.VFolderNodeSelect(SelectedNode: PVirtualNode);
var
    FolderItem: TClassDBSet;
begin
    VFiles.Clear;
    application.ProcessMessages;

    FolderItem := dm.GetVSTItem(VFolder, SelectedNode);
    // OrdnerEinlesen1.Caption := 'Ordern [' + FolderItem.Caption + '] einlesen ...';
    // OrdnerEinlesen1.Enabled := not FolderItem.isComputer;

    if FolderItem.isComputer then
    begin
        dm.loadDrives(SelectedNode, CB_AddDrive);
    end
    else

    // if FolderItem.isDrive then
    begin
        dm.loadFolders(SelectedNode, @CB_AddFolder, @CB_AddDatei);
    end;

    // if (dm.cfg.options.Display.ExpandFolders) then
    // begin
    VFolder.Expanded[SelectedNode] := true;
    // end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
end;

{ *************************************
  *  OWN Internals
  *
  *
  *
  ************************************ }
/// ///////////////////////////////////////////
/// Stores the Window Position in the registry
/// ///////////////////////////////////////////
procedure TForm1.storeWindowPositionInReg;
begin
    dm.cfg.WriteToRegistry(SLeft, Form1.Left.ToString);
    dm.cfg.WriteToRegistry(STop, Form1.Top.ToString);
    dm.cfg.WriteToRegistry(SWidth, Form1.Width.ToString);
    dm.cfg.WriteToRegistry(SHeight, Form1.Height.ToString);
    dm.cfg.WriteToRegistry(SVFolder, PVFolder1.Width.ToString);
    dm.cfg.WriteToRegistry(SVFiles, PVFiles1.Width.ToString);
    dm.cfg.WriteToRegistry(SInfo, Panel1.Width.ToString);
end;

/// ////////////////////////////////////////////
/// Loads the Window Position from the registry
/// ////////////////////////////////////////////
procedure TForm1.loadWindowPositionFromReg;
begin
    Form1.Left := dm.cfg.ReadFromRegistry(SLeft, Form1.Left.ToString).toInteger;
    Form1.Top := dm.cfg.ReadFromRegistry(STop, Form1.Top.ToString).toInteger;
    Form1.Width := dm.cfg.ReadFromRegistry(SWidth, Form1.Width.ToString).toInteger;
    Form1.Height := dm.cfg.ReadFromRegistry(SHeight, Form1.Height.ToString).toInteger;
    PVFolder1.Width := dm.cfg.ReadFromRegistry(SVFolder, PVFolder1.Width.ToString).toInteger;
    PVFiles1.Width := dm.cfg.ReadFromRegistry(SVFiles, PVFiles1.Width.ToString).toInteger;
    Panel1.Width := dm.cfg.ReadFromRegistry(SInfo, Panel1.Width.ToString).toInteger;
end;

/// ///////////////////////////////////////////
/// Updates the status bar panels
/// Individual functions for specific panels
/// ///////////////////////////////////////////
function TForm1.UpdatePanel(const text: string; const position: integer): boolean;
begin
    StatusBar1.panels[position].text := text;
    StatusBar1.Update;

    Result := StatusBar1.panels[position].text = text;

    dm.logging.Add(llinfo, lsmain, text);
end;

/// Updates the statusbar panel 0
function TForm1.showConnectionStatus(const text: string): boolean;
begin
    Result := UpdatePanel(text, 0);
end;

/// Updates the statusbar panel 1
function TForm1.showDBname(const text: string): boolean;
begin
    Result := UpdatePanel(text, 1);
end;

/// Updates the statusbar panel 2
function TForm1.showActivity(const text: string): boolean;
begin
    Result := UpdatePanel(text, 2);
end;

/// Updates the statusbar panel 2
function TForm1.showFolderInfo(const text: string): boolean;
begin
    Result := UpdatePanel(text, 2);
end;

/// Updates the statusbar panel 2
function TForm1.showFileInfo(const text: string): boolean;
begin
    Result := UpdatePanel(text, 3);
end;

procedure TForm1.VFilesKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    Label4.Caption := inttostr(ord(Key));
end;

procedure TForm1.Logging1Click(Sender: TObject);
begin
    form8.show;
end;

procedure TForm1.showEinstellungen;
begin
    form4.showmodal;
end;

procedure TForm1.SpeedButton7Click(Sender: TObject);
begin
    updateCounts;
end;

{ this procedure is called when the next file is read via recursive read }
procedure TForm1.UpdateDisplayRecursiveRead(const f: TDatei);
begin
    GaugeFiles.MaxValue := Form1.GaugeFiles.MaxValue + 1;
    GaugeFiles.Update;
    Label7.Update;
    Label6.Caption := Form1.GaugeFiles.MaxValue.ToString;
    Label6.Update;

    // showFileInfo(f.pfad + f.filename);
end;

procedure TForm1.updateCounts;
begin
    Label5.Caption := dm.DBCountFolders;
    Label8.Caption := dm.DBCountFiles;
    Label9.Caption := dm.DBCountHahses;
    Label10.Caption := dm.DBCountFileSize;
end;

/// ///////////////////////////////////////////////////////
/// ///////////////////////////////////////////////////////
/// ///////////////////////////////////////////////////////
/// ///////////////////////////////////////////////////////
/// ///////////////////////////////////////////////////////

procedure TForm1.VFilesChange(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
    if (Node <> nil) then
    begin
        // VFilesNodeSelect(Node);
    end;
end;

procedure TForm1.VFilesPaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode;
    Column: TColumnIndex; TextType: TVSTTextType);
begin
    TargetCanvas.Font.Color := clBlack;

    if (dm.GetVSTItem(Sender, Node).isFromDB) then
    begin
        TargetCanvas.Font.Color := ClBlue;
    end;
    // Data := TBaseVirtualTree(Sender).GetNodeData(Node);
    //
    // case Column of
    // 0: // filename
    // begin
    // // TargetCanvas.Font.Style := Font.Style + [fsBold];
    // end;
    // 1: // filesize
    // begin
    // case Data.size of
    // 0 .. 1000000:
    // TargetCanvas.Font.Color := clblack;
    // 1000001 .. 5000000:
    // TargetCanvas.Font.Color := clGreen;
    // 5000001 .. 10000000:
    // TargetCanvas.Font.Color := clBlue;
    //
    // 10000001 .. 100000000:
    // TargetCanvas.Font.Color := clRed;
    // 100000001 .. 1000000000:
    // TargetCanvas.Font.Color := clmaroon;
    // else
    // TargetCanvas.Font.Color := clpurple;
    // end;
    // end;
    // end;
end;

{ *************************************
  *  Background Routines
  *
  *
  *
  *
  ************************************ }
procedure TForm1.VFilesCompareNodes(Sender: TBaseVirtualTree; Node1, Node2: PVirtualNode; Column: TColumnIndex;
    var Result: integer);
// var
// Data1: PVNodeData;
// Data2: PVNodeData;
begin
    // Result := 0;
    //
    // Data1 := VFiles.GetNodeData(Node1);
    // Data2 := VFiles.GetNodeData(Node2);
    //
    // if (assigned(Data1)) or (assigned(Data2)) then
    // begin
    // if (Column = 0) then
    // begin
    // // special sorting
    // // same kind of items get sorted within
    // // if ascending then first all folders get sortet, then all files get sortet.
    // // if descending, everything get sorted descending
    // if (Data1.dbID.kind = Data2.dbID.kind) then
    // Result := CompareText(Data1.Caption, Data2.Caption)
    // else if Data2.dbID.kind = kfile then
    // Result := -1
    // else
    // Result := 1;
    //
    // end else if (Column = 1) then
    // begin
    // if (Data1.size > Data2.size) then
    // Result := 1;
    // if (Data1.size = Data2.size) then
    // Result := 0;
    // if (Data1.size < Data2.size) then
    // Result := -1;
    // end;
    // end;
end;

procedure TForm1.VSTFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
    dm.GetVSTItem(Sender, Node).Free;
end;

{ *************************************
  *  VTree Images
  *
  *
  ************************************ }
procedure TForm1.VSTGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; kind: TVTImageKind;
    Column: TColumnIndex; var Ghosted: boolean; var ImageIndex: TImageIndex);
begin
    case kind of
        ikNormal, ikSelected:
            begin
                case Column of
                    0:
                        ImageIndex := dm.GetVSTItem(Sender, Node).ImageIndex;
                end;
            end;
    end;
end;

{ *************************************
  *  VTree Captions
  *
  *
  ************************************ }

procedure TForm1.VFolderGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
    TextType: TVSTTextType; var CellText: string);
begin
    case Column of
        0:
            CellText := dm.GetVSTItem(Sender, Node).Caption;
        1:
            CellText := formatCurr(',0', dm.GetVSTItem(Sender, Node).ComputerID);
        2:
            CellText := formatCurr(',0', dm.GetVSTItem(Sender, Node).DriveID);
        3:
            CellText := formatCurr(',0', dm.GetVSTItem(Sender, Node).FolderID);
        4:
            CellText := formatCurr(',0', dm.GetVSTItem(Sender, Node).ParentID);
    end;
end;

procedure TForm1.VFilesGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
    TextType: TVSTTextType; var CellText: string);
begin
    CellText := '';

    case Column of
        0:
            CellText := dm.GetVSTItem(Sender, Node).Caption;
        1:
            if (dm.GetVSTItem(Sender, Node).isFile) then
                CellText := formatCurr('#,0', dm.GetVSTItem(Sender, Node).Size);
        2:
            CellText := formatdatetime('c', UnixToDateTime(dm.GetVSTItem(Sender, Node).datum));
        3:
            CellText := dm.GetVSTItem(Sender, Node).fullPath;
        4:
            if (dm.GetVSTItem(Sender, Node).isHashed) then
                CellText := 'hashed';
        5:
            if (dm.GetVSTItem(Sender, Node).isTagged) then
                CellText := 'Tagged';
        6:
            // if (dm.GetVSTItem(Sender, Node).isFromDB) then
            CellText := inttostr(dm.GetVSTItem(Sender, Node).ID);
        // if (vData.Meta.dimX > 0) and (vData.Meta.dimY > 0) then
        // CellText := vData.Meta.dimX.ToString + ' x ' + vData.Meta.dimY.ToString;
        // 6:
        // if (vData.Meta.Display.camera <> '') then
        // begin
        // CellText := vData.Meta.Display.camera;
        // end else begin
        // CellText := vData.Meta.camera;
        // end;
        // 7:
        // if (vData.Meta.kind = MyKImage) AND (trunc(vData.Meta.Coordinates.latitude) <> 0) and
        // (trunc(vData.Meta.Coordinates.longitude) <> 0) then
        // FmtStr(CellText, 'Lat: %f Lon: %f - Alt: %d', [vData.Meta.Coordinates.latitude,
        // vData.Meta.Coordinates.longitude, vData.Meta.Coordinates.altitude]);
        // 8:
        // if (vData.Meta.kind = MyKImage) then
        // begin
        // if vData.Meta.date < 1 then
        // // showmessage(inttostr(vData.Meta.date))
        // else
        // CellText := formatdatetime('c', UnixToDateTime(vData.Meta.date));
        // end;
    end;
end;

procedure TForm1.VFilesDblClick(Sender: TObject);
begin
    if (VFiles.FocusedNode <> nil) then
    begin
        // actionVSTNode(VFiles.FocusedNode, VFolder.FocusedNode);
    end;
end;

/// //////////////////////////////////
/// / Sorting
/// //////////////////////////////////
procedure TForm1.VFilesHeaderClick(Sender: TVTHeader; HitInfo: TVTHeaderHitInfo);
begin
    if not dm.isVSTFillUp then
    begin
        VFiles.SortTree(HitInfo.Column, Sender.SortDirection, true);

        if Sender.SortDirection = sdAscending then
            Sender.SortDirection := sdDescending
        else
            Sender.SortDirection := sdAscending
    end;
end;

procedure TForm1.VFolderInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode;
    var InitialStates: TVirtualNodeInitStates);
var
    dbset: TClassDBSet;
begin
    // dbset := dm.GetVSTItem(Sender, Node);
    // if dbset.isComputer then
    // begin
    // dm.loadDrives(Node, addDriveCallBack);
    // Sender.Expanded[Node] := true;
    // end;
    //
    // if dbset.isDrive then
    // begin
    // dm.loadFolders(Node, @addFolderCallBack, @addFileCallBack);
    // Sender.Expanded[Node] := true;
    // end;
end;

procedure TForm1.VFolderPaintText(Sender: TBaseVirtualTree; const TargetCanvas: TCanvas; Node: PVirtualNode;
    Column: TColumnIndex; TextType: TVSTTextType);
begin
    TargetCanvas.Font.Color := clBlack;

    if (dm.GetVSTItem(Sender, Node).isFromDB) then
    begin
        TargetCanvas.Font.Color := ClBlue;
    end;
end;

end.
