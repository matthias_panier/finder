object Form5: TForm5
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = 'Whitelist'
  ClientHeight = 179
  ClientWidth = 300
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 273
    Height = 113
    Caption = 'Dateimuster zur Whitelist hinzuf'#252'gen'
    TabOrder = 0
    object Label1: TLabel
      Left = 24
      Top = 24
      Width = 64
      Height = 13
      Caption = 'Dateinmuster'
    end
    object Label3: TLabel
      Left = 24
      Top = 51
      Width = 57
      Height = 13
      Caption = 'Min File Size'
    end
    object Label4: TLabel
      Left = 24
      Top = 78
      Width = 61
      Height = 13
      Caption = 'Max File Size'
    end
    object Edit1: TEdit
      Left = 136
      Top = 21
      Width = 121
      Height = 21
      TabOrder = 0
      Text = 'Edit1'
    end
    object Edit2: TEdit
      Left = 136
      Top = 48
      Width = 121
      Height = 21
      TabOrder = 1
      Text = 'Edit2'
    end
    object Edit3: TEdit
      Left = 136
      Top = 75
      Width = 121
      Height = 21
      TabOrder = 2
      Text = 'Edit3'
    end
  end
  object Button1: TButton
    Left = 206
    Top = 136
    Width = 75
    Height = 25
    Caption = 'Speichern'
    Default = True
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 16
    Top = 136
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Abbrechen'
    TabOrder = 2
    OnClick = Button2Click
  end
end
