unit Unit_logging;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, u_DM_type_def;

type
    TForm8 = class(TForm)
        ListView1: TListView;
    private
        { Private-Deklarationen }
    public
        { Public-Deklarationen }
        procedure addQuery(const caption: string; const t: TQueryTicks);
    end;

var
    Form8: TForm8;

implementation

{$R *.dfm}

procedure TForm8.addQuery(const caption: string; const t: TQueryTicks);
var
    i: integer;
    item: TListItem;
begin
    for i := Low(t) to High(t) do
    begin
        item := ListView1.Items.Add;
        item.caption := caption;
        item.SubItems.Add(t[i].funktion);
        item.SubItems.Add(t[i].start.ToString);
        item.SubItems.Add(t[i].finish.ToString);
        item.SubItems.Add((t[i].finish - t[i].start).ToString);
        item.SubItems.Add(t[i].msg);
    end;
end;

end.
