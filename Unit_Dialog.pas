unit Unit_Dialog;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
    TForm6 = class(TForm)
        Label1: TLabel;
        Button1: TButton;
        Edit1: TEdit;
        Button2: TButton;
        procedure Button1Click(Sender: TObject);
        procedure Button2Click(Sender: TObject);
        procedure FormShow(Sender: TObject);
    private
        { Private-Deklarationen }
        procedure SetProperty_MsgText(const s: string);
        function GetProperty_MsgText: string;
        procedure SetProperty_MsgInfo(const s: string);
        function GetProperty_MsgInfo: string;
    public
        { Public-Deklarationen }
        addNewEntry: boolean;
        property MsgInfo: string read GetProperty_MsgInfo write SetProperty_MsgInfo;
        property MsgText: string read GetProperty_MsgText write SetProperty_MsgText;
    end;

var
    Form6: TForm6;

implementation

{$R *.dfm}

uses Unit_Einstellungen;

function TForm6.GetProperty_MsgText: string;
begin
    result := Edit1.Text;
end;

procedure TForm6.SetProperty_MsgText(const s: string);
begin
    Edit1.Text := s;
end;

function TForm6.GetProperty_MsgInfo: string;
begin
    result := Label1.Caption;
end;

procedure TForm6.SetProperty_MsgInfo(const s: string);
begin
    Label1.Caption := s;
end;

procedure TForm6.Button1Click(Sender: TObject);
begin
    if (addNewEntry) then
    begin
        form4.FolderExcludeListAdd(Edit1.Text);
    end else begin
        form4.FolderExcludeListEdit(Edit1.Text);
    end;
    close;
end;

procedure TForm6.Button2Click(Sender: TObject);
begin
    close;
end;

procedure TForm6.FormShow(Sender: TObject);
begin
    Edit1.SetFocus;
end;

end.
