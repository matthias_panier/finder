unit Unit_RecursiveRead;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.Dialogs, VirtualTrees, System.ImageList, Vcl.ImgList, u_DM_type_def, System.DateUtils,
    Vcl.StdCtrls;

type
    TForm7 = class(TForm)
        VRecusive: TVirtualStringTree;
        Button1: TButton;
        Label1: TLabel;
        Label2: TLabel;
        procedure Button1Click(Sender: TObject);
        procedure WmAfterShow(var Msg: TMessage); message WM_AFTER_SHOW;
        procedure FormShow(Sender: TObject);
        procedure VRecusiveFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
        procedure VRecusiveGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind;
            Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: TImageIndex);
        procedure VRecusiveGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
            TextType: TVSTTextType; var CellText: string);
        procedure VRecusiveInitChildren(Sender: TBaseVirtualTree; Node: PVirtualNode; var ChildCount: Cardinal);
        procedure VRecusiveInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode;
            var InitialStates: TVirtualNodeInitStates);
    private
        { Private-Deklarationen }
    public
        { Public-Deklarationen }
    end;

var
    Form7: TForm7;

implementation

{$R *.dfm}

uses dataModul, main;

procedure TForm7.Button1Click(Sender: TObject);
begin
    dm.setRunning(false);
end;

procedure TForm7.FormShow(Sender: TObject);
begin
    PostMessage(Self.Handle, WM_AFTER_SHOW, 0, 0);
end;

procedure TForm7.VRecusiveFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
begin
end;

procedure TForm7.VRecusiveGetImageIndex(Sender: TBaseVirtualTree; Node: PVirtualNode; Kind: TVTImageKind;
    Column: TColumnIndex; var Ghosted: Boolean; var ImageIndex: TImageIndex);
begin
end;

procedure TForm7.VRecusiveGetText(Sender: TBaseVirtualTree; Node: PVirtualNode; Column: TColumnIndex;
    TextType: TVSTTextType; var CellText: string);
begin
end;

procedure TForm7.VRecusiveInitChildren(Sender: TBaseVirtualTree; Node: PVirtualNode; var ChildCount: Cardinal);
begin
end;

procedure TForm7.VRecusiveInitNode(Sender: TBaseVirtualTree; ParentNode, Node: PVirtualNode;
    var InitialStates: TVirtualNodeInitStates);
begin
end;

procedure TForm7.WmAfterShow(var Msg: TMessage);
begin
//
end;

end.
