﻿unit Unit_Einstellungen;

interface

uses
    Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
    Vcl.Controls, Vcl.Forms, Vcl.ComCtrls, Vcl.StdCtrls, math, Vcl.Buttons, Vcl.CheckLst, u_DM_type_def, ClassFilesList;

type
    TForm4 = class(TForm)
        GroupBox1: TGroupBox;
        CheckBox1: TCheckBox;
        CheckBox2: TCheckBox;
        GroupBox2: TGroupBox;
        CheckBox3: TCheckBox;
        CheckBox4: TCheckBox;
        CheckBox5: TCheckBox;
        CheckBox6: TCheckBox;
        CheckBox7: TCheckBox;
        Button1: TButton;
        Button2: TButton;
        GroupBox3: TGroupBox;
        GroupBox4: TGroupBox;
        CheckBox9: TCheckBox;
        CheckBox10: TCheckBox;
        ListView1: TListView;
        SpeedButton4: TSpeedButton;
        SpeedButton5: TSpeedButton;
        SpeedButton6: TSpeedButton;
        SpeedButton7: TSpeedButton;
        SpeedButton8: TSpeedButton;
        SpeedButton9: TSpeedButton;
        CheckBox11: TCheckBox;
        Edit1: TEdit;
        GroupBox5: TGroupBox;
        CheckBox12: TCheckBox;
        CheckBox13: TCheckBox;
        CheckBox14: TCheckBox;
        CheckBox15: TCheckBox;
        CheckBox16: TCheckBox;
        Edit2: TEdit;
        ListView2: TListView;
        GroupBox6: TGroupBox;
        CheckBox8: TCheckBox;
        SpeedButton1: TSpeedButton;
        SpeedButton2: TSpeedButton;
        SpeedButton3: TSpeedButton;
        ListView3: TListView;
        CheckBox17: TCheckBox;
        CheckBox18: TCheckBox;
        GroupBox7: TGroupBox;
        CheckBox19: TCheckBox;
        CheckBox20: TCheckBox;
        CheckBox21: TCheckBox;
        CheckBox22: TCheckBox;
        CheckBox23: TCheckBox;
        CheckBox24: TCheckBox;
        procedure FormDestroy(Sender: TObject);
        procedure Button1Click(Sender: TObject);
        procedure Button2Click(Sender: TObject);
        procedure FormShow(Sender: TObject);
        procedure SpeedButton1Click(Sender: TObject);
        procedure SpeedButton2Click(Sender: TObject);
        procedure SpeedButton3Click(Sender: TObject);
        procedure SpeedButton4Click(Sender: TObject);
        procedure SpeedButton5Click(Sender: TObject);
        procedure SpeedButton6Click(Sender: TObject);
        procedure SpeedButton7Click(Sender: TObject);
        procedure SpeedButton8Click(Sender: TObject);
        procedure SpeedButton9Click(Sender: TObject);

        procedure setWhitelistFromText(const s: string);
        procedure setBlacklistFromText(const s: string);

    private
        { Private-Deklarationen }
        function GetListItemsAsText(const items: TListItems; const LineSeparator: string;
            const Columnseparator: string): string;

    public
        { Public-Deklarationen }
        procedure addListItem(const i: integer; const mask, minsize, maxsize: string);
        procedure updateListItem(const i: integer; const mask, minsize, maxsize: string);

        procedure LoadAnzeige(const display: TOptionsDisplay);
        procedure LoadLiveIndexing(const LiveImport: TOptionsLiveImport);
        procedure loadPreload(const preload: TOptionsPreload);
        procedure loadExcludedFolderList(const FolderList: TOptionsFolderList);
        procedure LoadRecursiveIndexing(const Recursive: TOptionsRecursiveRead);
        procedure LoadWhiteList(const Whitelist: TClassFilesList);
        procedure LoadBlackList(const Blacklist: TClassFilesList);

        procedure FolderExcludeListAdd(const s: string);
        procedure FolderExcludeListEdit(const s: string);
    end;

var
    Form4: TForm4;

implementation

{$R *.dfm}

uses dataModul, Unit_Whitelist, Unit_Dialog;

procedure TForm4.FormDestroy(Sender: TObject);
begin
    ListView1.items.Clear;
    ListView2.items.Clear;
    ListView3.items.Clear;
end;

procedure TForm4.LoadAnzeige(const display: TOptionsDisplay);
begin
    CheckBox1.Checked := display.showFolders;
    CheckBox2.Checked := display.ExpandFolders;
    CheckBox7.Checked := display.ShowDoubletten;
end;

procedure TForm4.LoadLiveIndexing(const LiveImport: TOptionsLiveImport);
begin
    CheckBox3.Checked := LiveImport.Folders;
    CheckBox4.Checked := LiveImport.Files.active;
    CheckBox5.Checked := LiveImport.hash.active;
    CheckBox6.Checked := LiveImport.Exif.active;
    CheckBox22.Checked := LiveImport.Files.useThread;
    CheckBox23.Checked := LiveImport.hash.useThread;
    CheckBox24.Checked := LiveImport.Exif.useThread;
    CheckBox11.Checked := LiveImport.UseMaxFileSize;
    Edit1.Text := LiveImport.MaxFileSize.ToString;
end;

procedure TForm4.loadPreload(const preload: TOptionsPreload);
begin
    CheckBox19.Checked := preload.Drive;
    CheckBox20.Checked := preload.Folders;
    CheckBox21.Checked := preload.Files;
end;

procedure TForm4.loadExcludedFolderList(const FolderList: TOptionsFolderList);
var
    i: integer;
    item: TListItem;
begin
    CheckBox8.Checked := FolderList.active;

    ListView3.items.Clear;

    for i := 0 to FolderList.List.Count - 1 do
    begin
        item := ListView3.items.Add;
        item.Caption := FolderList.List[i];
    end;
end;

procedure TForm4.LoadRecursiveIndexing(const Recursive: TOptionsRecursiveRead);
begin
    CheckBox12.Checked := Recursive.useThread;
    CheckBox13.Checked := Recursive.CombineInFolders;
    CheckBox14.Checked := Recursive.ExpandCombinedFolders;
    CheckBox15.Checked := Recursive.ScollToEnd;
    CheckBox16.Checked := Recursive.UseMaxFileSize;
    CheckBox17.Checked := Recursive.hashFiles;
    CheckBox18.Checked := Recursive.exifFiles;
    Edit2.Text := Recursive.MaxFileSize.ToString;
end;

procedure TForm4.LoadWhiteList(const Whitelist: TClassFilesList);
var
    i: integer;
    item: TListItem;
begin
    CheckBox9.Checked := Whitelist.active;

    ListView1.items.Clear;

    for i := Low(Whitelist.List) to High(Whitelist.List) do
    begin
        item := ListView1.items.Add;
        item.Caption := Whitelist.List[i].mask;
        item.SubItems.Add(Whitelist.List[i].minsize.ToString);
        item.SubItems.Add(Whitelist.List[i].maxsize.ToString);
    end;

end;

procedure TForm4.LoadBlackList(const Blacklist: TClassFilesList);
var
    i: integer;
    item: TListItem;
begin
    CheckBox10.Checked := Blacklist.active;

    ListView2.items.Clear;

    for i := Low(Blacklist.List) to High(Blacklist.List) do
    begin
        item := ListView2.items.Add;
        item.Caption := Blacklist.List[i].mask;
        item.SubItems.Add(Blacklist.List[i].minsize.ToString);
        item.SubItems.Add(Blacklist.List[i].maxsize.ToString);
    end;
end;

procedure TForm4.Button1Click(Sender: TObject);
var
    i: integer;
begin
    dm.cfg.options.display.showFolders := CheckBox1.Checked;
    dm.cfg.options.display.ExpandFolders := CheckBox2.Checked;
    dm.cfg.options.display.ShowDoubletten := CheckBox7.Checked;

    dm.cfg.options.RecursiveRead.useThread := CheckBox12.Checked;
    dm.cfg.options.RecursiveRead.hashFiles := CheckBox17.Checked;
    dm.cfg.options.RecursiveRead.exifFiles := CheckBox18.Checked;

    dm.cfg.options.RecursiveRead.CombineInFolders := CheckBox13.Checked;
    dm.cfg.options.RecursiveRead.ExpandCombinedFolders := CheckBox14.Checked;
    dm.cfg.options.RecursiveRead.ScollToEnd := CheckBox15.Checked;
    dm.cfg.options.RecursiveRead.UseMaxFileSize := CheckBox16.Checked;

    dm.cfg.options.preload.Drive := CheckBox19.Checked;
    dm.cfg.options.preload.Folders := CheckBox20.Checked;
    dm.cfg.options.preload.Files := CheckBox21.Checked;

    if trystrtoint(Edit2.Text, i) then
        dm.cfg.options.RecursiveRead.MaxFileSize := i;

    dm.cfg.options.LiveImport.Folders := CheckBox3.Checked;
    dm.cfg.options.LiveImport.Files.active := CheckBox4.Checked;
    dm.cfg.options.LiveImport.Files.useThread := CheckBox22.Checked;

    dm.cfg.options.LiveImport.hash.active := CheckBox5.Checked;
    dm.cfg.options.LiveImport.hash.useThread := CheckBox23.Checked;
    dm.cfg.options.LiveImport.Exif.active := CheckBox6.Checked;
    dm.cfg.options.LiveImport.Exif.useThread := CheckBox24.Checked;
    dm.cfg.options.LiveImport.UseMaxFileSize := CheckBox11.Checked;

    if (trystrtoint(Edit1.Text, i)) then
        dm.cfg.options.LiveImport.MaxFileSize := i;

    // exclude folders
    dm.cfg.options.FolderBlackList.active := CheckBox8.Checked;
    dm.cfg.options.FolderBlackList.List.Clear;

    for i := 0 to ListView3.items.Count - 1 do
        dm.cfg.options.FolderBlackList.List.Add(ListView3.items[i].Caption);

    // files whitelist
    dm.cfg.options.FilesWhiteList.active := CheckBox9.Checked;
    dm.cfg.options.FilesWhiteList.Text := GetListItemsAsText(ListView1.items, '|', ';');;

    // files blacklist
    dm.cfg.options.FilesBlackList.active := CheckBox10.Checked;
    dm.cfg.options.FilesBlackList.Text := GetListItemsAsText(ListView2.items, '|', ';');;

    //
    dm.cfg.SaveOptions;
    close;
end;

function TForm4.GetListItemsAsText(const items: TListItems; const LineSeparator: string;
    const Columnseparator: string): string;
var
    i: integer;
    s: string;
begin
    s := '';
    for i := 0 to items.Count - 1 do
    begin
        if (s <> '') then
        begin
            s := s + LineSeparator;
        end;
        s := s + items[i].Caption + Columnseparator + items[i].SubItems[0] + Columnseparator + items[i].SubItems[1];
    end;

    result := s;
    s := '';
end;

procedure TForm4.setWhitelistFromText(const s: string);
begin
    //
end;

procedure TForm4.setBlacklistFromText(const s: string);
begin
    //
end;

procedure TForm4.Button2Click(Sender: TObject);
begin
    FormShow(Sender);
end;

procedure TForm4.FormShow(Sender: TObject);
begin
    LoadAnzeige(dm.cfg.options.display);
    LoadLiveIndexing(dm.cfg.options.LiveImport);
    loadExcludedFolderList(dm.cfg.options.FolderBlackList);
    loadPreload(dm.cfg.options.preload);

    LoadBlackList(dm.cfg.options.FilesBlackList);
    LoadWhiteList(dm.cfg.options.FilesWhiteList);
    LoadRecursiveIndexing(dm.cfg.options.RecursiveRead);
end;

procedure TForm4.SpeedButton1Click(Sender: TObject);
var
    FolderDialog: TForm6;
begin
    FolderDialog := TForm6.create(self);

    try
        try
            FolderDialog.Caption := 'Ordner hinzufügen';
            FolderDialog.MsgInfo := 'Bitte geben Sie die zu überspringenden Ordner ein.';
            FolderDialog.MsgText := '';
            FolderDialog.addNewEntry := true;

            FolderDialog.ShowModal;
        except
            on E: Exception do
            begin
                // showmessage(e.Message);
            end;
        end;

    finally
        FolderDialog.Free;
    end;
end;

procedure TForm4.FolderExcludeListAdd(const s: string);
var
    item: TListItem;
begin
    item := ListView3.items.Add;
    item.Caption := s;
end;

procedure TForm4.FolderExcludeListEdit(const s: string);
begin
    ListView3.ItemFocused.Caption := s;
end;

procedure TForm4.SpeedButton2Click(Sender: TObject);
var
    FolderDialog: TForm6;
begin
    if (ListView3.ItemIndex > -1) then
    begin
        FolderDialog := TForm6.create(Form4);

        try
            try
                FolderDialog.Caption := 'Ordner Ändern';
                FolderDialog.MsgInfo := 'Bitte passen Sie den Ordner an.';
                FolderDialog.MsgText := ListView3.ItemFocused.Caption;
                FolderDialog.addNewEntry := false;

                FolderDialog.ShowModal;
            except
                on E: Exception do
                begin
                    // showmessage(e.Message);
                end;
            end;
        finally
            FolderDialog.Free;
        end;
    end;
end;

procedure TForm4.SpeedButton3Click(Sender: TObject);
begin
    if (ListView3.ItemIndex > -1) then
    begin
        ListView3.items.Delete(ListView3.ItemIndex);
    end;

end;

procedure TForm4.SpeedButton4Click(Sender: TObject);
var
    MaskDialog: TForm5;
begin
    MaskDialog := TForm5.create(self);
    MaskDialog.addNewEntry := true;
    try
        try
            MaskDialog.SetFileParams(1, '', '', '');
            MaskDialog.ShowModal;
        except
            on E: Exception do
            begin
                // showmessage(e.Message);
            end;
        end;

    finally
        MaskDialog.Free;
    end;
end;

procedure TForm4.SpeedButton5Click(Sender: TObject);
var
    MaskDialog: TForm5;
begin
    if ListView1.SelCount > 0 then
    begin
        MaskDialog := TForm5.create(self);
        MaskDialog.addNewEntry := false;

        try
            try
                MaskDialog.SetFileParams(1, ListView1.Selected.Caption, ListView1.Selected.SubItems[0],
                    ListView1.Selected.SubItems[1]);
                MaskDialog.ShowModal;
            except
                on E: Exception do
                begin
                    // showmessage(e.Message);
                end;
            end;
        finally
            MaskDialog.Free;
        end;
    end;
end;

procedure TForm4.SpeedButton6Click(Sender: TObject);
begin
    if ListView1.SelCount > 0 then
    begin
        ListView1.Selected.Delete;
    end;
end;

procedure TForm4.SpeedButton7Click(Sender: TObject);
var
    MaskDialog: TForm5;
begin
    MaskDialog := TForm5.create(self);
    MaskDialog.addNewEntry := true;
    try
        try
            MaskDialog.SetFileParams(2, '', '', '');
            MaskDialog.ShowModal;
        except
            on E: Exception do
            begin
                // showmessage(e.Message);
            end;
        end;

    finally
        MaskDialog.Free;
    end;
end;

procedure TForm4.SpeedButton8Click(Sender: TObject);
var
    MaskDialog: TForm5;
begin
    if ListView2.SelCount > 0 then
    begin
        MaskDialog := TForm5.create(self);
        MaskDialog.addNewEntry := false;

        try
            try
                MaskDialog.SetFileParams(2, ListView2.Selected.Caption, ListView2.Selected.SubItems[0],
                    ListView2.Selected.SubItems[1]);
                MaskDialog.ShowModal;
            except
                on E: Exception do
                begin
                    // showmessage(e.Message);
                end;
            end;
        finally
            MaskDialog.Free;
        end;
    end;
end;

procedure TForm4.SpeedButton9Click(Sender: TObject);
begin
    if ListView2.SelCount > 0 then
    begin
        ListView2.Selected.Delete;
    end;
end;

procedure TForm4.addListItem(const i: integer; const mask: string; const minsize: string; const maxsize: string);
var
    item: TListItem;
begin
    case i of
        1:
            item := ListView1.items.Add;
        2:
            item := ListView2.items.Add;
    else
        item := nil;
    end;

    if item <> nil then
    begin
        item.Caption := mask;
        item.SubItems.Add(minsize);
        item.SubItems.Add(maxsize);
    end;
end;

procedure TForm4.updateListItem(const i: integer; const mask: string; const minsize: string; const maxsize: string);
var
    item: TListItem;
begin
    case i of
        1:
            item := ListView1.Selected;
        2:
            item := ListView2.Selected;
    else
        item := nil;
    end;

    if item <> nil then
    begin
        item.Caption := mask;
        item.SubItems[0] := minsize;
        item.SubItems[1] := maxsize;
    end;
end;

end.
